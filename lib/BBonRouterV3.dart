import 'package:flutter/material.dart';
import 'package:itswd/v3/screen/SignIn/SignInScreen.dart';
import 'package:itswd/v3/screen/main/MainScreen.dart';
import 'package:itswd/v3/screen/my_page/change_info/ChangeUserInfo.dart';
import 'package:itswd/v3/screen/my_page/setting/SettingScreen.dart';
import 'package:itswd/v3/screen/reply/ReplyScreen.dart';
import 'package:itswd/v3/screen/search/SearchTagScreen.dart';
import 'package:itswd/v3/screen/splash/SplashScreen.dart';
import 'package:itswd/v3/screen/user_content/UserContentScreen.dart';

import 'v3/screen/my_page/dress_list/DressListScreen.dart';


class BBonRouterV3 {
   BBonRouterV3(this.context);

  BuildContext context;

  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/sign':
        return MaterialPageRoute(builder: (_) => SignInScreen());
      case '/main':
        Map<String, dynamic> data = settings.arguments;
        return MaterialPageRoute(builder: (_) => MainScreen(userInfo: data['userInfo']));
      case '/reply':
        Map<String, dynamic> data = settings.arguments;
        return MaterialPageRoute(builder: (_) => ReplyScreen(dress: data['dress'],));
      case '/userContent':
        Map<String, dynamic> data = settings.arguments;
        return MaterialPageRoute(builder: (_) => UserContentScreen(user: data['user'],));
      case '/setting':
        return MaterialPageRoute(builder: (_) => SettingScreen());
      case '/changeInfo':
        Map<String, dynamic> data = settings.arguments;
        return MaterialPageRoute(builder: (_) => ChangeUserInfo(type: data['type'],));
      case '/sigleContents':
        Map<String, dynamic> data = settings.arguments;
        return MaterialPageRoute(builder: (_) => 
          DressListScreen(
            url: data['url'], 
            enableReadMore:data['enableReadMore']??false,
            title: data['title']??'무제'
        ));
      case '/searchTagResult':
        Map<String, dynamic> data = settings.arguments;
        return MaterialPageRoute(builder: (_) => SearchTagScreen(tagWord: data['tagWord'],));

      default:
        return MaterialPageRoute(builder: (_) => SplashScreen(key: UniqueKey()));
    }
  }
}