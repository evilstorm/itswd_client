import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:itswd/BBonRouterV3.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/provider/DailyDressEventProvider.dart';
import 'package:itswd/v3/provider/DailyDressProvider.dart';
import 'package:itswd/v3/provider/SearchDressProvider.dart';
import 'package:itswd/v3/provider/SearchTagProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }

  
}

void main() async {

  WidgetsFlutterBinding.ensureInitialized(); 
  HttpOverrides.global = new MyHttpOverrides();
  initializeDateFormatting().then((_) => runApp(MyApp()));
} 

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  static ThemeData theme = new ThemeData( 
    backgroundColor: const Color(0xFFFFFFFF),
    dialogBackgroundColor: const Color(0xFF6C6576),
    primaryColor: const Color(0xFFACFFFF),
  );

  static ThemeData darkTheme = new ThemeData(     
    backgroundColor: const Color(0xFF242326),
    dialogBackgroundColor: const Color(0xFF383838),
    primaryColor: const Color(0xFF141316),
    accentColor: Colors.redAccent[200],
    hintColor: Colors.grey[400],
    disabledColor: Colors.grey[700],
    secondaryHeaderColor: Colors.deepOrangeAccent[400],
    errorColor: Colors.redAccent[700],
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    textTheme: TextTheme(
      headline1: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold, color: Colors.redAccent[200]),
      headline2: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold, color: Colors.redAccent[200]),
      headline3: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold, color: Colors.redAccent[200]),
      headline4: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
      headline5: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
      headline6: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
      subtitle1: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
      subtitle2: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w500),
      bodyText1: TextStyle(fontSize: 18.0,  ),
      bodyText2: TextStyle(fontSize: 16.0, ),
      caption: TextStyle(fontSize: 12.0, color: Colors.grey[400]),
      button: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700, color: Colors.pinkAccent[700]),
      overline: TextStyle(fontSize: 14.0),
    ).apply(
      bodyColor: Colors.white,
    )
    
  );
  final ThemeData usedTheme = darkTheme;
  Widget somethingWentWrong() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Center(
        child: Text(
          'Eror',

        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    Print.i(' main build');
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
          Print.e(' init snapshot $snapshot');
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider( 
            providers: [
              ChangeNotifierProvider(create: (_) => UserProvider()),
              ChangeNotifierProvider(create: (_) => DailyDressProvider()),
              ChangeNotifierProvider(create: (_) => DailyDressEventProvider()),
              ChangeNotifierProvider(create: (_) => SearchTagProvider()),
            ],
            child: GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: MaterialApp(
                title: 'Flutter Demo',
                debugShowCheckedModeBanner: false,
                theme: usedTheme,
                initialRoute: "/",
                onGenerateRoute: BBonRouterV3(context).generateRoute,
              ),
            ),
          );
        }

        return Directionality(
            textDirection: TextDirection.ltr,
            child: Text('잠시만 기다려주세요')
            );
      }
    );
  }
}