import 'package:flutter/material.dart';

class ESTrackPad extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
    ..color = Colors.deepPurpleAccent
    ..strokeCap = StrokeCap.round
    ..strokeWidth = 4.0;



    Paint paint2 = Paint()
    ..color = Colors.white
    ..strokeCap = StrokeCap.round
    ..strokeWidth = 4.0;



    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawRect(rect, paint);


    Rect rect2 = Rect.fromLTWH(size.width/4, size.height/4, size.width/2, size.height/2);
    canvas.drawRect(rect2, paint2);

    Offset p1 = Offset(0.0, 0.0);
    Offset p2 = Offset(size.width/4, size.height/4);
    canvas.drawLine(p1, p2, paint2);

    Offset p3 = Offset(size.width, 0.0);
    Offset p4 = Offset(size.width - (size.width/4), size.height/4);
    canvas.drawLine(p3, p4, paint2);

    Offset p5 = Offset(0.0, size.height);
    Offset p6 = Offset(size.width/2 + size.width/4, size.height/4);
    canvas.drawLine(p5, p6, paint2);

    Offset p7 = Offset(size.width, size.height);
    Offset p8 = Offset(size.width/4, size.height/4);
    canvas.drawLine(p7, p8, paint2);

  }

  

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }  

}