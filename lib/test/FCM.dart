import 'package:flutter/material.dart';

import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:itswd/v3/components/common/Print.dart';

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging(); 


class FCMTest extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _FCMTest();
  }
}

class _FCMTest extends State<FCMTest> {

  @override
  void initState() {
    super.initState();
    firebaseCloudMessagingListeners();
  }

  @override
  Widget build(BuildContext context) {

    return Center(
      child: Text(
        'Test',
      ),
    );
  }

  void firebaseCloudMessagingListeners() {
    if (Platform.isIOS) iOSPermission();

    _firebaseMessaging.getToken().then((token){
      Print.i('token:'+token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        Print.i('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        Print.i('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        Print.i('on launch $message');
      },
    );
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      Print.i("Settings registered: $settings");
    });
  }
}