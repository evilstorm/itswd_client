
import 'dart:async';
import 'dart:convert' show json;

import "package:http/http.dart" as http;
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:itswd/v3/components/common/Print.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
  ],
);

class SignInDemo extends StatefulWidget {
  @override
  State createState() => SignInDemoState();
}

class SignInDemoState extends State<SignInDemo> {
  GoogleSignInAccount _currentUser;
  String _contactText;
  GoogleSignInAuthentication auth;

  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {

    account.authentication.then((_) {
      auth = _;
      Print.i(auth.accessToken);
      //ya29.Il-9B0V0UhShsQNEbg-qSLWbrM1ABCKmSYcSUe0fPT0oyiIQ0EturCmjHin1q5ynKtGzv466POr2wKoDdvz88MKDyBh5A2zmEVLtWUjOM8H08zoTwZsDwmnvuLnbzPg2Lw
      Print.i(auth.idToken);
      /**
       * eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc2MmZhNjM3YWY5NTM1OTBkYjhiYjhhNjM2YmYxMWQ0MzYwYWJjOTgiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI0MDMyNTYwMTg2OS1obTRlYmdwZTJhZzBicWQ3cTM2MW5vN2FqcGM5cmljYS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImF1ZCI6IjQwMzI1NjAxODY5LWJpcmk5dmgwdm0zcHZwdGM3NmRlcm5mYTg4YTVzdjUwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE3OTI1NDI0NjAxODA4MjEyOTczIiwiZW1haWwiOiJnZXZpbHN0b3JtQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiTWluIENoZW9sIEtpbSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLUF3Sk8xU1NSTnk4L0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FDSGkzcmYzSjNpRWV5VjdGb0dFSFVpWWtzWHFVZlNVcEEvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6Ik1pbiBDaGVvbCIsImZhbWlseV9uYW1lIjoiS2ltIiwibG9jYWxlIjoia28iLCJpYXQiOjE1ODIwODk2ODIsImV4cCI6MTU4MjA5MzI4Mn0.NQfa8Fk9BDcwTK4tI2vAoER0-kbvopm51KidrDIVrJ6qEo7aGtVeW_-TF9PEG5qKhIWNjp_G2exomsTtxxY4NAW2DG_sLWSwsKNJs2L7zwsTEqUg19iWZ5j8wgDenWMQtS6cJgUNZdJcPqcDeJmEMj7vKCDf4_Ln2IfIgHP6jiaBCXQs_epII_ipewFXseUKms0OJrhlmM4ys5IlzlNUS7hcQMGQnQxReVLKRuaOJ63TQzzLACNHs53A8bXbhRJON2DoB_LkRMVNGfVA4YvrJ78YSZyeotdzHEB_4TupsA2DvVM18jzFVwSbK3fWytPdKqmVWg93pmJLVLffvjE3oQ
       */
      Print.i(auth.toString());
      Print.i(" AUTH !!! END ");
    });
      account.authentication.then((_) {
        Print.i(_);
      });
      setState(() {
        _currentUser = account;
      });
    });
    _googleSignIn.signInSilently();
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      Print.e(error);
    }
  }

  Future<void> _handleSignOut() => _googleSignIn.disconnect();

  Widget _buildBody() {
    if (_currentUser != null) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ListTile(
            leading: GoogleUserCircleAvatar(
              identity: _currentUser,
            ),
            title: Text(_currentUser.displayName ?? ''),
            subtitle: Text(_currentUser.email ?? ''),
          ),
          const Text("Signed in successfully."),
          Text(_currentUser.id),
          RaisedButton(
            child: const Text('SIGN OUT'),
            onPressed: _handleSignOut,
          ),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          const Text("You are not currently signed in."),
          RaisedButton(
            child: const Text('SIGN IN'),
            onPressed: _handleSignIn,
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Google Sign In'),
        ),
        body: ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: _buildBody(),
        ));
  }
}