library constants;

const int APP_VER = 10;

const CONTENT_HEIGHT = 620.0;
const HEADER_HEIGHT = 0.0;

enum SORT_TYPE {LIKE, DATE}


enum LoginType {
  Inapp,
  Google,
  Facebook,
  KakaoTalk,
  Apple
}