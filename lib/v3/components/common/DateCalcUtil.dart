import 'package:timeago/timeago.dart' as timeago;


class DateCalcUtil { 
  
  static final DateCalcUtil _singleton = new DateCalcUtil._internal(); 
   static setLocalMessages() {
    timeago.setLocaleMessages('ko', timeago.KoMessages());

  }

  factory DateCalcUtil() { 
    return _singleton; 
  } 

  DateCalcUtil._internal() {} 
  
  static String timeAgo(DateTime date) {
    return timeago.format(
      date,
      locale: 'ko', // for test.
      allowFromNow: true
    );
  }
  
  static bool isSameSay(DateTime selectedDate) {
    final now = DateTime.now();

    int dateGap = now.difference(selectedDate).inDays;

    if(dateGap == 0 && now.day == selectedDate.day) {
      return true;
    }

    return false;
  }


}


class KoMessages implements timeago.LookupMessages {
  String prefixAgo() => '';
  String prefixFromNow() => '';
  String suffixAgo() => '전';
  String suffixFromNow() => '후';
  String lessThanOneMinute(int seconds) => '방금';
  String aboutAMinute(int minutes) => '방금';
  String minutes(int minutes) => '$minutes분';
  String aboutAnHour(int minutes) => '1시간';
  String hours(int hours) => '$hours시간';
  String aDay(int hours) => '1일';
  String days(int days) => '$days일';
  String aboutAMonth(int days) => '한달';
  String months(int months) => '$months개월';
  String aboutAYear(int year) => '1년';
  String years(int years) => '$years년';
  String wordSeparator() => ' ';
}
