import 'package:intl/intl.dart';

DateFormat dateFormatHHmmsss = DateFormat('HH:mm:ss.sss');
DateFormat dateFormatHHmm = DateFormat('HH:mm:ss');
DateFormat dateFormatMMdd = DateFormat('M/d');
DateFormat dateFormatdd = DateFormat('dd');
