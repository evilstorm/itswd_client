import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/SharedPref.dart';
import 'package:itswd/v3/components/common/SharedPrefKeys.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/UserProvider.dart';

class FBMessage {

  static final FBMessage _instance = FBMessage._internal();
  factory FBMessage() {
    return _instance;
  }

  FBMessage._internal() {
   //초기 이벤트  
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  UserProvider userProvider;

  String token;

  void init(String userToken) {
    if(this.token == userToken) {
      return;
    }

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        Print.i(' FCM onMessage Message!! $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        Print.i(' FCM onLaunch Message!! $message');
      },
      onResume: (Map<String, dynamic> message) async {
        Print.i(' FCM onResume Message!! $message');
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
    });

    _firebaseMessaging.getToken().then((String token) {
      if(userToken == token ) {
        return;
      }
      

      setString(SharedPrefKeys.PUSH_TOEN, token);
      ApiHelper().patch(
        '/user', 
        json.encode({
          'pushToken': token
        })
      );
      Print.i('FCM TOKEN : $token');
    });
  }
}