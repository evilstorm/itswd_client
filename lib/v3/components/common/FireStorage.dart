import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:itswd/v3/components/common/Print.dart';


class FireStorage {

  FirebaseApp app;
  FirebaseStorage storage;

  Future<String> uploadFile(email, file) async {
    String uploadFileName = '${email}_${DateTime.now().millisecondsSinceEpoch}';
    
    Reference ref = FirebaseStorage.instance
    .ref()
    .child('dress/$uploadFileName');

    final UploadTask uploadTask = ref.putFile(file);
    await uploadTask;
    var downloadUrl = await ref.getDownloadURL();

    return downloadUrl;
  }
}