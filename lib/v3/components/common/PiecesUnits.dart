import 'package:itswd/v3/model/response/ModelUser.dart';

String getName(ModelUser user) {
  if(user == null || user.name == null) {
    return "Unknown";
  }
  return user.name;
}

String getAboutMe(ModelUser user) {
  if(user == null) {
    return "Unknown";
  }
  if(user.aboutMe != null) {
    return user.aboutMe;
  }
  if(user.name == null) {
    return user.email.split("@")[0];
  }
  return user.name;
}

