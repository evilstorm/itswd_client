import 'package:shared_preferences/shared_preferences.dart';

Future<String> getString(key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString(key) ?? null;
}

Future<bool> setString(key, value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return  await prefs.setString(key, value);
}

Future<int> getInt(key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getInt(key) ?? 0;
}

Future<bool> setInt(key, value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return await prefs.setInt(key, value);
}

Future<double> getDouble(key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getDouble(key) ?? 0;
}

Future<bool> setDouble(key, value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return await prefs.setDouble(key, value);
}

Future<bool> getBool(key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if(prefs.getBool(key) == null) {
    return false;
  }
  return prefs.getBool(key);
}

Future<bool> setBool(key, value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return await prefs.setBool(key, value);
}
