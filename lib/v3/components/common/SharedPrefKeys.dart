
class SharedPrefKeys {
  static final String READ_NOTIFY_INDEX_I = "notify_index";
  static final String AGREE_TERM_VER_I = "term";
  static final String JWT_TOKEN = "jwt_token";
  static final String PUSH_TOEN = "push_token";

  static final String LOGIN_TYPE_I = "loginType";



}