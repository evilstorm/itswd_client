import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/SharedPref.dart';
import 'package:itswd/v3/components/common/SharedPrefKeys.dart';
import 'package:itswd/v3/network/ApiHelper.dart';

class ReqSignIn {

  dynamic signIn(data, signinType) async {

    try {
      var response = await ApiHelper().post(
        '/user/signin',
        data
      );

      if(response['code'] != 200) {
        return {
          'result':false,
          'data':response
        };
      }
      Print.i('setToken : ${response['data']['jwtToken']}');
      
      setString(SharedPrefKeys.JWT_TOKEN, response['data']['jwtToken']);
      setInt(SharedPrefKeys.LOGIN_TYPE_I, signinType);

      return {
        'result': true,
        'data': response['data']['myInfo']
      };

    } catch (e) {
      return{
        'result': false,
        'data': 'Error ${e.toString()}'
      };
    }
  }


}