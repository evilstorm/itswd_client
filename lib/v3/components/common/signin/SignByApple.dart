import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/signin/ReqSignIn.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';


class SignByApple {
    
  Function signinResult;
  SignByApple({
    @required this.signinResult
  });

  void doSignInByApple(bool isSilently) async {

    try {

      AuthorizationCredentialAppleID credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );

      Print.d(credential);

      var signin = ReqSignIn();
      var _signinResult = await signin.signIn(
        json.encode({
          'email': credential.userIdentifier,
          'loginType': 4,
        }), 4
      );
      signinResult(_signinResult);   
    } catch (e) {
      Print.e(e);
      signinResult({
        'result': false,
        'data': 'Error ${e.toString()}'
      });
    }
  }
}