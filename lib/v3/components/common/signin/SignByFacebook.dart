import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/signin/Constants.dart';
import 'package:itswd/v3/components/common/signin/ReqSignIn.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';


class SignByFacebook {
  
  Function signinResult;
  
  SignByFacebook({
    @required this.signinResult
  });

  void doSignInByFacebook(bool isSilently) async {
    try {
      
      // final AccessToken result = await FacebookAuth.instance.login();
      // Print.e(result);

      final FacebookLogin facebookSignIn = new FacebookLogin();

      final FacebookLoginResult result = await facebookSignIn.logIn(['email']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          var signin = ReqSignIn();
          var _signinResult = await signin.signIn(
            json.encode({
              'email': result.accessToken.userId,
              'loginType': 2,
            }), 2
          );
          signinResult(_signinResult);        
       break;         
        case FacebookLoginStatus.cancelledByUser:
          Print.e("FacebookLoginStatus.cancelledByUser");
          signinResult({
            'result': false,
            'data': 'Error ${result.toString()}'
          });
        break;
        case FacebookLoginStatus.error:
          Print.e("FacebookLoginStatus.error");
          signinResult({
            'result': false,
            'data': 'Error ${result.toString()}'
          });
        break;
      }

    } catch (e) {
      Print.e(e);
      signinResult({
        'result': false,
        'data': 'Error ${e.toString()}'
      });
    }
  }

}