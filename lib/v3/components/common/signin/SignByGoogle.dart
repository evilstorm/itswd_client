
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/signin/Constants.dart';
import 'package:itswd/v3/components/common/signin/ReqSignIn.dart';
import 'package:itswd/v3/model/ModelSignIn.dart';
import 'package:itswd/v3/provider/UserProvider.dart';

/**
 * 구글 로그인
 */
class SignByGoogle {
  
  Function signinResult;

  SignByGoogle({
    @required this.signinResult
  });

/**
 * GoogleSignInAccount:
 * {
 *  displayName: 모카한 잔, 
 *  email: acomocha@gmail.com, 
 *  id: 115837700447429565962, 
 *  photoUrl: https://lh3.googleusercontent.com/-TOAEIGvPHEU/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckyKpi6c_dJD3x-Kqz5Nk41LrNpkA/s1337/photo.jpg
 * }
 * 
 */
  void doSignInByGoogle(bool isSilently) async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: <String>[
        'email',
      ],
    );

Print.e("moveNextPage google 111111111111");

    if(isSilently) {
      Print.e("moveNextPage google 2222222222");
      _googleSignIn.signInSilently();
    } else {

      try {
        if(await _googleSignIn.isSignedIn()){
      Print.e("moveNextPage google 3333333");
          await _googleSignIn.disconnect();
        }
        
      Print.e("moveNextPage google 44444444");
        _googleSignIn.signIn();

      } catch (e) {
      Print.e("moveNextPage google 555555555555");
        signinResult({
          'result': false,
          'data': 'Error ${e.toString()}'
        });
        return null;
      }
    }

    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) async {    
      Print.i(" Google SignIn Accoujnt: $account");

      if(account == null) {
        signinResult({
          'result': false,
          'data': ''
        });
      } else {
          var signin = ReqSignIn();
          var _signinResult = await signin.signIn(
            json.encode({
            'email': account.id,
            'loginType': 1,
            'name': account.displayName,
            'pictureMe': account.photoUrl
            }), 1
          );
          signinResult(_signinResult);
      }
    });


  }


  


  ModelSignIn convertModel(GoogleSignInAccount account) {
      var result = ModelSignIn();
      result.loginType = 1;
      result.email = account.id;
      result.name = account.displayName;
      result.pictureUrl = account.photoUrl;

    return result;
  }


}