import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/signin/Constants.dart';
import 'package:itswd/v3/components/common/signin/ReqSignIn.dart';
import 'package:itswd/v3/provider/UserProvider.dart';

import 'package:kakao_flutter_sdk/all.dart';
import 'package:kakao_flutter_sdk/auth.dart';


class SignByKakao {
  
  Function signinResult;
  
  SignByKakao({
    @required this.signinResult
  });

///
/// Kakao Signin Result Json Format: 
/// 
/// User: {id: 1517644622, 
/// properties: {nickname: 김민철, 
///  profile_image: https://k.kakaocdn.net/dn/7lSwm/btqykONB6Gf/NOCbKzu0sWKiiTb4XRgRsK/img_640x640.jpg, 
///  thumbnail_image: https://k.kakaocdn.net/dn/7lSwm/btqykONB6Gf/NOCbKzu0sWKiiTb4XRgRsK/img_110x110.jpg}, 
/// kakao_account: {
///  profile_needs_agreement: false, 
///    profile: {
///      nickname: 김민철, 
///      thumbnail_image_url: https://k.kakaocdn.net/dn/7lSwm/btqykONB6Gf/NOCbKzu0sWKiiTb4XRgRsK/img_110x110.jpg, 
///      profile_image_url: https://k.kakaocdn.net/dn/7lSwm/btqykONB6Gf/NOCbKzu0sWKiiTb4XRgRsK/img_640x640.jpg}, 
///    is_email_verified: true, is_email_valid: true, 
///    email_needs_agreement: false, email: evilstorm@naver.com}, 
///  connected_at: 2020-10-30T10:54:15.000Z}
///
///
  void doSignInByKakao(bool isSilently) async {
 try {
      KakaoContext.clientId = "e46074bb70c59e13e57e7403851cda4f";

      String authCode = await AuthCodeClient.instance.request();

      AccessTokenResponse token = await AuthApi.instance.issueAccessToken(authCode);
      AccessTokenStore.instance.toStore(token); // S

      var user = await UserApi.instance.me();
      Print.e(" User: $user");
        var signin = ReqSignIn();
        var _signinResult = await signin.signIn(
          json.encode({
          'email': user.id,
          'loginType': 3,
          'name': user.properties['nickname'],
          'pictureMe': user.properties['profile_image']
          }), 3
        );
        signinResult(_signinResult);

      } catch (e) {
        Print.e(e.toString());
        signinResult({
          'result': false,
          'data': 'Error ${e.toString()}'
        });
      }
  }

}