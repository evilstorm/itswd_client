
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/SharedPref.dart';
import 'package:itswd/v3/components/common/SharedPrefKeys.dart';
import 'package:itswd/v3/components/common/signin/Constants.dart';
import 'package:itswd/v3/components/common/signin/SignByApple.dart';
import 'package:itswd/v3/components/common/signin/SignByFacebook.dart';
import 'package:itswd/v3/components/common/signin/SignByGoogle.dart';
import 'package:itswd/v3/components/common/signin/SignByKakao.dart';
import 'package:itswd/v3/provider/UserProvider.dart';

class SignInBBon {
  final Function signinResult;
  
  SignInBBon({
    this.signinResult
  });


  void autoSignIn() async {
Print.e("moveNextPage 222222222");
    final token = await getString(SharedPrefKeys.JWT_TOKEN);
    final loginType = await getInt(SharedPrefKeys.LOGIN_TYPE_I);

    if(token == null) {
      Print.e("moveNextPage 333333333");
      signinResult({
          'result': false,
          'data': ''
        });
      return;
    }
Print.e("moveNextPage 444444444 $token //$loginType");
    
    switch(loginType) {
      case SignInByGoogle: 
        SignByGoogle(signinResult: signinResult).doSignInByGoogle(true);
      break;
      case SignInByFaceBook: 
        SignByFacebook(signinResult: signinResult).doSignInByFacebook(true);
      break;
      case SignInByKaKao: 
        SignByKakao(signinResult: signinResult).doSignInByKakao(true);
      break;
      case SignInByApple: 
        SignByApple(signinResult: signinResult).doSignInByApple(true);
      break;
    }
  }

  void doSignIn(int loginType) async {
    switch(loginType) {
      case SignInByGoogle: 
        SignByGoogle(signinResult: signinResult).doSignInByGoogle(false);
      break;
      case SignInByFaceBook:
        SignByFacebook(signinResult: signinResult).doSignInByFacebook(false);
      break;
      case SignInByKaKao: 
        SignByKakao(signinResult: signinResult).doSignInByKakao(false);
      break;
      case SignInByApple:
        SignByApple(signinResult: signinResult).doSignInByApple(false);
      break;
    }

  }


}