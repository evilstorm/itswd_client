import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:itswd/v3/components/views/CircleDraw.dart';

import 'package:itswd/v3/components/views/colors.dart';


class AwardAvatar extends StatelessWidget {

  final String url;
  final double width;
  final double height;
  final double lineWith;
  final bool isToday;
  AwardAvatar({
    this.url,
    this.width = 45.0,
    this.height = 45.0,
    this.lineWith = 2.0,
    this.isToday = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      height: this.height,
      child: Stack(
        children: <Widget>[
           Center(
            child: ClipOval(
              child: 
                (!isToday)
                ? CachedNetworkImage(
                    width: this.width - lineWith*5,
                    height: this.height - lineWith*5,
                    fit: BoxFit.fitWidth,
                    imageUrl: this.url, 
                  )
                : Container(
                    width: this.width - lineWith*5,
                    height: this.height - lineWith*5,
                    child: Center(
                      child: Text.rich(
                        TextSpan(
                          text: 'Today\n',
                          style: Theme.of(context).textTheme.bodyText1,
                          children: <TextSpan> [
                            TextSpan(
                              text: '?',
                              style: Theme.of(context).textTheme.subtitle1
                            ),
                          ]
                        ),
                        textAlign: TextAlign.center,
                      )
                    ),
                )
            ),
          ),
          Center(
            child: Container(
              width: this.width,
              height: this.height,
              child: CustomPaint(
                painter: CircleDraw(
                  lineColor: (isToday)?COLORS_DARK_GRAIDENT_TYPE_1: COLORS_GOLD_GRAIDENT_TYPE_1,
                  width: lineWith
                ),
              ),
            ),
          ),
          Center(
            child: Container(
              width: this.width-lineWith*4,
              height: this.height-lineWith*4,
              child: CustomPaint(
                painter: CircleDraw(
                  lineColor: (isToday)?COLORS_DARK_GRAIDENT_TYPE_2: COLORS_GOLD_GRAIDENT_TYPE_2,
                  width: lineWith
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}