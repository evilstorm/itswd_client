import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';

class BlurText extends StatelessWidget {

  final String text;
  final double textSize;
  final double strokeWidth;
  final Color textColor;
  final Color textLineColor;
  final Color blurColor;

  BlurText({
    Key key,
    this.text,
    this.textSize = 100,
    this.strokeWidth = 6,
    this.textColor = Colors.white,
    this.textLineColor = const Color.fromARGB(150, 240, 160, 40),
    this.blurColor = const Color.fromARGB(250, 250, 230, 140)
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          AutoSizeText(
            text,
            maxLines: 1,
            style: TextStyle(
              fontSize: textSize,
              fontWeight: FontWeight.bold,
              foreground: Paint()
                ..style = PaintingStyle.stroke
                ..strokeWidth = strokeWidth
                ..color = textLineColor,
                shadows: [
                  BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 15,
                    color: blurColor,
                  )
                ]
            ),
          ),
          AutoSizeText(
            text,
            maxLines: 1,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: textSize,
              color: textColor,
            )
          )
        ],
      ),
    );
  }
}