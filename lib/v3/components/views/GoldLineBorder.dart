import 'package:flutter/material.dart';
import 'package:itswd/v3/components/views/colors.dart';


class GoldLineBorder extends StatelessWidget {

  final double width;
  final double height;
  final Widget child;
  final Color backgroundColor
  ;
  GoldLineBorder({    
    Key key,
    this.width = double.infinity,
    this.height = 180.0,
    this.child,
    this.backgroundColor
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration: BoxDecoration (
          gradient: LinearGradient(
            colors: COLORS_GOLD_GRAIDENT_TYPE_3
          )
        ),
        child: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Container(
              color: backgroundColor??Theme.of(context).backgroundColor,
              child: child,
            ),
        ),
      ),
    );
  }
}