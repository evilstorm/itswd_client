
import 'package:flutter/material.dart';

class HeaderWithBack extends StatefulWidget {

  const HeaderWithBack({
    Key key, 
    this.title, 
    this.callback,
  }) : super(key: key);

  final String title;
  final Function callback; 

  @override
  _HeaderWithBackState createState() => _HeaderWithBackState();
}

class _HeaderWithBackState extends State<HeaderWithBack> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        IconButton(
          onPressed: () {
            widget.callback();
          },
          iconSize: 24,
          color: Colors.white,
          icon: Icon(Icons.arrow_back),
        ),
        SizedBox(width: 8.0,),
        Text(widget.title, style: Theme.of(context).textTheme.subtitle2,)
      ],
    );
  }
}
