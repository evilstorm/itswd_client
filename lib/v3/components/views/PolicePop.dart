import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/material.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:provider/provider.dart';


class PolicePop extends StatefulWidget {
  final ModelDress dress;

  PolicePop({
    this.dress,
  });

  @override
  _PolicePopState createState() => _PolicePopState();
}

class _PolicePopState extends State<PolicePop> {
  UserProvider userProvider;

  int selectedRadio = 0;
  int selectedRadioTile = 0;
  bool _showResult = false;
  String _resultMsg = "";
  
  

  @override
  void initState() {
    super.initState();
    selectedRadio = 0;
    selectedRadioTile = 0;
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of<UserProvider>(context);
    
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
      ),      
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  void postPolice() async {
    if(selectedRadioTile == 0) {
      Navigator.pop(context);
      return;
    }

    var response = await ApiHelper().post(
      '/police', 
      json.encode({
      'dressId': widget.dress.id,
      'reporterId': userProvider.userInfo.id,
      'reason': selectedRadioTile,
    }));

    if(response['code'] != 200) {
      setState(() {
        _resultMsg = "일시적인 문제로 전송하지 못했습니다. \n잠시 후 다시 시도해주세요.(${response['message']})";
        _showResult = true;  
      });

      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          _showResult = false;
        });
      });

      return null;
    }
    
    widget.dress.reportThis = true;
    userProvider.updateUserInfo();

    setState(() {
      _resultMsg = "신고 접수 되었습니다.";
      _showResult = true;  
    });

    Future.delayed(const Duration(milliseconds: 1000), () {
      setState(() {
        _showResult = false;
      });

      Navigator.pop(context);
    });

  }

  final double contentWidth = 430.0;
  final double contentHeight = 400.0;
  dialogContent(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: contentWidth,
          height: contentHeight,
          color: Theme.of(context).backgroundColor,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 32.0,),
              Text('신고하기', style: Theme.of(context).textTheme.subtitle2,),
              SizedBox(height: 16.0,),
              RadioListTile(
                value: 1,
                groupValue: selectedRadioTile,
                title: Text("부적절한 이미지", style: Theme.of(context).textTheme.bodyText1,),
                onChanged: (val) {
                  print("Radio Tile pressed $val");
                  setSelectedRadioTile(val);
                },
                activeColor: Colors.red,
                selected: true,
              ),
              RadioListTile(
                value: 2,
                groupValue: selectedRadioTile,
                title: Text("광고", style: Theme.of(context).textTheme.bodyText1,),
                onChanged: (val) {
                  print("Radio Tile pressed $val");
                  setSelectedRadioTile(val);
                },
                activeColor: Colors.red,
                selected: false,
              ),
              RadioListTile(
                value: 3,
                groupValue: selectedRadioTile,
                title: Text("스팸", style: Theme.of(context).textTheme.bodyText1,),
                onChanged: (val) {
                  print("Radio Tile pressed $val");
                  setSelectedRadioTile(val);
                },
                activeColor: Colors.red,
                selected: false,
              ),   
              Spacer(),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    }, 
                    child: Text('취소', style: Theme.of(context).textTheme.button,)
                  ),            
                  FlatButton(
                    onPressed: () {
                      postPolice();
                    }, 
                    child: Text('신고', style: Theme.of(context).textTheme.button.copyWith(color: Colors.red, fontWeight: FontWeight.w900),)
                  ),            
                ],
              ),
              SizedBox(height: 24.0,),
            ],
          ),
        ),
        Visibility (
          visible: _showResult,
          child: Positioned(
            top: contentHeight/3,
            left: 17,
            width: 300,
            height: contentHeight/2,
            child: Container(
              width: 300,
              color: Theme.of(context).dialogBackgroundColor,
              child: Center(
                child: Text(
                  _resultMsg,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}