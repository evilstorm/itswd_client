import 'package:flutter/material.dart';

class SliverHeader extends SliverPersistentHeaderDelegate {

  Widget child;
  double minHeight;
  double maxHeight;
  SliverHeader({
    @required this.child,
    this.minHeight = kToolbarHeight,
    this.maxHeight
  });

   
  @override
  double get maxExtent => maxHeight;

  @override
  double get minExtent => minHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

}