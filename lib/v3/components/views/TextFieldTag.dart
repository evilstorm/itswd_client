import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelTag.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/SearchTagProvider.dart';

import 'package:provider/provider.dart';

//TODO 포커스 사라질 때 Search 초기화 
class TextFieldTag extends StatefulWidget {
  final double witdh;
  final double height;

  TextFieldTag({
    Key key,
    this.witdh,
    this.height  
  }): super(key: key);

  _TextFieldTagState state = _TextFieldTagState();
  @override
  _TextFieldTagState createState() => state;

  List<String> getTags() {
    return state.getTags();
  }
  String getText() {
    return state.getText();
  }

}

class _TextFieldTagState extends State<TextFieldTag> {
  var _watcher = TextEditingController();
  SearchTagProvider _searchTagProvider;

  bool _searchTag = false;
  String _searchTagWord = "";
  int _searchTagStartPos = 0;

  int _prevCursorPos = 0;
  var inputMessage = List();
  var tagPos = -1;

  @override
  void initState() {
    super.initState();
  }

  List<String> getTags() {
    List<String> tagList = List();
    int length = _watcher.text.length;
    bool tagStart = false;
    var sb = StringBuffer();
    for (int i=0; i<length; i++) {
      if(_watcher.text[i] == " " || i == (length-1)) {
        if(tagStart) {
          if(_watcher.text[i] != " ") {
            sb.write(_watcher.text[i]);
          }
          tagList.add(sb.toString());
          sb.clear();
        }
        tagStart = false;
      } else if(_watcher.text[i] == "#" || tagStart) {
        if(tagStart) {
          sb.write(_watcher.text[i]);
        }
        tagStart = true;
      }
    }
    return tagList;
  }

  String getText() {
    return _watcher.text;
  }

  void checkTagArea() {
    var currentPos = _watcher.selection.base.offset;
    var prevWord = "";

    //테그의 끝은 띄워쓰기이다.
    inputMessage = _watcher.text.split(" ");
    int wordLength = 0;
    for(int i =0; i<inputMessage.length; i++) {
      wordLength += inputMessage[i].length;
      if(i != 0) {
        //앞의 띄워쓰기 한칸 추가 한다. 
        wordLength += 1;
      }
      if(wordLength >= _prevCursorPos) {
        prevWord = inputMessage[i];
      }

      if(wordLength >= currentPos) {
        tagPos = i;
        if(inputMessage[i].contains("#")) {
          _searchTagStartPos = inputMessage[i].indexOf("#");
          var _ = inputMessage[i].split("#");
          if(_.length == 1) {
            clearSearchTag();
            return;
          } else if(_.length == 2) {
            if(_[1] == "") {
              return;
            }
          }


          setState(() {
            _searchTag = true;
            _searchTagWord = _[_.length-1];
          });
          doSearch();
        } else {
          clearSearchTag();
        }
        break;
      }
    }
  }

  void doSearch() {
    _searchTagProvider.searchTag(_searchTagWord);
  }
  
  @override
  void dispose() {
    _watcher.dispose();
    super.dispose();
  }

  Widget tagList(BuildContext context) {
    Print.i("tagList!!!!!!!!");
    if(_searchTagProvider.state == ProviderState.DONE) {
      if(_searchTagProvider.tagList?.length == 0) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('$_searchTagWord 로 등록된 뽐이 없습니다.'),
        );
      }
    }
    return Container(
      color: Theme.of(context).dialogBackgroundColor,
      width: widget.witdh,
      height: 200,
      child: Column(
        children: [
          Visibility(
            visible: _searchTagProvider.state == ProviderState.PROGRESS,
            child: Text('$_searchTagWord 검색 중입니다.'),
          ),
          Container(
            height: 120.0,
            child: ListView.builder(
              itemCount: _searchTagProvider.tagList?.length,
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
                child: tagItems(context, index),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void clearSearchTag() {
    setState(() {
      _searchTag = false;
    });
    _searchTagWord = "";
    _searchTagStartPos = 0;
    _prevCursorPos = 0;
    inputMessage = List();
    tagPos = -1;
  }

  Widget tagItems(BuildContext context, index) {
    Print.i(" TAG ITEMS : $tagItems");
    ModelTag item = _searchTagProvider.tagList[index];
    return GestureDetector(
      onTap: () {
        setState(() {
          _searchTagWord = item.tag;
        });
          
          String temp = inputMessage[tagPos];
          var start = temp.indexOf("#");
          var result = temp.replaceRange(start+1, temp.length, item.tag+" ");
          inputMessage[tagPos] = result;

          int size = inputMessage.length;
          String msg = "";
          for (int i=0; i<size; i++) {
            if(i != 0 ) {
              msg += " ";
            }
            msg += inputMessage[i];
          }
          _watcher.text = msg;
          _watcher.selection = TextSelection.collapsed(offset: _watcher.text.length-1);
          clearSearchTag();
      },
      child: Container(
        height: 50.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('#${item.tag}'),
            Text('게시물 ${item.count}개'),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _searchTagProvider = context.watch<SearchTagProvider>();
    Print.e(" Build!");
    return SafeArea(
      child: Container(
        width: widget.witdh,
        height: widget.height,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextField (
                controller: _watcher,
                style: Theme.of(context).textTheme.bodyText1,
                onTap: () {
                  setState(() {
                    _searchTag = false;
                    _searchTagWord = "";
                  });
                },
                onChanged: (_) {
                  checkTagArea();
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Theme.of(context).accentColor)
                  ),
                  hintText: '',
                  hintStyle: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.grey)
                ),
              ),
              Stack (
                children: <Widget>[
                  Container(
                    child: Visibility (
                      visible: _searchTag,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: tagList(context)
                      ),
                    )
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}