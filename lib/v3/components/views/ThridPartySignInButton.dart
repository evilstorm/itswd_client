import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ThridPartySignInButton extends StatelessWidget {

  final VoidCallback callback;
  final String svgImage;
  final Color color;
  final String title;

  const ThridPartySignInButton({
    Key key,
    this.svgImage,
    this.color,
    this.title,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        padding: EdgeInsets.only(top: 3.0,bottom: 3.0,left: 8.0),
        color: const Color(0xFF000000),
        onPressed: () {
          callback();
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 30,
                child: SvgPicture.asset(
                  svgImage,
                  width: 20,
                  height: 20,
                  color: color,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  title, 
                  style: TextStyle(
                    color: Colors.white, 
                    fontSize: 18, 
                    fontWeight: FontWeight.w500
                    ),
                  )
              ),
            ],
          ),
        )
      ),
    );
  }
}