import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/PolicePop.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:provider/provider.dart';


const String url = 'https://image.flaticon.com/icons/png/512/1489/1489597.png';
class UserIcon extends StatelessWidget {
  final ModelUser user;
  final ModelDress dress;
  final double size;
  final bool showName;
  final bool launchDetail;
  final Function clickListener;
  
  UserProvider userProvider;

  UserIcon({
      Key key, 
      @required this.user,
      this.size = 40.0,
      this.showName = true,
      this.clickListener,
      this.launchDetail = true,
      this.dress
  }):super(key: key);

  void launchUserContent(BuildContext context, ModelUser user) {
    if(user == null) {
      return;
    }

    Navigator.pushNamed(
      context, 
      '/userContent',
      arguments: <String, dynamic> {
        'user': user,
      }
    );


    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => UserContent(user: user,)));
  }

  String getName() {
    if(user == null || user.name == null) {
      return 'Unknown';
    }
    return user.name;
  }

  Widget noneImage() {
    return Icon(
      Icons.tag_faces,
      size: size,
      );
  }

  Widget userImage() {
    if(user == null || user.pictureMe == null) {
      return noneImage();
    } 

    return ClipOval(
      child: CachedNetworkImage(
        imageUrl: user.pictureMe, 
        imageBuilder: (context, imageProvider) => Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover
            )
          ),
        ),
        placeholder: (context, url) => noneImage(),
        errorWidget: (context, url, error) =>  noneImage(),
      )
    );
  }

  void _blockUser() async {
    Print.e(" Block User ID: ${user.id}");
    
    await ApiHelper().patch(
      '/user/block/${user.id}',
      json.encode({
      })
    );

    userProvider.addBlockUser(user.id);
  }

  Widget _userBlockWidget(BuildContext context) {
    return Center(
        child: Container(
          color: Theme.of(context).backgroundColor,
          height: 220.0,
          width: MediaQuery.of(context).size.width - 40.0,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(height: 16.0,),
                Text(
                  '사용자 차단', 
                  style: Theme.of(context).textTheme.subtitle1.copyWith(color: Theme.of(context).accentColor),
                ),
                SizedBox(height: 24.0,),
                Text(
                  '해당 사용자의 컨텐츠를 더 이상 \n확인할 수 없습니다',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(height: 24.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        '취소',
                        style: Theme.of(context).textTheme.button
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                        _blockUser();
                      },
                      child: Text(
                        '차단',
                        style: Theme.of(context).textTheme.button.copyWith(color: Theme.of(context).accentColor)
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      );
  }
  Widget buildBottomSheet(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      height: 200.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
              showDialog(
                context: context,
                builder: (BuildContext context){
                  return _userBlockWidget(context);
                }
              );
            },
            child: Container(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Text(
                  '사용자 신고',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ),          
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
              showDialog(
                context: context,
                builder: (BuildContext context){
                  return PolicePop(dress: dress);
                }
              );
            },
            child: Container(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Text(
                  '계시물 신고하기',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ),
          ),          
        ],
      )
    );
      
  }

  @override
  Widget build(BuildContext context) {
    
    userProvider = Provider.of<UserProvider>(context);

    return GestureDetector(
      onTap: () {
        if(launchDetail) {
          launchUserContent(context, user);
        }

        if(clickListener != null) {
          clickListener(user);
        }
      },
      child: Container(
        child: Row(
          children: <Widget>[
            userImage(),
            Visibility(
              visible: showName == true,
              child: Expanded(
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 8.0,),
                    Text(getName(), style: Theme.of(context).textTheme.bodyText1,),
                    Spacer(),
                    IconButton(
                      iconSize: 30,
                      icon: Icon(
                        Icons.more_vert,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        showModalBottomSheet(
    	                    context: context, 
                          builder: buildBottomSheet

                        );

                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}