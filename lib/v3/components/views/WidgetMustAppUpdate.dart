import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'dart:io';

import 'package:itswd/v3/model/response/ModelAppUpdate.dart';



class WidgetMustAppUpdate extends StatelessWidget {

  final ModelAppUpdate  data;


  WidgetMustAppUpdate({
    Key key,
    this.data
  }):super(key: key);


  void terminate() {
    Print.i('앱 종료');
    exit(0);
  }

  void launchAppStore() {
    //TODO 스토어 이동 추가 해야 함!!!
    Print.i('스토어로 이동');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
      child: Center(
        child: Card(
          color: Theme.of(context).dialogBackgroundColor,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.all(16),
                title: Text(
                  '필수 업데이트',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                subtitle: Padding(
                    padding: EdgeInsets.only(top: 24),
                    child: Text(data.say,
                        style: Theme.of(context).textTheme.bodyText1)),
              ),
              ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      textTheme: Theme.of(context).buttonTheme.textTheme,
                      child: Text('종료', style: Theme.of(context).textTheme.button.copyWith(color: Colors.red),),
                      onPressed: () => terminate(),
                    ),
                    FlatButton(
                      child: Text('업데이트', style: Theme.of(context).textTheme.button,),
                      onPressed: () => launchAppStore(),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),

    );
  }
}