import 'package:flutter/material.dart';

import 'package:expandable/expandable.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:itswd/v3/components/common/AboutDate.dart';
import 'package:itswd/v3/model/response/ModelNotify.dart';



class WidgetNotify extends StatelessWidget {


  final List<ModelNotify>  data;
  final Function callback;
  final bool showTitle;

  const WidgetNotify({
      Key key,
      this.data,
      this.showTitle = true,
      this.callback,
  }): super(key: key);
  

  @override
  Widget build(BuildContext context) {
    int seq = 0;

    return Container(
      decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Visibility(
            visible: showTitle,
            child: Column(
              children: <Widget>[
                SizedBox(height: 44),
                Text('공지사항', style: Theme.of(context).textTheme.headline6),
              ],              
            ),
          ),
          SizedBox(height: 24),
          Expanded(
            child: ListView.builder(
              itemCount:data.length,
              itemBuilder: (context, index) {
                var item = data[index];
                if(index == 0) {
                  seq = item.seq;
                }
                return Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0,0.0, 0.0),
                  child: Card(
                    color: Theme.of(context).dialogBackgroundColor,
                    child: 
                      ExpandablePanel(
                        header: Padding(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                AboutDate().dateFormat.format(item.createdAt), 
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                              Text(item.title, style: Theme.of(context).textTheme.bodyText1),
                            ],
                          )
                        ),
                        collapsed: Text(""),
                        expanded: Html(
                          data: item.say,
                          padding: EdgeInsets.all(16.0),
                          defaultTextStyle: Theme.of(context).textTheme.bodyText2,
                        ),
                    
                      ),
                  ),
                );
              }
            ),
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                textTheme: Theme.of(context).buttonTheme.textTheme,
                child: Text('확인', style: Theme.of(context).textTheme.button),
                onPressed: () => callback(seq),
              ),
            ],
          ),
        ],
      )
    );
  }
}