import 'package:flutter/material.dart';
import 'dart:io';

import 'package:flutter_html/flutter_html.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelTerm.dart';

class WidgetTerm extends StatelessWidget {

  final ModelTerm  data;
  final Function callback;
  final bool justShow;

  WidgetTerm({
    Key key,
    this.data,
    this.justShow = false,
    this.callback
  }):super(key: key);


  void terminate() {
    Print.w('앱 종료');
    exit(0);
  }

  @override
  Widget build(BuildContext context) {
    return Container (
      decoration: BoxDecoration(color: Theme.of(context).backgroundColor),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SizedBox(height: 18),
          Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                SizedBox(height: 16.0,),
                Text(
                  '이용약관', 
                  textAlign: TextAlign.end,
                  textDirection: TextDirection.rtl,
                  style: Theme.of(context).textTheme.headline6),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(color: Theme.of(context).dialogBackgroundColor),
                    margin: const EdgeInsets.only(top: 16.0),
                    child:
                    SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Html(
                        data:data.term,
                        padding: EdgeInsets.all(16.0),
                        defaultTextStyle: Theme.of(context).textTheme.caption
                      ),
                    )                
                  ),
                )
              ]),
              flex: 1),
          SizedBox(height: 36),
          Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('정보동의', style: Theme.of(context).textTheme.headline6),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(color: Theme.of(context).dialogBackgroundColor),
                      margin: const EdgeInsets.only(top: 16.0),
                      child: SingleChildScrollView(
                        child: Html(
                          data: data.userTerm,
                          padding: EdgeInsets.all(16.0),
                          defaultTextStyle: Theme.of(context).textTheme.caption,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              flex: 1),
          ButtonBar(
            children: <Widget>[
              Visibility(
                visible: !justShow,
                child: FlatButton(
                  child: Text('종료', style: Theme.of(context).textTheme.button.copyWith(color: Colors.red),),
                  onPressed: () => terminate(),
                ),
              ),
              FlatButton(
                child: Text(justShow?'확인':'동의',  style: Theme.of(context).textTheme.button),
                onPressed: () => callback(data.seq),
              ),
            ],
          ),
        ],
      ),
    );
  }
}