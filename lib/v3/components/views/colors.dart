import 'dart:ui';

const COLORS_GOLD_GRAIDENT_TYPE_1 = [
  const Color.fromARGB(255, 250, 230, 140),
  const Color.fromARGB(255, 240, 160, 40),
  const Color.fromARGB(255, 250, 240, 140),
  const Color.fromARGB(255, 220, 160, 40),
  const Color.fromARGB(255, 250, 240, 140),
];
const COLORS_GOLD_GRAIDENT_TYPE_2 = [
  const Color.fromARGB(255, 220, 160, 40),
  const Color.fromARGB(255, 250, 240, 140),
  const Color.fromARGB(255, 220, 160, 40), 
  const Color.fromARGB(255, 250, 240, 140),
  const Color.fromARGB(255, 220, 160, 40)
];
const COLORS_GOLD_GRAIDENT_TYPE_3 = [
  const Color.fromARGB(255, 220, 160, 40),
  const Color.fromARGB(255, 250, 230, 140),
  const Color.fromARGB(255, 240, 160, 40),
  const Color.fromARGB(255, 250, 240, 140),
  const Color.fromARGB(255, 250, 240, 140),
];

const COLORS_DARK_GRAIDENT_TYPE_1 = [
  const Color.fromARGB(255, 19, 19, 19),
  const Color.fromARGB(255, 120, 120, 120),
  const Color.fromARGB(255, 66, 66, 66),
  const Color.fromARGB(255, 120, 120, 120),
  const Color.fromARGB(255, 19, 19, 19),
];
const COLORS_DARK_GRAIDENT_TYPE_2 = [
  const Color.fromARGB(255, 120, 120, 120),
  const Color.fromARGB(255, 19, 19, 19),
  const Color.fromARGB(255, 120, 120, 120),
  const Color.fromARGB(255, 19, 19, 19),
  const Color.fromARGB(255, 166, 166, 166),
];
