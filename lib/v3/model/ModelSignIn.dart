import 'dart:convert';

class ModelSignIn {
  String email = null;
  int loginType = -1;
  String name = null;
  String pictureUrl = null;
  
  ModelSignIn({
    this.email,
    this.loginType,
    this.name,
    this.pictureUrl,
  });



  ModelSignIn copyWith({
    String email,
    int loginType,
    String name,
    String pictureUrl,
  }) {
    return ModelSignIn(
      email: email ?? this.email,
      loginType: loginType ?? this.loginType,
      name: name ?? this.name,
      pictureUrl: pictureUrl ?? this.pictureUrl,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'loginType': loginType,
      'name': name,
      'pictureUrl': pictureUrl,
    };
  }

  factory ModelSignIn.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelSignIn(
      email: map['email'],
      loginType: map['loginType'],
      name: map['name'],
      pictureUrl: map['pictureUrl'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ModelSignIn.fromJson(String source) => ModelSignIn.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ModelSignIn(email: $email, loginType: $loginType, name: $name, pictureUrl: $pictureUrl)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelSignIn &&
      o.email == email &&
      o.loginType == loginType &&
      o.name == name &&
      o.pictureUrl == pictureUrl;
  }

  @override
  int get hashCode {
    return email.hashCode ^
      loginType.hashCode ^
      name.hashCode ^
      pictureUrl.hashCode;
  }
}
