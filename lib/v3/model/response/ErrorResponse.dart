class ErrorResponse {
  String message;
  Trace trace;
  
ErrorResponse.fromJson(Map<String, dynamic> json) {
  message = json['message'];
  trace = Trace.fromJson(json['trace']);
  }
}

class Trace {
  int code;
  String title;
  String errmsg;


  Trace.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    title = json['title'];
    errmsg = json['errmsg'];
  }
}