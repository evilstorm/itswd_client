import 'dart:convert';

class ModelAppUpdate {
  int appVer;
  String say;
  bool isMustUpdate;

  ModelAppUpdate({
    this.appVer,
    this.say,
    this.isMustUpdate,
  });
  
  ModelAppUpdate copyWith({
    int appVer,
    String say,
    bool isMustUpdate,
  }) {
    return ModelAppUpdate(
      appVer: appVer ?? this.appVer,
      say: say ?? this.say,
      isMustUpdate: isMustUpdate ?? this.isMustUpdate,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'appVer': appVer,
      'say': say,
      'isMustUpdate': isMustUpdate,
    };
  }

  static ModelAppUpdate fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelAppUpdate(
      appVer: map['appVer'],
      say: map['say'],
      isMustUpdate: map['isMustUpdate'],
    );
  }

  String toJson() => json.encode(toMap());

  static ModelAppUpdate fromJson(String source) => fromMap(json.decode(source));
  static ModelAppUpdate fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() => 'ModelAppUpdate appVer: $appVer, say: $say, isMustUpdate: $isMustUpdate';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelAppUpdate &&
      o.appVer == appVer &&
      o.say == say &&
      o.isMustUpdate == isMustUpdate;
  }

  @override
  int get hashCode => appVer.hashCode ^ say.hashCode ^ isMustUpdate.hashCode;
}
