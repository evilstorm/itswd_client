import 'dart:convert';

import 'package:itswd/v3/model/response/ModelUser.dart';

class ModelDress {
  String id;
  ModelUser owner;
  List<String> images;
  String say;
  bool likeThis;
  bool reportThis;
  int likeCount;
  int replyCount;
  bool isAward;
  int favoritePoint;
  bool showImage = false;
  DateTime createdAt;

  ModelDress({
    this.id,
    this.owner,
    this.images,
    this.say,
    this.likeThis,
    this.reportThis,
    this.likeCount,
    this.replyCount,
    this.isAward,
    this.favoritePoint,
    this.createdAt,
  });



  ModelDress copyWith({
    String id,
    ModelUser owner,
    List<String> images,
    String say,
    bool likeThis,
    bool reportThis,
    int likeCount,
    int replyCount,
    bool isAward,
    int favoritePoint,
    DateTime createdAt,
  }) {
    return ModelDress(
      id: id ?? this.id,
      owner: owner ?? this.owner,
      images: images ?? this.images,
      say: say ?? this.say,
      likeThis: likeThis ?? this.likeThis,
      reportThis: reportThis ?? this.reportThis,
      likeCount: likeCount ?? this.likeCount,
      replyCount: replyCount ?? this.replyCount,
      isAward: isAward?? this.isAward,
      favoritePoint: favoritePoint?? this.favoritePoint,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'owner': owner.toMap(),
      'images': List<dynamic>.from(images.map((x) => x)),
      'say': say,
      'likeThis': likeThis,
      'reportThis': reportThis,
      'likeCount': likeCount,
      'replyCount': replyCount,
      'isAward': isAward,
      'favoritePoint': favoritePoint,
      'createdAt': createdAt,
    };
  }

  static ModelDress fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelDress(
      id: map['_id'],
      owner: ModelUser.fromMap(map['owner']),
      images: List<String>.from(map['images']),
      say: map['say'],
      likeThis: map['likeThis'],
      reportThis: map['reportThis'],
      likeCount: map['likeCount'],
      replyCount: map['replyCount'],
      isAward: map['isAward'],
      favoritePoint: map['favoritePoint'],
      createdAt: DateTime.parse(map['createdAt']),
    );
  }

  String toJson() => json.encode(toMap());

  static ModelDress fromJson(String source) => fromMap(json.decode(source));
  static ModelDress fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() {
    return 'ModelDress id: $id, owner: $owner, images: $images, say: $say, likeThis: $likeThis, reportThis:$reportThis, likeCount: $likeCount, replyCount: $replyCount, isAward: $isAward, favoritePoint: $favoritePoint, createdAt: $createdAt';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelDress &&
      o.id == id &&
      o.owner == owner &&
      o.images == images &&
      o.say == say &&
      o.likeThis == likeThis &&
      o.reportThis == reportThis &&
      o.likeCount == likeCount &&
      o.replyCount == replyCount &&
      o.isAward == isAward &&
      o.favoritePoint == favoritePoint &&
      o.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return 
      id.hashCode ^
      owner.hashCode ^
      images.hashCode ^
      say.hashCode ^
      likeThis.hashCode ^
      reportThis.hashCode ^
      likeCount.hashCode ^
      replyCount.hashCode ^
      isAward.hashCode ^
      favoritePoint. hashCode ^
      createdAt.hashCode;
  }
}
