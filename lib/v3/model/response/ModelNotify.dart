import 'dart:convert';

class ModelNotify {
  bool appStop;
  bool important;
  bool show;
  String id;
  String title;
  String say;
  DateTime createdAt;
  int seq;

  ModelNotify({
    this.appStop,
    this.important,
    this.show,
    this.id,
    this.title,
    this.say,
    this.createdAt,
    this.seq,
  });

  ModelNotify copyWith({
    bool appStop,
    bool important,
    bool show,
    String id,
    String title,
    String say,
    DateTime createdAt,
    int seq,
  }) {
    return ModelNotify(
      appStop: appStop ?? this.appStop,
      important: important ?? this.important,
      show: show ?? this.show,
      id: id ?? this.id,
      title: title ?? this.title,
      say: say ?? this.say,
      createdAt: createdAt ?? this.createdAt,
      seq: seq ?? this.seq,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'appStop': appStop,
      'important': important,
      'show': show,
      'id': id,
      'title': title,
      'say': say,
      'createdAt': createdAt,
      'seq': seq,
    };
  }

  static ModelNotify fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelNotify(
      appStop: map['appStop'],
      important: map['important'],
      show: map['show'],
      id: map['_id'],
      title: map['title'],
      say: map['say'],
      createdAt: DateTime.parse(map['createdAt']),
      seq: map['seq'],
    );
  }

  String toJson() => json.encode(toMap());

  static ModelNotify fromJson(String source) => fromMap(json.decode(source));
  static ModelNotify fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() {
    return 'ModelNotify appStop: $appStop, important: $important, show: $show, id: $id, title: $title, say: $say, createdAt: $createdAt, seq: $seq';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelNotify &&
      o.appStop == appStop &&
      o.important == important &&
      o.show == show &&
      o.id == id &&
      o.title == title &&
      o.say == say &&
      o.createdAt == createdAt &&
      o.seq == seq;
  }

  @override
  int get hashCode {
    return appStop.hashCode ^
      important.hashCode ^
      show.hashCode ^
      id.hashCode ^
      title.hashCode ^
      say.hashCode ^
      createdAt.hashCode ^
      seq.hashCode;
  }
}
