import 'dart:convert';

class ModelReceivedAct {
  int totalCount;
  int awardCount;
  int receiveLikeCount;
  int receiveReplyCount;
  ModelReceivedAct({
    this.totalCount,
    this.awardCount,
    this.receiveLikeCount,
    this.receiveReplyCount,
  });

  

  ModelReceivedAct copyWith({
    int totalCount,
    int awardCount,
    int receiveLikeCount,
    int receiveReplyCount,
  }) {
    return ModelReceivedAct(
      totalCount: totalCount ?? this.totalCount,
      awardCount: awardCount ?? this.awardCount,
      receiveLikeCount: receiveLikeCount ?? this.receiveLikeCount,
      receiveReplyCount: receiveReplyCount ?? this.receiveReplyCount,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'totalCount': totalCount,
      'awardCount': awardCount,
      'receiveLikeCount': receiveLikeCount,
      'receiveReplyCount': receiveReplyCount,
    };
  }

  static ModelReceivedAct fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelReceivedAct(
      totalCount: map['totalCount'],
      awardCount: map['awardCount'],
      receiveLikeCount: map['receiveLikeCount'],
      receiveReplyCount: map['receiveReplyCount'],
    );
  }

  String toJson() => json.encode(toMap());

  static ModelReceivedAct fromJson(String source) => fromMap(json.decode(source));
  static ModelReceivedAct fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() {
    return 'ModelReceivedAct(totalCount: $totalCount, awardCount: $awardCount, receiveLikeCount: $receiveLikeCount, receiveReplyCount: $receiveReplyCount)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelReceivedAct &&
      o.totalCount == totalCount &&
      o.awardCount == awardCount &&
      o.receiveLikeCount == receiveLikeCount &&
      o.receiveReplyCount == receiveReplyCount;
  }

  @override
  int get hashCode {
    return totalCount.hashCode ^
      awardCount.hashCode ^
      receiveLikeCount.hashCode ^
      receiveReplyCount.hashCode;
  }
}
