import 'dart:convert';

import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';

class ModelReply {
  ModelUser owner;
  String say;
  DateTime createdAt;
  ModelReply({
    this.owner,
    this.say,
    this.createdAt,
  });

  

  ModelReply copyWith({
    ModelUser owner,
    ModelDress parent,
    String say,
    DateTime createdAt,
  }) {
    return ModelReply(
      owner: owner ?? this.owner,
      say: say ?? this.say,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'owner': owner.toMap(),
      'say': say,
      'createdAt': createdAt,
    };
  }

  static ModelReply fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelReply(
      owner: ModelUser.fromMap(map['owner']),
      say: map['say'],
      createdAt: DateTime.parse(map['createdAt']),
    );
  }

  String toJson() => json.encode(toMap());

  static ModelReply fromJson(String source) => fromMap(json.decode(source));
  static ModelReply fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() {
    return 'ModelReply owner: $owner, say: $say, createdAt: $createdAt';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelReply &&
      o.owner == owner &&
      o.say == say &&
      o.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return owner.hashCode ^
      say.hashCode ^
      createdAt.hashCode;
  }
}
