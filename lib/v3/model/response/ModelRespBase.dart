import 'dart:convert';

class ModelResponseBase{
 bool success;
 String message;
 String erros;


 ModelResponseBase({
  this.success,
  this.message,
  this.erros,
 });


 factory ModelResponseBase.fromJson(Map<String,dynamic> parsedJson){
  return ModelResponseBase(
   success : parsedJson['success'] == null ? false : parsedJson['success'],
   message : parsedJson['message'],
   erros : parsedJson['erros'],
  );
 }

 static List<ModelResponseBase> listFromJson(List<dynamic> list) {
  List<ModelResponseBase> rows = list.map((i) => ModelResponseBase.fromJson(i)).toList();
  return rows;
 }

 static List<ModelResponseBase> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<ModelResponseBase>((json) => ModelResponseBase.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'success' : success,
   'message' : message,
   'erros' : erros,
 };

}