import 'dart:convert';

import 'package:itswd/v3/model/response/ModelUser.dart';

class ModelSetting {
  bool systemAlarm;
  bool likeAlarm;
  bool replyAlarm;
  bool emailSend;
  
  ModelSetting({
    this.systemAlarm,
    this.likeAlarm,
    this.replyAlarm,
    this.emailSend,
  });

  

  ModelSetting copyWith({
    ModelUser owner,
    bool systemAlarm,
    bool likeAlarm,
    bool replyAlarm,
    bool emailSend,
  }) {
    return ModelSetting(
      systemAlarm: systemAlarm ?? this.systemAlarm,
      likeAlarm: likeAlarm ?? this.likeAlarm,
      replyAlarm: replyAlarm ?? this.replyAlarm,
      emailSend: emailSend ?? this.emailSend,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'systemAlarm': systemAlarm,
      'likeAlarm': likeAlarm,
      'replyAlarm': replyAlarm,
      'emailSend': emailSend,
    };
  }

  static ModelSetting fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    if(!map.containsKey('likeAlarm')) {
      return null;
    }

    return ModelSetting(
      systemAlarm: map['systemAlarm'],
      likeAlarm: map['likeAlarm'],
      replyAlarm: map['replyAlarm'],
      emailSend: map['emailSend'],
    );
  }

  String toJson() => json.encode(toMap());

  static ModelSetting fromJson(String source) => fromMap(json.decode(source));
  static ModelSetting fromJsonDynamic(dynamic source) {
    if(source is Map) {
      return fromMap(source);
    }
    return null;
  }

  @override
  String toString() {
    return 'ModelSetting systemAlarm: $systemAlarm, likeAlarm: $likeAlarm, replyAlarm: $replyAlarm, emailSend: $emailSend';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelSetting &&
      o.systemAlarm == systemAlarm &&
      o.likeAlarm == likeAlarm &&
      o.replyAlarm == replyAlarm &&
      o.emailSend == emailSend;
  }

  @override
  int get hashCode {
    return 
      systemAlarm.hashCode ^
      likeAlarm.hashCode ^
      replyAlarm.hashCode ^
      emailSend.hashCode;
  }
}
