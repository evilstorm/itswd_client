import 'package:itswd/v3/model/response/ModelAppUpdate.dart';
import 'package:itswd/v3/model/response/ModelNotify.dart';
import 'package:itswd/v3/model/response/ModelTerm.dart';
import 'package:itswd/v3/model/response/ResponseDefault.dart';

class ModelSplash extends ResponseDefault{
  List<ModelNotify> notify;
  ModelTerm term;
  ModelAppUpdate appUpdate;

  setData(Map<String, dynamic> json) {
    if (json['data']['notify'] != null) {
      notify = List<ModelNotify>();
      json['data']['notify'].forEach((v) {
        notify.add(ModelNotify.fromJsonDynamic(v));
      });
    }
    if(json['data']['term'] != null) {
      term = ModelTerm.fromJsonDynamic(json['data']['term']);
    }
    if(json['data']['update'] != null) {
      appUpdate = ModelAppUpdate.fromJsonDynamic(json['data']['update']);
    }
  }
  ModelSplash.fromJson(Map<String, dynamic> json) {
    responseParser(json);
  }
}