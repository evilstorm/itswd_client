import 'dart:convert';

class ModelTag {
  String id;
  int count;
  String tag;
  ModelTag({
    this.id,
    this.count,
    this.tag,
  });

  

  ModelTag copyWith({
    String id,
    int count,
    String tag,
  }) {
    return ModelTag(
      id: id ?? this.id,
      count: count ?? this.count,
      tag: tag ?? this.tag,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'count': count,
      'tag': tag,
    };
  }

  static ModelTag fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return ModelTag(
      id: map['_id'],
      count: map['count'],
      tag: map['tag'],
    );
  }

  String toJson() => json.encode(toMap());

  static ModelTag fromJson(String source) => fromMap(json.decode(source));
  static ModelTag fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() => 'ModelTag(id: $id, count: $count, tag: $tag)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelTag &&
      o.id == id &&
      o.count == count &&
      o.tag == tag;
  }

  @override
  int get hashCode => id.hashCode ^ count.hashCode ^ tag.hashCode;
}
