import 'dart:convert';

class ModelTerm {
  String id;
  int seq;
  bool release;
  String term;
  String userTerm;
  DateTime createdAt;

  ModelTerm({
    this.id,
    this.seq,
    this.release,
    this.term,
    this.userTerm,
    this.createdAt,
  });

  


  ModelTerm copyWith({
    String id,
    int seq,
    bool release,
    String term,
    String userTerm,
    DateTime createdAt,
  }) {
    return ModelTerm(
      id: id ?? this.id,
      seq: seq ?? this.seq,
      release: release ?? this.release,
      term: term ?? this.term,
      userTerm: userTerm ?? this.userTerm,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'seq': seq,
      'release': release,
      'term': term,
      'userTerm': userTerm,
      'createdAt': createdAt,
    };
  }

  static ModelTerm fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    return ModelTerm(
      id: map['_id'],
      seq: map['seq'],
      release: map['release'],
      term: map['term'],
      userTerm: map['userTerm'],
      createdAt: DateTime.parse(map['createdAt']),
    );
  }

  String toJson() => json.encode(toMap());

  static ModelTerm fromJson(String source) => fromMap(json.decode(source));
  static ModelTerm fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() {
    return 'ModelTerm _id: $id, seq: $seq, release: $release, term: $term, userTerm: $userTerm, createdAt: $createdAt';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelTerm &&
      o.id == id &&
      o.seq == seq &&
      o.release == release &&
      o.term == term &&
      o.userTerm == userTerm &&
      o.createdAt == createdAt;
  }

  @override
  int get hashCode {
    return id.hashCode ^
      seq.hashCode ^
      release.hashCode ^
      term.hashCode ^
      userTerm.hashCode ^
      createdAt.hashCode;
  }
}
