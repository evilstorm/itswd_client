import 'dart:convert';

import 'package:itswd/v3/model/response/ModelSetting.dart';

class ModelUser {
  String id;
  String email;
  String name;
  int secureLevel;
  int loginType;
  String pictureMe = "none"; 
  String aboutMe = "아직 소개 글이 없습니다."; 
  int step;
  bool useStop;
  int stopReason;
  String pushToken;
  bool checkEmail; 
  int hasAward; 
  int dressCount;
  int replyCount;
  int likeDressCount;
  ModelSetting setting;
  List<String> blockUser;
  List<String> reportDress;

  ModelUser({
    this.id,
    this.email,
    this.name,
    this.secureLevel,
    this.loginType,
    this.pictureMe,
    this.aboutMe,
    this.step,
    this.useStop,
    this.stopReason,
    this.pushToken,
    this.checkEmail,
    this.hasAward,
    this.dressCount,
    this.replyCount,
    this.likeDressCount,
    this.setting,
    this.blockUser,
    this.reportDress,
  });

  

  ModelUser copyWith({
    String id,
    String email,
    String name,
    int secureLevel,
    int loginType,
    String pictureMe,
    String aboutMe,
    int step,
    bool useStop,
    int stopReason,
    String pushToken,
    bool checkEmail,
    int hasAward,
    int dressCount,
    int replyCount,
    int likeDressCount,
    ModelSetting setting,
    List<String> blockUser,
    List<String> reportDress,
  }) {
    return ModelUser(
      id: id ?? this.id,
      email: email ?? this.email,
      name: name ?? this.name,
      secureLevel: secureLevel ?? this.secureLevel,
      loginType: loginType ?? this.loginType,
      pictureMe: pictureMe ?? this.pictureMe,
      aboutMe: aboutMe ?? this.aboutMe,
      step: step ?? this.step,
      useStop: useStop ?? this.useStop,
      stopReason: stopReason ?? this.stopReason,
      pushToken: pushToken ?? this.pushToken,
      checkEmail: checkEmail ?? this.checkEmail,
      hasAward: hasAward ?? this.hasAward,
      dressCount: dressCount ?? this.dressCount,
      replyCount: replyCount ?? this.replyCount,
      likeDressCount: likeDressCount ?? this.likeDressCount,
      setting: setting ?? this.setting,
      blockUser: blockUser ?? this.blockUser,
      reportDress: reportDress ?? this.reportDress,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'email': email,
      'name': name,
      'secureLevel': secureLevel,
      'loginType': loginType,
      'pictureMe': pictureMe,
      'aboutMe': aboutMe,
      'step': step,
      'useStop': useStop,
      'stopReason': stopReason,
      'pushToken': pushToken,
      'checkEmail': checkEmail,
      'hasAward': hasAward,
      'dressCount': dressCount,
      'replyCount': replyCount,
      'likeDressCount': likeDressCount,
      'setting': setting?.toMap(),
      'blockUser': List<dynamic>.from(blockUser.map((x) => x)),
      'reportDress': List<dynamic>.from(reportDress.map((x) => x)),
    };
  }

  static ModelUser fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    dynamic setting;
    if(map.containsKey('setting')) {
      if(map['setting'] != null ) {
        if(map['setting'] is List ) {
          setting = map['setting'][0];
        } else {
          setting = map['setting'];
        }
      }
    }

    return ModelUser(
      id: map['_id'],
      email: map['email'],
      name: map['name'],
      secureLevel: map['secureLevel'],
      loginType: map['loginType'],
      pictureMe: map['pictureMe'],
      aboutMe: map['aboutMe'],
      step: map['step'],
      useStop: map['useStop'],
      stopReason: map['stopReason'],
      pushToken: map['pushToken'],
      checkEmail: map['checkEmail'],
      hasAward: map['hasAward'],
      dressCount: map['dressCount'],
      replyCount: map['replyCount'],
      likeDressCount: map['likeDressCount'],
      setting: ModelSetting.fromJsonDynamic(setting),
      blockUser: map.containsKey('blockUser')? List<String>.from(map['blockUser']): [],
      reportDress: map.containsKey('reportDress')? List<String>.from(map['reportDress']): []
    );
  }

  String toJson() => json.encode(toMap());

  static ModelUser fromJson(String source) => fromMap(json.decode(source));
  static ModelUser fromJsonDynamic(dynamic source) => fromMap(source);

  @override
  String toString() {
    return 'ModelUser(id: $id, email: $email, name: $name, secureLevel: $secureLevel, loginType:, $loginType, pictureMe: $pictureMe, aboutMe: $aboutMe, step: $step, useStop: $useStop, stopReason: $stopReason, pushToken: $pushToken, checkEmail: $checkEmail, hasAward: $hasAward, dressCount: $dressCount, replyCount: $replyCount, likeDressCount: $likeDressCount, setting: $setting)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
  
    return o is ModelUser &&
      o.id == id &&
      o.email == email &&
      o.name == name &&
      o.secureLevel == secureLevel &&
      o.loginType == loginType &&
      o.pictureMe == pictureMe &&
      o.aboutMe == aboutMe &&
      o.step == step &&
      o.useStop == useStop &&
      o.stopReason == stopReason &&
      o.pushToken == pushToken &&
      o.checkEmail == checkEmail &&
      o.hasAward == hasAward &&
      o.dressCount == dressCount &&
      o.replyCount == replyCount &&
      o.likeDressCount == likeDressCount &&
      o.setting == setting &&
      o.blockUser == blockUser &&
      o.reportDress == reportDress;
  }

  @override
  int get hashCode {
    return id.hashCode ^
      email.hashCode ^
      name.hashCode ^
      secureLevel.hashCode ^
      loginType.hashCode ^
      pictureMe.hashCode ^
      aboutMe.hashCode ^
      step.hashCode ^
      useStop.hashCode ^
      stopReason.hashCode ^
      pushToken.hashCode ^
      checkEmail.hashCode ^
      hasAward.hashCode ^
      dressCount.hashCode ^
      replyCount.hashCode ^
      likeDressCount.hashCode ^
      setting.hashCode ^
      blockUser.hashCode ^
      reportDress.hashCode;
  }
}