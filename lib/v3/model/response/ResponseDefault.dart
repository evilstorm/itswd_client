
import 'package:itswd/v3/model/response/ErrorResponse.dart';

abstract class ResponseDefault {
  String status;
  int code;
  ErrorResponse errors;
  String message;

  void responseParser(Map<String, dynamic> json) {
     if(json == null || json.isEmpty) {
      return;
    }
    status = json['status'];
    code = json['code'];
    if(json['error'] != null) {
      errors = ErrorResponse.fromJson(json['error']);
    }
    if(code == 200) {
      setData(json);
    } else {
      if (json['data'] != null) { 
        message = json['data']['message'];
      }
    }
  }

  setData(Map<String, dynamic> json);
}