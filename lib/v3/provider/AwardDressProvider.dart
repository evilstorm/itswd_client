
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';

class AwardDressProvider {

  List<ModelDress> _awardList;
  String _errMsg;

  //어워드 수상자 리스트(API 캣수 콜 )
  List<ModelDress> get awardList => _awardList;
  String get message => _errMsg;

  Future<List<ModelDress>> reqDressCount(int count) async {

    final response = await ApiHelper().get('/dress/award/$count');

    if (response['data']['dress'] != null) {
      _awardList = List<ModelDress>();
      response['data']['dress'].forEach((v) {
        _awardList.add(ModelDress.fromJsonDynamic(v));
      });
    }

    return _awardList;


  }
}