import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

abstract class BaseDressProvider extends BaseProvider {

  List<ModelDress> getDressList();
  void readMore();
  bool canReadMore();

}