import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum ProviderState {INIT, START, PROGRESS, DONE, DATA_CHANGE, EXCEPTION}

class BaseProvider with ChangeNotifier, DiagnosticableTreeMixin  {

  ProviderState _state = ProviderState.INIT;
  ProviderState get state => _state;

  void stateChangeNotify(ProviderState state) {
    _state = state;
    notifyListeners();
  }
  
  void stateChangeNotNotify(ProviderState state) {
    _state = state;
  }

}