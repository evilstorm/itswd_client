import 'package:itswd/v3/provider/BaseProvider.dart';

class DailyDressEventProvider extends BaseProvider {

  ///
  ///0: 그리드 뷰, 1: 리스트 뷰 
  int _isGridView = 0;
  int _selectedPos = 0;
  double _listStartOffSet = 0;

  int get isGridView => _isGridView;
  void showGridView() {
    _selectedPos = 0;
    _listStartOffSet = 0;
    _isGridView = 0;
    stateChangeNotify(ProviderState.DATA_CHANGE);
  }

  void showListView(int pos, double offset) {
    _selectedPos = pos;
    _listStartOffSet = offset;
    _isGridView = 1;

    stateChangeNotify(ProviderState.DATA_CHANGE);
  } 
 
  
  int get selectedPos => _selectedPos;
  set setSelectedPos(int pos) => _selectedPos = pos;

  double get listStartOffSet => _listStartOffSet;
  set setListStartOffSet(double offset) => _listStartOffSet = offset;


}