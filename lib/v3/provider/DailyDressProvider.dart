import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/DressProvider.dart';

class DailyDressProvider extends BaseProvider {

  ApiHelper _apiHelper = ApiHelper();

  List<ModelDress> _dressList;
  List<ModelDress> _awardList;
  bool _canReadMore = true;
  String _currentDate;
  String _errMsg;

  List<ModelDress> get dressList => _dressList;
  List<ModelDress> get awardList => _awardList;
  bool get canReadMore => _canReadMore;
  String get currentDate => _currentDate;

  String get message => _errMsg;

  int page = 0;
  int requestTime = 0;
  int year = 0;
  int month = 0;
  int date = 0;
  int dressListCount = 0;
  DailyDressProvider() {
    var today = DateTime.now();
    this.year = today.year;
    this.month = today.month;
    this.date = today.day;
    reqDress(today.year, today.month, today.day);
  }

  
  void readMore() {
    Print.i("Provider ReadMOre");
    page += 1;
    reqDress(year, month, date, requestTime: requestTime, page: page);
  }

  void setTodayDress(ModelDress todayDress) {
    _dressList.insert(0, todayDress);
  }

  Future<void> reqDress(int year, int month, int date, {int requestTime = 0, int page = 0}) async {
    int resDressCount = 0;
    _currentDate = "$year/$month/$date";
    
    stateChangeNotNotify(ProviderState.PROGRESS);

    this.year = year;
    this.month = month;
    this.date = date;
    this.page = page;

    if(requestTime == 0 || page == 0) {
      requestTime = DateTime.now().microsecondsSinceEpoch;
    }
    

    var response = await ApiHelper().get(
      '/dress/$year/$month/$date/$requestTime/$page'
      );

    if(response['code'] == 200) {
      
      if(page == 0) {
        _awardList = List<ModelDress>();
        _dressList = List<ModelDress>();
      }

      if (response['data']['award'] != null) {
        response['data']['award'].forEach((v) {
          _awardList.add(ModelDress.fromJsonDynamic(v));
        });
      }
      

      if (response['data']['dress'] != null) {
        response['data']['dress'].forEach((v) {
          ++resDressCount;
          _dressList.add(ModelDress.fromJsonDynamic(v));
        });
      }
    }

    if(page > 0) {
      if(dressListCount != resDressCount) {
        _canReadMore = false;
      }
      stateChangeNotify(ProviderState.DATA_CHANGE);
    } else {
      _canReadMore = true;
      dressListCount = resDressCount;
      stateChangeNotify(ProviderState.DONE);
    }
    
  }

}