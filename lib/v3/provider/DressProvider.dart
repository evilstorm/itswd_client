import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseDressProvider.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class DressProvider extends BaseDressProvider {

  List<ModelDress> _dressList = [];
  int _dressListCount = 0;
  bool _canReadMore = true;
  int _pageCount = -1;

  List<ModelDress> get dressList => _dressList;

  @override
  bool canReadMore() {
    return _canReadMore;
  }

  @override
  List<ModelDress> getDressList() {
    return _dressList;
  }

  @override
  void readMore() {
    reqDress(_getUrl());
  }



  bool hasReadMore = false;
  String url;

  void clear() {
    _pageCount = -1;
    _canReadMore = true;
    _dressListCount = 0;
    _dressList.clear();
  }

  DressProvider(String url, bool hasReadMore) {
    this.hasReadMore = hasReadMore;
    this.url = url;
    reqDress(_getUrl());
  }

  String _getUrl() {
    if(hasReadMore) {
      return '$url/${_pageCount+=1}';
    } else {
      return url;
    }
  }

  void reqDress(String url) async {
    stateChangeNotify(ProviderState.PROGRESS);

    int resDressCount = 0;

    try {
      var response = await ApiHelper().get(url);

      if(response['code'] != 200) {
        stateChangeNotify(ProviderState.EXCEPTION);
        return null;
      }

      if (response['data']['dress'] != null) {
        response['data']['dress'].forEach((v) {
          
          resDressCount += 1;
          _dressList.add(ModelDress.fromJsonDynamic(v));

        });
      }

      if(_dressListCount == 0) {
        _dressListCount = resDressCount;
      } else {
        if(_dressListCount != resDressCount) {
          _canReadMore = false;
        }
      }
      
      stateChangeNotify(ProviderState.DONE);

    } catch (e) {
      Print.e(e);
      stateChangeNotify(ProviderState.EXCEPTION);
    }
  }
  
}