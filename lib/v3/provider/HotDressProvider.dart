import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseDressProvider.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class HotDressProvider extends BaseDressProvider {

  List<ModelDress> _hotDress;

  int _page = 0;
  int _dressListCount = 0;
  bool _canReadMore = true;
  String _tag;

  String get tag => _tag;

  @override
  bool canReadMore() {
    return _canReadMore;
  }

  @override
  List<ModelDress> getDressList() {
    return _hotDress;
  }

  @override
  void readMore() {
    _page+=1;
    reqHotDress();
  }


  HotDressProvider() {
    reqHotDress();
  }

  void reqHotDress() async {
    int resDressCount = 0;

    stateChangeNotNotify(ProviderState.PROGRESS);

    try {
      final response = await ApiHelper().get('/tag/search/favoriate/$_page');
      if(response['code'] != 200) {
        if(_hotDress.length > 0) {
          _canReadMore = false;
          return;
        }
        _hotDress = List<ModelDress>();
      } else {
        if (response['data'] != null) {
          _hotDress = List<ModelDress>();
          response['data'].forEach((v) {
            resDressCount++;
            _hotDress.add(ModelDress.fromJsonDynamic(v));
          });
        }
      }

      if(_page > 0) {
        if(_dressListCount != resDressCount) {
          _canReadMore = false;
        }
        stateChangeNotify(ProviderState.DATA_CHANGE);
      } else {
        _canReadMore = true;
        _dressListCount = resDressCount;
        stateChangeNotify(ProviderState.DONE);
      }
    
      stateChangeNotify(ProviderState.DONE);

    } on Exception catch(e) {
      Print.e(e);
      stateChangeNotify(ProviderState.EXCEPTION);
    }
  }


  void clear() {
    _page = 0;
    _tag = null; 
    _dressListCount = 0;
    _canReadMore = true;

  }
    
}