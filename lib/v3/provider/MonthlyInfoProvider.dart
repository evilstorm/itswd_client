import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class MonthlyInfoProvider extends BaseProvider {
  String userId;
  int year;
  int month;
  int day;

  List<ModelDress> _dressList = [];
  List<ModelDress> get dressList => _dressList;

  
  MonthlyInfoProvider({
    this.userId,
    this.year,
    this.month,
    this.day,
  }) { 
    getMonthlyDress(userId, year, month);
  }

  void getMonthlyDress(userId, year, month) async {

    this.userId = userId;
    this.year = year;
    this.month = month;

    stateChangeNotify(ProviderState.PROGRESS);

    try {
      
      var response = await ApiHelper().get('/dress/monthly/user/$userId/$year/$month');

      if(response['code'] != 200) {
        stateChangeNotify(ProviderState.EXCEPTION);
        return null;
      }

      if (response['data']['dress'] != null) {
        response['data']['dress'].forEach((v) {
          
          _dressList.add(ModelDress.fromJsonDynamic(v));

        });
      }

      stateChangeNotify(ProviderState.DONE);

    } catch(e) {
      Print.e(e);
      stateChangeNotify(ProviderState.EXCEPTION);

    }    
  }

  
  
}