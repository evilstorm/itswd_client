import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelReply.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class ReplyProvider extends BaseProvider {
  String dressId;

  ReplyProvider({this.dressId}) {
    Print.e(" Reply Provider Created!");
    
    reqReply();
  }

  List<ModelReply> _resp = List();
  String _errMsg;

  List<ModelReply> get resp => _resp;
  String get message => _errMsg;

  Future<List<ModelReply>> reqReply() async {

    try {

      var response = await ApiHelper().get('/reply/dress/$dressId');

      if(response['code'] != 200) {
        notifyListeners();
        return _resp;
      }

      if (response['data'] != null) {
        _resp = List<ModelReply>();
        response['data'].forEach((v) {
          _resp.add(ModelReply.fromJsonDynamic(v));
        });
      }
      notifyListeners();
      return _resp;
    } on Exception catch(e) {
      _errMsg = e.toString();
      Print.e(e);
    }
  }

  Future<ModelReply> postReply(data) async {
    ModelReply result;

    var response = await ApiHelper().post('/reply', data);

    if (response['data'] != null) {
      result = ModelReply.fromJsonDynamic(response['data']);
      _resp.insert(0, result);
    }
    
    notifyListeners();
    return result;
  }
}