
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseDressProvider.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class SearchDressProvider extends BaseDressProvider {

  List<ModelDress> _searchDress = new List();

  int _page = 0;
  int _dressListCount = 0;
  bool _canReadMore = true;
  String _tag;

  List<ModelDress> get searchDress => _searchDress;
  String get tag => _tag;

  SearchDressProvider({tag}) {
    if(tag != null) {
      _tag = tag;
      searchDressWithTag(tag);
    }
  }


  @override
  bool canReadMore() {
    return _canReadMore;
  }

  @override
  List<ModelDress> getDressList() {
    return _searchDress;
  }

  @override
  void readMore() {
    searchDressWithTag(_tag, page: _page+=1);
  }


  void searchDressWithTag(String tag, {page = 0}) async {
    Print.d(" searchDressWithTag: $tag");
    if(tag.startsWith("#")) {
      tag = tag.replaceFirst("#", "");
    }
      
    tag = tag.trim();
    tag = tag.replaceAll("?", "%3F");
    tag = tag.replaceAll("&", "%26");
    tag = tag.replaceAll("/", "%2F");
    tag = tag.replaceAll(":", "%3A");
    tag = tag.replaceAll("=", "%3D");

    _tag = tag;
    int resDressCount = 0;
    
    stateChangeNotNotify(ProviderState.PROGRESS);
    if( page ==0 ) {
      _searchDress = List<ModelDress>();
    }

    final response = await ApiHelper().get('/tag/search/word/$tag/dress/$page');
    if(response['code'] != 200) {
      _searchDress = List<ModelDress>();
    } else {
      if (response['data'] != null) {
        response['data'].forEach((v) {
          resDressCount++;
          _searchDress.add(ModelDress.fromJsonDynamic(v));
        });
      }
    }
    if(_page > 0) {
      if(_dressListCount != resDressCount) {
        _canReadMore = false;
      }
      stateChangeNotify(ProviderState.DATA_CHANGE);
    } else {
      _canReadMore = true;
      _dressListCount = resDressCount;
      stateChangeNotify(ProviderState.DONE);
    }
    
    stateChangeNotify(ProviderState.DONE);
  }


  void clear() {
    _page = 0;
    _tag = null; 
    _dressListCount = 0;
    _canReadMore = true;
    _searchDress = new List<ModelDress>();

  }
    
}