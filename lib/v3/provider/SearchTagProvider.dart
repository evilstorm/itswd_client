
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelTag.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class SearchTagProvider extends BaseProvider {

  List<ModelTag> _tagList;
  String _tag;

  String get currentTag => _tag;
  List<ModelTag> get tagList => _tagList;
  
  void inputProgress() {
    stateChangeNotify(ProviderState.PROGRESS);
  }
  void searchTag(tag) async {
    Print.e(" DO SEARCH TAG!!! $tag");

    stateChangeNotify(ProviderState.PROGRESS);
    _tag = tag;
    
    try {
      final response = await ApiHelper().get('/tag/search/$tag');
      if(response['code'] != 200) {
        _tagList = List<ModelTag>();
      } else {
        if (response['data'] != null) {
          _tagList = List<ModelTag>();
          response['data'].forEach((v) {
            _tagList.add(ModelTag.fromJsonDynamic(v));
          });
        }
      }

      stateChangeNotify(ProviderState.DONE);
    } on Exception catch(e) {
      Print.e(e);
      stateChangeNotify(ProviderState.EXCEPTION);
    }
  }

  void clear() {
    _tagList = List<ModelTag>();
    stateChangeNotify(ProviderState.INIT);
  }

    
}