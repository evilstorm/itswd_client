import 'package:itswd/v3/components/common/EnumUtils.dart' as EnumUtils;
import 'package:itswd/v3/components/Contants.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class UserDressProvider extends BaseProvider {

  SORT_TYPE _currentType;
  List<ModelDress> _dressList;

  int _page = 0;
  int _dressListCount = 0;

  String _currentUserId;
  bool _canReadMore = true;

  List<ModelDress> get dressList => _dressList;
  bool get canReadMore => _canReadMore;

  UserDressProvider();

  void reqUserDress(String userId, SORT_TYPE type) async {
    
    stateChangeNotify(ProviderState.PROGRESS);

    int resDressCount = 0;
    if(_currentUserId == userId && _currentType == type) {
      _page += 1;
    } else {
      _currentUserId = userId;
      _currentType = type;
      _page = 0;
      _dressList = List();
    }

    var response = await ApiHelper().get('/dress/user/$userId/${EnumUtils.removePrefix(type.toString())}/$_page');

    if(response['code'] == 200) {
      if (response['data']['dress'] != null) {
        response['data']['dress'].forEach((v) {
          resDressCount += 1;

          _dressList.add(ModelDress.fromJsonDynamic(v));
        });
      }

      if(_page > 0) {
        if(_dressListCount != resDressCount) {
          _canReadMore = false;
        }
        stateChangeNotify(ProviderState.DATA_CHANGE);
      } else {
        _canReadMore = true;
        _dressListCount = resDressCount;
        stateChangeNotify(ProviderState.DONE);
      }
    }   
  }
  void readMore() {
    reqUserDress(_currentUserId, _currentType);
  }

}