import 'package:itswd/v3/provider/BaseProvider.dart';


class UserEventProvider extends BaseProvider {
  int _isGridView = 0; //0: Grid other: list 
  int _selectedPos = 0;
  double _listStartOffSet = 0;
  bool _isBack = false;
  int _tabPos = 0;

  int get isGridView => _isGridView;
  void showGridView() {
    _selectedPos = 0;
    _listStartOffSet = 0;
    _isGridView = 0;
    stateChangeNotify(ProviderState.DATA_CHANGE);
  }

  void showListView(int pos, double offset) {
    _selectedPos = pos;
    _listStartOffSet = offset;
    _isGridView = 1;

    stateChangeNotify(ProviderState.DATA_CHANGE);
  } 
 
  
  int get selectedPos => _selectedPos;
  set setSelectedPos(int pos) => _selectedPos = pos;

  double get listStartOffSet => _listStartOffSet;
  set setListStartOffSet(double offset) => _listStartOffSet = offset;

  bool get isBack => _isBack;
  set isBack(bool back) {
     _isBack = back;
     stateChangeNotify(ProviderState.DATA_CHANGE);
  }

  int get getTabPos => _tabPos;
  set setTabPos(int pos) {
    _tabPos = pos;
    stateChangeNotify(ProviderState.DATA_CHANGE);
  }
}