import 'dart:convert';

import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/SharedPref.dart';
import 'package:itswd/v3/components/common/SharedPrefKeys.dart';
import 'package:itswd/v3/components/common/signin/Constants.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/model/response/ModelReceivedAct.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';

class UserProvider extends BaseProvider {
  
  ModelUser _userInfo;
  String _errMsg;

  ModelDress _todayMyDress;
  
  int _signinState = SignInNotYet;

  int get signinState => _signinState;
  ModelUser get userInfo => _userInfo;
  String get message => _errMsg;
  ModelDress get todayMyDress => _todayMyDress;

  ModelReceivedAct _receiveAct;
  ModelReceivedAct get receivedActData => _receiveAct;


  void setClearSignInState() {
    _signinState = SignInNotYet;
    stateChangeNotify(ProviderState.DATA_CHANGE);
  }

  // void signIn(data) async {
  //   _signinState = SignInProgress;
    
  //   stateChangeNotify(ProviderState.PROGRESS);
  //   try {
  //   var response = await ApiHelper().post(
  //     '/user/signin',
  //     data
  //   );

  //   if(response['code'] != 200) {
  //     _signinState = SignInFail;
  //    return stateChangeNotify(ProviderState.EXCEPTION);
  //   }
    
  //   _signinState = SignInSccess;
  //   _userInfo = ModelUser.fromJsonDynamic(response['data']['myInfo']);

  //   setString(SharedPrefKeys.JWT_TOKEN, response['data']['jwtToken']);
  //   setInt(SharedPrefKeys.LOGIN_TYPE_I, _userInfo.loginType);

  //   getTodayMyDress();

  //   stateChangeNotify(ProviderState.DONE);

  //   } catch (e) {
  //       _signinState = SignInFail;
  //     return stateChangeNotify(ProviderState.EXCEPTION);
  //   }
  // }

  void setUserInfo(dynamic userInfo) async {
    _userInfo = ModelUser.fromJsonDynamic(userInfo);
    await getTodayMyDress();
    stateChangeNotify(ProviderState.DONE);

  }

  void setTodayMyDress(ModelDress todayDress) {
    _todayMyDress = todayDress;
    stateChangeNotify(ProviderState.DATA_CHANGE);
  }

  void setSigninState(int signinSt$te) {
    _signinState = signinState;    
    
    stateChangeNotify(ProviderState.DONE);
  }

  void updateUserInfo() async {

    try {
      var response = await ApiHelper().get('/user/me');
      if(response['code'] != 200) {
        return null;
      }

      _userInfo = ModelUser.fromJsonDynamic(response['data'][0]);
      stateChangeNotify(ProviderState.DONE);

    } catch(e) {
      Print.e(e);
      stateChangeNotify(ProviderState.EXCEPTION);
    }

  }

  Future<ModelUser> modifyUserInfo(dynamic body) async {
    
    try {

      var response = await ApiHelper().patch('/user', body);
      if(response['code'] != 200) {
        stateChangeNotify(ProviderState.EXCEPTION);
      }

      _userInfo = ModelUser.fromJsonDynamic(response['data'][0]);
      stateChangeNotify(ProviderState.DONE);
      return _userInfo;
    } catch(e) {
      Print.e(e);
      stateChangeNotify(ProviderState.EXCEPTION);
    }
  }

  Future<ModelDress> getTodayMyDress() async {
    if(_todayMyDress != null) {
      return _todayMyDress;
    }

    var response = await ApiHelper().get('/dress/today');
    
    if(response['code'] != 200) {
      _todayMyDress = null;
      return _todayMyDress;
    }

    List<dynamic> temp = response['data']['dress'];
    if(temp.isEmpty) {
      _todayMyDress = null;
      return _todayMyDress;
    }

    _todayMyDress = ModelDress.fromJsonDynamic(temp[0]);
    return _todayMyDress;
  }


  Future<ModelDress> postTodayDress(List<String> images, String say, List<String> tags) async {
    if(_todayMyDress != null) {
      return _todayMyDress;
    }

    var response = await ApiHelper().post(
      '/dress', 
      json.encode({
      'owner': _userInfo.id,
      'images': images,
      'say': say,
      'tags': tags
    }));

    
    if(response['code'] == 200) {
      _todayMyDress = ModelDress.fromJsonDynamic(response['data']['dress'][0]);

      stateChangeNotify(ProviderState.DATA_CHANGE);
      return _todayMyDress;
    } 

  }

  void addBlockUser(String id) {
    _userInfo.blockUser.add(id);
    stateChangeNotify(ProviderState.DATA_CHANGE);
  }

  void reqReceivedInfo() async {
    if(_userInfo.id == null) {
      return;
    }
    
    var response = await ApiHelper().get('/user/received/${_userInfo.id}');
    if(response['code'] != 200) {
      return null;
    }

    _receiveAct = ModelReceivedAct.fromJsonDynamic(response['data'][0]);

    stateChangeNotify(ProviderState.DATA_CHANGE);
    
  }


}