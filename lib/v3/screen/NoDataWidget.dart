import 'package:flutter/material.dart';

class NoDataWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          '깨 끗!',
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ),
    );
  }
}