import 'dart:io';

import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/signin/Constants.dart';
import 'package:itswd/v3/components/common/signin/SignInBBon.dart';
import 'package:itswd/v3/components/views/CircularProgress.dart';
import 'package:itswd/v3/components/views/ThridPartySignInButton.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/SignIn/AccountWidget.dart';
import 'package:provider/provider.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  
  bool _isSignInIng = false;
  bool _isShowFailMsg = false;
  bool _showAddAccount = false;

  Widget _googleSignInButton() {
    return ThridPartySignInButton(
      key: UniqueKey(), 
      svgImage: "assets/Google__G__Logo.svg",
      title: "Sign in with Google",
      callback: () {
        SignInBBon(signinResult: _signinResult).doSignIn(SignInByGoogle);
      } 
    );
  }

    Widget _facebookSignInButton() {
    return ThridPartySignInButton(
      key: UniqueKey(), 
      svgImage: "assets/facebook-svgrepo-com.svg", 
      color: Color(0xFF4267B2),
      title: "Sign in with Facebook",
      callback: () {
        SignInBBon(signinResult: _signinResult).doSignIn(SignInByFaceBook);
      }
    );
  }

    Widget _kakaoSignInButton(BuildContext context) {
    return ThridPartySignInButton(
      key: UniqueKey(), 
      svgImage:"assets/kakaotalk.svg", 
      title: "Sign in with KakaoTalk",
      callback: () {
        SignInBBon(signinResult: _signinResult).doSignIn(SignInByKaKao);
      } 
    );
  }



  Widget _appleSignInButton() {
    return ThridPartySignInButton(
      key: UniqueKey(), 
      svgImage: "assets/apple_log.svg",
      title: "Sign in with Apple",
      callback: () async {
        SignInBBon(signinResult: _signinResult).doSignIn(SignInByApple);
      } 
    );
  }

  void _signin() {

  }

  Widget _bbonSignIn() {
    return Container(
      child: Column(
        children: [
          TextField(
            style: Theme.of(context).textTheme.bodyText1,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: '이메일',
            ),
          ),
          SizedBox(height: 8.0,),
          TextField(
            style: Theme.of(context).textTheme.bodyText1,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: '비밀번호',
            ),
          ),
          Container(
            height: 32.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.fromLTRB(4.0,8.0,4.0,8.0)),
                  ),
                  onPressed: () => _signin(),
                  child: Text(
                    '회원가입',
                    style: Theme.of(context).textTheme.overline,
                  ),
                ),
                SizedBox(width: 16.0,),
                TextButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.fromLTRB(4.0,8.0,4.0,8.0)),
                  ),
                  onPressed: () => _signin(),
                  child: Text(
                    '비밀번호찾기',
                    style: Theme.of(context).textTheme.overline,
                  ),
                ),

              ],
            ),
          ),
          SizedBox(height: 16.0,),
          Container(
            width: MediaQuery.of(context).size.width,
            child: TextButton(
              style: ButtonStyle(
                padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 12.0)),
                backgroundColor: MaterialStateProperty.resolveWith((states) {
                  if(states.contains(MaterialState.pressed)) {
                    return Theme.of(context).dialogBackgroundColor;
                  } else {
                    return Colors.black;
                  }
                }),
              ),
              onPressed: () => _signin(),   
              child: Text(
                '로그인',
                style: Theme.of(context).textTheme.bodyText1,
              )
            ),
          ),
        ],
      ),
    );
  }

  Widget _thirdPartySignInList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 16.0,),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              '가입 & 로그인', 
              style: Theme.of(context).textTheme.subtitle1, 
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(height: 36.0,),
          _bbonSignIn(),
          SizedBox(height: 26.0,),
          Divider(
            color: Color(0xFFD8D8D8),
            thickness: 1.5,
          ),
          SizedBox(height: 16.0,),
          _googleSignInButton(),
          SizedBox(height: 16.0,),
          _facebookSignInButton(),
          SizedBox(height: 16.0,),
          _kakaoSignInButton(context),
          SizedBox(height: 16.0,),
          Visibility(
            visible: Platform.isIOS,
            child: _appleSignInButton(),
          ),
        ],
      ),
    );
  }

  void _signinResult(result) {
    if(result['result'] == false) {
      setState(() {
        _isShowFailMsg = true;
        _isSignInIng = false;      
      });
    
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Theme.of(context).accentColor,
        content: Text(
          '로그인을 실패했습니다.', 
          style: Theme.of(context).textTheme.bodyText1,
          ),
      ),
    );

    } else {
      Navigator.pushReplacementNamed(
        context, 
        '/main',
        arguments: <String, dynamic> {
        'userInfo': result['data'],
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Print.e("Screen SigninScreen");
    
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Theme.of(context).backgroundColor,
          child:  Stack(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: _thirdPartySignInList(),
              ),
              Visibility(
                visible: _showAddAccount,
                child: AddAccountWidget()
              ),
              Visibility(
                visible: (_isSignInIng),
                child: CircularProgress(),
              )
            ],
          )
      ),
    ));
  }

}