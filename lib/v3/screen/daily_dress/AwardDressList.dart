import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:itswd/v3/components/views/AwardAvatar.dart';
import 'package:itswd/v3/components/views/LoadingH.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/AwardDressProvider.dart';

import 'package:provider/provider.dart';

import 'package:itswd/v3/components/common/DateUtils.dart' as DateUtil;

// ignore: must_be_immutable
class AwardDressList extends StatefulWidget {
  final double height;
  final Function selectedDate;
  AwardDressList({
    Key key,
    this.height,
    this.selectedDate
  }): super(key: key);

  @override
  _AwardDressListState createState() => _AwardDressListState();

}
class _AwardDressListState extends State<AwardDressList> {

  double avataHeight;
  int _selectedIndex = -1;

  @override
  void initState() {
    super.initState();
    avataHeight = widget.height - 10;
  }

  Widget avataDate(BuildContext context, DateTime date) {
    return SizedBox(
      width: avataHeight+5,
      height: avataHeight+5,
      child: Align(
        alignment: Alignment.bottomRight,
        child: Container(
          width: 35.0,
          height: 35.0,
          child: Center(
            child: Text(
              "${DateUtil.dateFormatMMdd.format(date)}",
              style: Theme.of(context).textTheme.caption.copyWith(color: Colors.black, fontSize: 10, fontWeight: FontWeight.w500),
            ),
          ),
          decoration: BoxDecoration(
            border: Border.all(width: 1),
            shape: BoxShape.circle,
            color: Colors.white,
          ),
        )
      ),
    );
  }

  Widget avataWithDate(BuildContext context, ModelDress dress, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
        widget.selectedDate(dress.createdAt);
      },
      child: Container(
        width: widget.height,
        height: widget.height,
        child: Stack(
          children: [
            Center(
              child: AwardAvatar(
                height: avataHeight,
                width: avataHeight,
                url: dress.images[0],
              ),
            ),
            avataDate(context, dress.createdAt),
            Visibility(
              visible: _selectedIndex != index,
              child: Container(
                width: widget.height,
                height: widget.height,
                color: Colors.black54,
              ),
            )
          ],
        ),
      ),
    );

  }

  Widget content(BuildContext context, List<ModelDress> list) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: list.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => avataWithDate(context, list[index], index),
    );
  }

  Widget today(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = -1;  
        });
        widget.selectedDate(DateTime.now());
      },
      child: Container(
        width: widget.height,
        height: widget.height,
        child: Stack(
          children: [
            Center(
              child: AwardAvatar(
                height: avataHeight,
                width: avataHeight,
                url: null,
                isToday: true,
              ),
            ),
            avataDate(context, DateTime.now()),
            Visibility(
              visible: _selectedIndex != -1,
              child: Container(
                width: widget.height,
                height: widget.height,
                color: Colors.black54,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      height: widget.height,
      child: Row(
        children: [
          today(context),
          Expanded(
            child: FutureProvider(
              create: (_) => AwardDressProvider().reqDressCount(30),
              child: Consumer<List<ModelDress>>(
                builder: (context, list, _) {
                  if(list == null) {
                    return LoadingH(duration: Duration(seconds: 3),);
                  }
                  return content(context, list);
                },),
            ),
          ),
        ],
      ),
    );
  }
}