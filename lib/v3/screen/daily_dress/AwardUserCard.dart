
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:itswd/v3/components/views/BlurText.dart';
import 'package:itswd/v3/components/views/GoldLineBorder.dart';


class AwardUserCard extends StatelessWidget {
  final String userName;
  final String date;
  final String rank;
  final String imageUrl;
  final int likeCount;
  final int replyCount;
  final double width;
  final double height;

  const AwardUserCard({
    Key key, 
    @required this.userName, 
    @required this.date,
    @required this.rank, 
    @required this.imageUrl, 
    @required this.likeCount, 
    @required this.replyCount,
    this.width = double.infinity,
    this.height = 180.0, 
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: GoldLineBorder(
        height: height,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: leftInfos(context)),
              CachedNetworkImage(
                width: 180.0,
                height: 180.0,
                fit: BoxFit.cover,
                imageUrl: imageUrl, 
              ),
            ],
          ),
        ),
      ),
    );
  }

  Column leftInfos(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Ranking(rank: rank, date: date),
          BlurText(
            text: userName,
            textSize: 30.0,
            textLineColor: const Color.fromARGB(155, 40, 60, 40),
            blurColor: const Color.fromARGB(255, 250, 250, 250),
          ),
          Spacer(),
          // 서버의 카운트와 맞추려며 많은 api콜이 필요해 제외시킨다.
          // countByType(context, Icons.favorite, Colors.red, likeCount),
          // countByType(context, Icons.comment, Colors.grey, replyCount),
        ],
      );
  }

  Row countByType(BuildContext context, IconData icons, Color color, int count) {
    return Row(
      children: [
        Icon(
          icons,
          color: color,
          size: 34.0,
        ),
        SizedBox(width: 8.0,),
        Text(
          '$count',
          style: Theme.of(context).textTheme.bodyText1,
        )
      ],
    );
  }
}

class Ranking extends StatelessWidget {
  const Ranking({
    Key key,
    @required this.rank,
    @required this.date,
  }) : super(key: key);

  final String rank;
  final String date;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Visibility(
          visible: date != null,
          child: Container(
            child: BlurText(
              text: date,
              textSize: 25,    
            ),
          ),
        ),
        SizedBox(width: 16.0,),
        BlurText(
          text: rank,
          textSize: 45,
        ),
      ],
    );
  }
}