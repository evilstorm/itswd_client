import 'package:flutter/material.dart';
import 'package:itswd/v3/components/Contants.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/DailyDressEventProvider.dart';
import 'package:itswd/v3/provider/DailyDressProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/dress_view/ContentListSliver.dart';
import 'package:provider/provider.dart';

class DailyDressDetailList extends StatefulWidget {
  @override
  _DailyDressDetailListState createState() => _DailyDressDetailListState();
}

class _DailyDressDetailListState extends State<DailyDressDetailList> {

  DailyDressProvider dressProvider;
  DailyDressEventProvider eventProvider;
  UserProvider userProvider;
  
  ScrollController _scrollController = ScrollController();

  bool _isDataLoding = true;
  bool _scrollTopPos = true;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
        _scrollTopPos = true;
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(!_isDataLoding && (dressProvider?.canReadMore??false)) {
          _isDataLoding = true;
          dressProvider.readMore();
        }
      } else {
        _scrollTopPos = false;
      }
    });

  }

  Widget _contentWieget() {

    if(dressProvider.dressList == null || dressProvider.dressList?.length == 0) {
      return SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) => Center(child: Text('뽄지는 친구가 없네?', style: Theme.of(context).textTheme.subtitle1,)),
          childCount: 1
        ),
      );
    }
    
    _scrollController.jumpTo((CONTENT_HEIGHT * eventProvider.selectedPos) + HEADER_HEIGHT);

    return ContentListSliver(
      key: UniqueKey(),  
      dressList: dressProvider.dressList, 
      selectedItemIndex: eventProvider.selectedPos, 
      launchUserDetail: true, 
    );
  }
  
  @override
  Widget build(BuildContext context) {
    dressProvider = Provider.of<DailyDressProvider>(context);
    eventProvider = Provider.of<DailyDressEventProvider>(context);
    userProvider = Provider.of<UserProvider>(context);
    
    if(dressProvider.state == ProviderState.DATA_CHANGE 
        ||dressProvider.state == ProviderState.DONE
    ) {
      _isDataLoding = false;
    }


    Print.e("DailyDressDetailList build ${eventProvider.selectedPos}");
    return SafeArea(
      child: Column(
        children: [
          HeaderWithBack(
            title: "${dressProvider.month}/${dressProvider.date}",
            callback: () => eventProvider.showGridView()
          ),
          Expanded(
            child: CustomScrollView(
              controller: _scrollController,
              slivers: [
                _contentWieget()
              ],
            ),
          )
        ],
      ),
    );
  }
}