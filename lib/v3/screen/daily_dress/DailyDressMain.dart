import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/AboutDate.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/SliverHeader.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/DailyDressEventProvider.dart';
import 'package:itswd/v3/provider/DailyDressProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/daily_dress/AwardDressList.dart';
import 'package:itswd/v3/screen/daily_dress/AwardUserCard.dart';
import 'package:itswd/v3/screen/dress_view/ContentGridSliver.dart';
import 'package:provider/provider.dart';

class DailyDressMain extends StatefulWidget {
  
  @override
  _DailyDressMainState createState() => _DailyDressMainState();
}

class _DailyDressMainState extends State<DailyDressMain> {
  ScrollController _scrollController = ScrollController();

  DailyDressProvider dressProvider;
  DailyDressEventProvider eventProvider;
  UserProvider userProvider;
  
  double scrollOffSet = 0;

  // List<ModelDress> _dressList;
  bool _scrollTopPos = true;
  bool _isDataLoding = true;

  @override
  void initState() {
    super.initState();
    Print.e(" DailyDress INIT initState ");
    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
        _scrollTopPos = true;
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(!_isDataLoding && (dressProvider?.canReadMore??false)) {
          _isDataLoding = true;
          dressProvider.readMore();
        }
      } else {
        _scrollTopPos = false;
      }
    });
  }

///그리드 뷰에서 아이템 선택 시 callback 
  _gridItemSelect(ModelDress dress, int index) {
    eventProvider.showListView(index, _scrollController.position.pixels);
  }

  Widget _content() {
    return ((dressProvider?.dressList??null) != null && dressProvider?.dressList?.length != 0)
    ? ContentGridSliver(key: UniqueKey(), 
        dressList: dressProvider.dressList, 
        clickListener: _gridItemSelect, 
        scrollController: _scrollController
      )
    : SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) => Center(child: Text('뽄지는 친구가 없네?', style: Theme.of(context).textTheme.subtitle1,)),
          childCount: 1
        ),
      );
  }

  Widget _dailyAwardCard() {

    return SliverPersistentHeader(
      delegate: SliverHeader(
        maxHeight: 200.0,
        minHeight: 50.0,
        child: (dressProvider.awardList?.length??0) != 0 
          ? AwardUserCard(
              rank: dressProvider.awardList[0].isAward? "뽄!":"Now",
              userName: dressProvider.awardList[0].owner.name?? dressProvider.awardList[0].owner.email.split("@")[0],
              imageUrl: dressProvider.awardList[0].images[0],
              likeCount: dressProvider.awardList[0].likeCount,
              replyCount: dressProvider.awardList[0].replyCount,
              height: 230.0,
              date: AboutDate().dateFormatMd.format(dressProvider.awardList[0].createdAt), 
            )
          : Text("")
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Print.e(" DailyDressMain Build!!!!");
    eventProvider = Provider.of<DailyDressEventProvider>(context, listen: false);
    dressProvider = Provider.of<DailyDressProvider>(context);
    userProvider = Provider.of<UserProvider>(context);

    if(dressProvider.state == ProviderState.DATA_CHANGE 
        ||dressProvider.state == ProviderState.DONE
    ) {
      _isDataLoding = false;
    }

    return SafeArea(
      child: CustomScrollView(
        controller: _scrollController,
        slivers: [
          SliverPersistentHeader(
            floating: true,
            pinned: false,                      
            delegate: SliverHeader(
              maxHeight: 100.0,
              minHeight: 100.0,
              child: AwardDressList(
                height: 100.0,
                selectedDate: (DateTime date) {
                  _scrollController.jumpTo(0);
                  dressProvider.reqDress(date.year, date.month, date.day);
                },
              ),
            ),
          ),
          _dailyAwardCard(),
          _content()
        ],
      ),
    );
  }
}