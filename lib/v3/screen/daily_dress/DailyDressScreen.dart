import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/provider/DailyDressEventProvider.dart';
import 'package:itswd/v3/screen/daily_dress/DailyDressDetailList.dart';
import 'package:itswd/v3/screen/daily_dress/DailyDressMain.dart';
import 'package:provider/provider.dart';

class DailyDressScreen extends StatefulWidget {
  DailyDressScreen({
    Key key,
  }): super(key: key);

  final _DailyDressScreenState state = _DailyDressScreenState();
  
  @override
  _DailyDressScreenState createState() => state;

  void screenSelected() {
    state._screenSelected();
  }

  bool backPress() {
    return state._historyBack();
  }

}

class _DailyDressScreenState extends State<DailyDressScreen> {

  DailyDressEventProvider _eventProvider;

  void _screenSelected() {
    
  }
  bool _historyBack() {
    
    if(_eventProvider?.isGridView == 0) {
      return true;
    }

    _eventProvider?.showGridView();
    return false;
  }

  @override
  Widget build(BuildContext context) {
    _eventProvider = Provider.of<DailyDressEventProvider>(context);

    Print.e("_eventProvider??? ${_eventProvider.isGridView}");
    return SafeArea(
      child: Center(
        child: Container(
          child: IndexedStack(
            index: _eventProvider.isGridView,
            children: [
              DailyDressMain(),
              DailyDressDetailList(),
            ],
          )
        ),
      ),
    );
  }
}