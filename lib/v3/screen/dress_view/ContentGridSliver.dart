import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:provider/provider.dart';

class ContentGridSliver extends StatefulWidget {
  final List<ModelDress> dressList;
  final Function clickListener;
  final ScrollController scrollController;

  ContentGridSliver({
    Key key,
    this.dressList,
    this.clickListener, 
    this.scrollController,
  }):super(key: key);

  @override
  _ContentGridSliverState createState() => _ContentGridSliverState();
}

class _ContentGridSliverState extends State<ContentGridSliver> {

  UserProvider userProvider;

  List<ModelDress> _dressList;
  @override
  void initState() {
    super.initState();

    setState(() {
      _dressList = widget.dressList;
    });
  }

  bool isBlockedUser(String ownerId) {
    for (var blockUserId in userProvider.userInfo.blockUser) {
      if(blockUserId == ownerId) {
        return true;
      }
    }
    return false;
  }



  Widget contentItemView(ModelDress data, int pos) {
    var errorShow = false;
    var isBlocked = isBlockedUser(data.owner.id);

    if( (data.reportThis??false) || isBlocked) {
      errorShow = true;
      if(data.showImage) {
        errorShow = false;
      }
    }
    return (errorShow == true)
    ? GestureDetector(
        child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.error,
                  color: Colors.red,
                ),
                SizedBox(width: 8.0),
                Text(isBlocked?'차단됨':'신고됨', style: Theme.of(context).textTheme.bodyText2,)
              ],
            ),
            SizedBox(height: 16.0),
            FlatButton(
              color: Theme.of(context).dialogBackgroundColor,
              onPressed: () {
                setState(() {

                  _dressList[pos].showImage = true;
                });
              },
              child: Text(
                '이미지 보기', 
                style: Theme.of(context).textTheme.bodyText2
              ),
            )
          ],
        ),
      ),
    )
    : GestureDetector(
      onTap: () {
        if((data.reportThis??false) || isBlocked) {
          setState(() {
            _dressList[pos].showImage = false;
          });
          return;
        } 

        widget.clickListener(data, pos);
      },
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: CachedNetworkImage(
          imageUrl: data.images[0],
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover
              )
            ),
          ),
          placeholder: (context, url) => Icon(Icons.panorama),
          errorWidget: (context, url, error) => Icon(Icons.error),
        )
      )
    );
  }


  @override
  Widget build(BuildContext context) {
    
    userProvider = Provider.of<UserProvider>(context);

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return contentItemView(_dressList[index], index);
        },
        childCount: _dressList.length??0
      ),
    );
  }
}