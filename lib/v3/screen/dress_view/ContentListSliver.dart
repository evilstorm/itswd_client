import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/dress_view/DressListItem.dart';
import 'package:provider/provider.dart';

class ContentListSliver extends StatefulWidget {

  final int selectedItemIndex;
  final List<ModelDress> dressList;
  final Function eventListener;
  final ScrollController scrollController;
  final bool launchUserDetail;

  ContentListSliver({
    Key key, 
    this.dressList,
    this.selectedItemIndex = 0,
    this.launchUserDetail = true,
    this.eventListener,
    this.scrollController
  }): super(key: key);
  

  @override
  _ContentListSliverState createState() => _ContentListSliverState();
}

class _ContentListSliverState extends State<ContentListSliver> {

  UserProvider userProvider;
  List<ModelDress> _dressList;
  var expandMap = Map();

  @override
  void initState() {
    super.initState();

    setState(() {
      _dressList = widget.dressList;
    });
  }


  @override
  Widget build(BuildContext context) {
    Print.d("ContentListSliver Build!!! ");
    userProvider = context.watch<UserProvider>();

    return SliverList(

      delegate: SliverChildBuilderDelegate ((context, index) {
        return DressListItem(
            dress: _dressList[index],
            pos: index,
            blockUserList: userProvider.userInfo.blockUser,
            launchUserDetail: true,
          );
      },
      childCount: _dressList.length,
      ),

    );
  }
}