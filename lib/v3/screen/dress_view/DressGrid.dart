import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/DressProvider.dart';
import 'package:itswd/v3/provider/EventProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/NoDataWidget.dart';
import 'package:itswd/v3/screen/dress_view/DressGridItem.dart';
import 'package:provider/provider.dart';

class DressGrid extends StatefulWidget {

  final List<ModelDress> dressList;
  final VoidCallback readMore;
  final Function callback;
  final bool canReadMore;

  DressGrid({
    Key key, 
    @required this.dressList,
    @required this.canReadMore,
    @required this.readMore,
    @required this.callback,
  }): super(key: key);

  @override
  _DressGridState createState() => _DressGridState();
}

class _DressGridState extends State<DressGrid> {

  ScrollController _scrollController = ScrollController();

  UserProvider userProvider;
  
  double scrollOffSet = 0;
  bool readMoreCallback = false;
  
  @override
  void initState() {
    super.initState();
    Print.e(" DressGrid INIT initState ");

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(widget.canReadMore) {
          widget.readMore();
        }
      } else {
      }
    });
  }
  
  
  bool isBlockedUser(String ownerId) {
    for (var blockUserId in userProvider.userInfo.blockUser) {
      if(blockUserId == ownerId) {
        return true;
      }
    }
    return false;
  }


  Widget _widgetContent() {
    return GridView.count(
      crossAxisCount: 3,
      controller: _scrollController,
      children: widget.dressList.map((dress) {
        var pos = widget.dressList.indexOf(dress);
        return DressGridItem(
          dress: widget.dressList[pos],
          blockUser: userProvider.userInfo.blockUser,
          pos: pos,
          callback: _itemSelected,
        );
      }).toList(),
    );
  }

  _itemSelected(int position) {
    widget.callback(position, _scrollController.position.pixels);
  }

  bool isDataEmpty() {
    if(widget.dressList == null || widget.dressList.length == 0) {
      return true;
    }

    return false;
  }


  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of<UserProvider>(context);
        
    return isDataEmpty() 
      ? NoDataWidget()
      : _widgetContent();
  }
}