import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';

class DressGridItem extends StatefulWidget {
  
  final ModelDress dress;
  final List<String> blockUser;
  final int pos;
  final Function callback;

  DressGridItem({
    Key key,
    @required this.dress,
    @required this.blockUser,
    @required this.pos,
    @required this.callback
  }):super(key: key);

  @override
  _DressGridItemState createState() => _DressGridItemState();
}

class _DressGridItemState extends State<DressGridItem> {
  var errorShow  = false;
  var isBlockedUser = false;
  

  @override
  void initState() {
    super.initState();
    isBlockedUser = checkBlockedUser();

  }

  bool checkBlockedUser() {
    for (var blockUserId in widget.blockUser) {
      if(blockUserId == widget.dress.owner.id) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    if((widget.dress.reportThis??false) || isBlockedUser) {
      errorShow = true;
      if(widget.dress.showImage) {
        errorShow = false;
      }
    }

    return (errorShow == true)
    ? GestureDetector(
        child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.red)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.error,
                  color: Colors.red,
                ),
                SizedBox(width: 8.0),
                Text(isBlockedUser?'차단됨':'신고됨', style: Theme.of(context).textTheme.bodyText2,)
              ],
            ),
            SizedBox(height: 16.0),
            FlatButton(
              color: Theme.of(context).dialogBackgroundColor,
              onPressed: () {
                setState(() {
                  widget.dress.showImage = true;
                });
              },
              child: Text(
                '이미지 보기', 
                style: Theme.of(context).textTheme.bodyText2
              ),
            )
          ],
        ),
      ),
    )
    : GestureDetector(
      onTap: () {
        if((widget.dress.reportThis??false) || isBlockedUser) {
          setState(() {
            widget.dress.showImage = false;
          });
          return;
        } 

        widget.callback(widget.pos);
        // widget.clickListener(data, pos);
      },
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: CachedNetworkImage(
          imageUrl: widget.dress.images[0],
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover
              )
            ),
          ),
          placeholder: (context, url) => Icon(Icons.panorama),
          errorWidget: (context, url, error) => Icon(Icons.error),
        )
      )
    );

  }
}