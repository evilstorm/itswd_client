import 'package:flutter/material.dart';
import 'package:itswd/v3/components/Contants.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/DressProvider.dart';
import 'package:itswd/v3/provider/EventProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/NoDataWidget.dart';
import 'package:itswd/v3/screen/dress_view/DressListItem.dart';
import 'package:provider/provider.dart';

class DressList extends StatefulWidget {
  
  final List<ModelDress> dressList;
  final VoidCallback readMore;
  final bool canReadMore;
  final int selectedPos;

  DressList({
    Key key, 
    @required this.dressList,
    @required this.canReadMore,
    @required this.selectedPos,
    @required this.readMore,
  }): super(key: key);


  @override
  _DressListState createState() => _DressListState();
}

class _DressListState extends State<DressList> {
  UserProvider userProvider;

  int _movePos;

  ScrollController _scrollController = ScrollController();
  
  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(widget.canReadMore) {
          widget.readMore();
        }
      } else {
      }
    });
  }


  bool isDataEmpty() {
    if(widget.dressList == null || widget.dressList.length == 0) {
      return true;
    }

    return false;
  }

  Widget _widgetContent() {

    if(_scrollController.hasClients) { 
      if(_movePos == widget.selectedPos) {
        
      } else {
        _movePos = widget.selectedPos;
        _scrollController.jumpTo((CONTENT_HEIGHT * widget.selectedPos) + HEADER_HEIGHT);  
      }
    }
    


    return ListView.builder(
        controller: _scrollController,
        itemCount: widget.dressList.length,
        itemBuilder: (BuildContext context, int index) {
          return DressListItem(
            dress: widget.dressList[index],
            pos: index,
            blockUserList: userProvider.userInfo.blockUser,
            launchUserDetail: true,
          );
        }
      );
  }

  @override
  Widget build(BuildContext context) {

    userProvider = Provider.of<UserProvider>(context);

    
    return isDataEmpty() 
      ? NoDataWidget()
      : _widgetContent();
  }
}