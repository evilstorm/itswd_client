import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:itswd/v3/components/views/ReadMoreText.dart';
import 'package:itswd/v3/components/views/UserIcon.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/network/ApiHelper.dart';


class DressListItem extends StatefulWidget {
  
  final ModelDress dress;
  final int pos;
  final List<String> blockUserList;
  final Function callback;
  final bool launchUserDetail;

  DressListItem({
    Key key, 
    @required this.dress,
    @required this.pos,
    this.launchUserDetail = false,
    this.blockUserList,
    this.callback,
  }): super(key: key);


  @override
  _DressListItemState createState() => _DressListItemState();
}

enum DressBlockType {
  NotBlock, Police, UserBlock, ManagerKil
}
class _DressListItemState extends State<DressListItem> {

  bool isSayExpanded = false;

  bool isBlockedUser() {
    for (var blockUserId in widget.blockUserList) {
      if(blockUserId == widget.dress.owner.id) {
        return true;
      }
    }
    return false;
  } 

  Widget _blockedDress(DressBlockType type) {
    return Container(
      width: double.infinity,
      color: Theme.of(context).dialogBackgroundColor,
      constraints: BoxConstraints (
        minHeight: 620.0
      ),
      child: Column(
        children: [
          Stack(
            children: [
              Container(
                height: 400,
                color: Theme.of(context).dialogBackgroundColor,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.warning, 
                      color: Colors.red,
                      size: 36.0,
                    ),
                    Text(
                      (type==DressBlockType.UserBlock)? '차단된 사용자 입니다.':'신고된 컨텐츠 입니다.', 
                      style: Theme.of(context).textTheme.subtitle2,
                    )
                  ],
                ),
              ),
              Visibility(
                visible: widget.dress.showImage??false,
                child: CachedNetworkImage(
                  imageUrl: widget.dress.images[0],
                  imageBuilder: (context, imageProvider) => Container(
                    width: double.infinity,
                    height: 400,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover
                      )
                    ),
                  ),
                  placeholder: (context, url) => Icon(Icons.panorama),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ],
          ),
          Container(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  FlatButton(
                    color: Theme.of(context).backgroundColor,
                    onPressed: () {
                      setState(() {
                        var showImage = widget.dress.showImage??false;
                        widget.dress.showImage = !showImage;
                      });
                    },
                    splashColor: Theme.of(context).dialogBackgroundColor,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(24.0, 8.0, 24.0, 8.0),
                      child: Text((widget.dress.showImage??false == true)?'사진 숨기기':'사진 확인', style: Theme.of(context).textTheme.button),
                    ),
                  ),
                  SizedBox(height:16.0),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _dressDetail() {
    return Container(
      constraints: BoxConstraints (
        minHeight: 620.0
      ),
      child: Column(
          children: <Widget>[
            SizedBox(height: 8.0,),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
              child: UserIcon(
                user: widget.dress.owner, 
                launchDetail: widget.launchUserDetail,
                dress : widget.dress,
                ),
            ),
            SizedBox(height: 8.0,),
            Hero(
              tag: widget.dress.id,
              child: CachedNetworkImage(
                imageUrl: widget.dress.images[0],
                imageBuilder: (context, imageProvider) => Container(
                  width: double.infinity,
                  height: 400,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover
                    )
                  ),
                ),
                placeholder: (context, url) => Icon(Icons.panorama),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          SizedBox(height: 8.0,),
          Container(
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                hartView(widget.dress, widget.pos),
                replyView(widget.dress, widget.pos),
                // Spacer(),
                // reportView(dress, index),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text('좋아요 ${widget.dress.likeCount}개', style: Theme.of(context).textTheme.bodyText2,),
            ),
          ),
          SizedBox(height: 4.0,),
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: ReadMoreText(
                widget.dress.say,
                style: Theme.of(context).textTheme.bodyText2,
                trimLines: 2,
                colorClickableText: Theme.of(context).hintColor,
                trimMode: TrimMode.Line,
                trimCollapsedText: '..... 전체 보기',
                trimExpandedText: '  닫기',
                pos: widget.pos,
                changeExpanedState: expandedChange 
              ),
            ),
          ),
          SizedBox(height: 4.0,),
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: GestureDetector(
                onTap: () {
                  moveReplyView(widget.pos, widget.dress);
                },
                child: Text(
                  '댓글 ${widget.dress.replyCount}개 전체보기',
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
            ),
          )
          ],
        ),
    ); 
  }
  
  Widget hartView(ModelDress dress, int index) {
    return 
      IconButton(
        iconSize: 30,
        icon: Icon(
          (dress.likeThis)? Icons.favorite: Icons.favorite_border,
          color: (dress.likeThis)? Theme.of(context).accentColor: Colors.white,
        ),
        onPressed: () {
          changeLikeState(dress, index);
        },
      );
  }

  void changeLikeState(ModelDress dress, int index) async{
    var response = ApiHelper().patch('/dress/like', 
      json.encode({
        'dressId': dress.id
      }));
    
    setState(() {
      dress.likeThis = !dress.likeThis;
      if(dress.likeThis) {
        dress.likeCount++;
      } else {
        dress.likeCount--;
      }
    });
  }

  Widget replyView(ModelDress dress, int index) {
    return IconButton(
      iconSize: 30,
      icon: Icon(Icons.comment),
      color: Colors.white,
      onPressed: () {
        moveReplyView(index, dress);
      },
    );
  }
  
  void moveReplyView(int index, ModelDress dress) {
    // _dressProvider.setSelectedIndex(index);
    Navigator.pushNamed(
      context, 
      '/reply',
      arguments: <String, dynamic> {
        'dress': dress,
        }
      );
  }

  expandedChange(bool _readMore, int pos) {
    setState(() {
      isSayExpanded = _readMore;
    });
  }

  DressBlockType getBlockType() {
    if((widget.dress.reportThis??false) == true) {
      return DressBlockType.Police;
    }

    if(isBlockedUser()) {
      return DressBlockType.UserBlock;
    }

    return DressBlockType.NotBlock;
  }

  @override
  Widget build(BuildContext context) {
    
    return (getBlockType() == DressBlockType.NotBlock)
      ? _dressDetail()
      : _blockedDress(getBlockType());
      
  }
}