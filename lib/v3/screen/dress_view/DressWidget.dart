import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/DressProvider.dart';
import 'package:itswd/v3/screen/dress_view/ContentGridSliver.dart';

class DressWidget extends StatefulWidget {
  DressProvider provider;

  DressWidget({
    @required this.provider
  });
  
  @override
  _DressWidgetState createState() => _DressWidgetState();

}

class _DressWidgetState extends State<DressWidget> {

  ScrollController _scrollController = ScrollController();

  // DressProvider dressProvider;

  /// 그리드 뷰에서 선택된 아이템 Index
  /// 리스트 뷰로 변경 될 때 해당 아이템으로 이동 계산에 필요
  int _selectedItemIndex = 0;
  /// Dress 보여지는 상태 그리드: true, 리스트: false
  bool _isGridView = true;
  /// 데이터 로딩중 
  bool _isDataLoding = true;
  /// 스크롤 탑 포지션 상태
  /// DailyDress 메뉴를 또 누른다면 스크롤 최 상단으로 이동.
  bool _scrollTopPos = true;
  double scrollOffSet = 0;

  bool _isViewTypeChange = false;
  
///그리드 뷰에서 아이템 선택 시 callback 
  _gridItemSelect(ModelDress dress, int index) {
    scrollOffSet = _scrollController.position.pixels;
    setState(() {
      _isGridView = false;
      _isViewTypeChange = true;
      _selectedItemIndex = index;
    });
  }


  @override
  void initState() {
    super.initState();

    Print.e(" DailyDress INIT initState ");

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
        _scrollTopPos = true;
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(!_isDataLoding && (widget.provider.canReadMore??false)) {
          _isDataLoding = true;
        }
      } else {
        _scrollTopPos = false;
      }
    });
  }

  Widget _contentView() {
    return Container();

  }

  @override
  Widget build(BuildContext context) {
    return _contentView();
  }
}