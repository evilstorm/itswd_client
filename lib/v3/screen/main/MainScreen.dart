import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/DateCalcUtil.dart';
import 'package:itswd/v3/components/common/FBMessage.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/daily_dress/DailyDressScreen.dart';
import 'package:itswd/v3/screen/my_page/MyInfoScreen.dart';
import 'package:itswd/v3/screen/my_page/calendar/SummaryCalendarScreen.dart';
import 'package:itswd/v3/screen/search/SearchScreen.dart';
import 'package:itswd/v3/screen/today_mydress/TodayMyDressScreen.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  final dynamic userInfo;
  MainScreen({
    Key key,
    this.userInfo
  });
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  UserProvider _userProvider;
  IndexedStack mainScreenStack;
  int _tapIndex = 1;


  TodayMyDressScreen screenToday = TodayMyDressScreen(key: UniqueKey());
  DailyDressScreen screenDailyDress = DailyDressScreen(key: UniqueKey());
  SearchScreen screenSearch = SearchScreen(key: UniqueKey());
  MyInfoScreen screenMyInfo = MyInfoScreen(key: UniqueKey());
  SummaryCalendarScreen calScreen = SummaryCalendarScreen();

  @override
  void initState() {
    DateCalcUtil.setLocalMessages();
    super.initState();
  }  

  Widget _content() {    
    mainScreenStack = IndexedStack(
      index: _tapIndex,
      children: <Widget>[
        screenToday,
        screenDailyDress,
        screenSearch,
        screenMyInfo,
        // calScreen,
      ],  
    );

    return mainScreenStack;
  }


  bool _backPress() {
    bool result = false;
    switch(_tapIndex) {
      case 0:
        result = screenToday.backPress();
      break;
      case 1:
        result =  screenDailyDress.backPress();
      break;
      case 2:
        result =  screenSearch.backPress();
      break;
      case 3:
        result =  screenMyInfo.backPress();
      break;
      default:
      return false;
    }
    
    if(_tapIndex != 1 && result) {
      setState(() {
        _tapIndex = 1;
      });
      return false;
    }

    return result;


  }


  @override
  Widget build(BuildContext context) {

    _userProvider = Provider.of<UserProvider>(context);
    if(_userProvider.userInfo == null) {
      _userProvider.setUserInfo(widget.userInfo);
    } else {
      FBMessage().init(_userProvider.userInfo.pushToken);
    }

    return WillPopScope(
      onWillPop: () async {
        bool result = _backPress();
        Print.d("result: $result");
        return  result;
      },
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: _content(),
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: Theme.of(context).accentColor,
          currentIndex: _tapIndex,
          onTap: (index) {
            if(_tapIndex != index) {
              setState(() {
                _tapIndex = index;
              });
            }
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.format_align_left,
                size: 28,
              ),
              label: ""
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.calendar_today,
                size: 28,
              ),
              label: ""
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                size: 28,
              ),
              label: ""
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                size: 28,
              ),
              label: ""
            ),
            // BottomNavigationBarItem(
            //   icon: Icon(
            //     Icons.person,
            //     size: 28,
            //   ),
            //   label: ""
            // ),
          ],

        ),
      ),
    );
  }
}