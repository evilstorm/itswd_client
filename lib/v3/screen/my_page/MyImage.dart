import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MyImage extends StatefulWidget {
  final String url;
  final double imageSize;
  final VoidCallback callback;
  MyImage({
    Key key,
    this.url,
    this.imageSize,
    this.callback,
  }): super(key: key);

  @override
  _MyImageState createState() => _MyImageState();
}

class _MyImageState extends State<MyImage> {
  
  String _url;
  File _image;
  @override
  void initState() {
    super.initState();
    setState(() {
      _url = widget.url;
    });
  }

  Widget noneImage() {
    return Icon(
      Icons.tag_faces,
      size: widget.imageSize,
      );
  }

  
  Widget userImage() {
    return ClipOval(
      child: CachedNetworkImage(
        imageUrl: _url, 
        imageBuilder: (context, imageProvider) => Container(
          width: widget.imageSize,
          height: widget.imageSize,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover
            )
          ),
        ),
        placeholder: (context, url) => noneImage(),
        errorWidget: (context, url, error) =>  noneImage(),
      )
    );
  }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // _imageChange();
        widget.callback();
      },
      child: Stack(
        children: <Widget>[
          ((_url ?? null) == null) 
          ? noneImage()
          : userImage(),
          Positioned(
            right: 0,
            bottom: 0,
            child: Icon(
              Icons.photo_camera,
              size: 24,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}