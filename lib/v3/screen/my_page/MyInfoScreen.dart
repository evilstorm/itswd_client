import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:itswd/v3/components/common/FireStorage.dart';
import 'package:itswd/v3/components/views/BlurText.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/my_page/MyImage.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MyInfoScreen extends StatefulWidget {

  MyInfoScreen({
    Key key,
  }): super(key: key);

  final _MyInfoScreenState state = _MyInfoScreenState();
  
  @override
  _MyInfoScreenState createState() => state;

  void screenSelected() {
    state._screenSelected();
  }

  bool backPress() {
    return state._historyBack();
  }

}

enum DressSearchType {
  Crown,  //상받은 옷들 
  MyDress, // 내 옷들 
  ActionLike, // 다른 사람옷에 추가 한 좋아요
  ActionReply, // 다른 사람옷에 내가 단 댓글
  MyDressSortLike, //내옷에 좋아요 
  MyDressSortReply, //내옷에 댓글
  NONE,
}

class _MyInfoScreenState extends State<MyInfoScreen> {


  final TextEditingController _nameWatcher = TextEditingController();
  final TextEditingController _descriptionWatcher = TextEditingController();
  FocusNode _nameFieldFocus;
  FocusNode _descriptionFieldFocus;

  UserProvider _userProvider;

  final double imageSize = 84.0;
  bool _refreshUserData = true;
  int _pageIndex = 0;

  DressSearchType _currentSearchType = DressSearchType.NONE;
  String _searchTypeStr = "없음";

    @override
  void initState() {
    super.initState();
    _nameFieldFocus = FocusNode();
  
  }

  void _screenSelected() {

  }

  bool _historyBack() {
    return true;
  }

  String setSearchTypeStr() {
    String result;
    switch(_currentSearchType) {
      case DressSearchType.Crown:
      result = "뽄";
      break;
      case DressSearchType.MyDress:
      result = "내가 등록한 뽄";
      break;
      case DressSearchType.ActionLike:
      result = "내가 좋아하는 뽄";
      break;
      case DressSearchType.ActionReply:
      result = "내가 댓글 등록한 뽄";
      break;
      case DressSearchType.MyDressSortLike:
      result = "좋아요 받은 뽄";
      break;
      case DressSearchType.MyDressSortReply:
      result = "댓글 받은 뽄";
      break;
      default:
      result = "";
      break;
    }

    // setState(() {
    //   _searchTypeStr = result;
    // });
    return result;
  }

  @override
  void dispose() {
    super.dispose();

    _nameFieldFocus.dispose();

  }

  void saveChangedName() {
    if(_userProvider?.userInfo?.name == _nameWatcher.text ) {
      return;
    }
    
    _userProvider?.modifyUserInfo(
      json.encode({
        "name":  _nameWatcher.text.trim()
      })
    );
  }
  void saveChangedDescription() {
    if(_userProvider?.userInfo?.aboutMe == _descriptionWatcher.text){
      return;
    }
    
    _userProvider?.modifyUserInfo(
      json.encode({
        "aboutMe":  _descriptionWatcher.text.trim()
      })
    );
  }

void _changeProfileImage() async {

    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(image == null) {
      return;
    }

    FireStorage fs = new FireStorage();
    String url = await fs.uploadFile('gevilstorm@gmail.com', image);
    _userProvider.modifyUserInfo(json.encode({"pictureMe": url}));
}

  Widget headerView() {
    return Column(
      children: <Widget>[
        SizedBox(height: 32.0,),
        Row(
          children: <Widget>[
            SizedBox(width: 16.0,),
            MyImage(
              key: UniqueKey(),
              url: _userProvider?.userInfo?.pictureMe, 
              imageSize: 85,
              callback: _changeProfileImage,
            ),
            SizedBox(width: 16.0,),
            userNameWidget(),
          ],
        ),
        SizedBox(height: 16.0),
        Container(
          width: double.infinity,
          height: 170.0,
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).dialogBackgroundColor),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: SingleChildScrollView(
                      child: Linkify(
                        onOpen: (link) async {
                          if(await canLaunch(link.url)) {
                            await launch(link.url);
                          } else {
                            throw 'Could not lanch $link';
                          }
                        },
                        textAlign: TextAlign.start,
                        text: _userProvider?.userInfo?.aboutMe,
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                    )
                  ),
                ),
                SizedBox(width: 4.0,),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(
                      context, 
                      '/changeInfo',
                      arguments: <String, dynamic> {
                        'type': 'aboutMe',
                      }
                    );
                  },
                  child: Container(
                    width: 30.0,
                    height: 30.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromARGB(0x38, 0xc8, 0xc8, 0xc8),
                    ),
                    child: Icon(
                      Icons.mode_edit,
                      size: 16.0,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  GestureDetector userNameWidget() {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context, 
          '/changeInfo',
          arguments: <String, dynamic> {
            'type': 'name',
          }
        );
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Color.fromARGB(0x38, 0xc8, 0xc8, 0xc8),
              ),
              child: Icon(
                Icons.mode_edit,
                size: 16.0,
                color: Colors.white,
              ),
            ),
          SizedBox(width: 8.0,),
          Container(
            width: 240.0,
            child: Align(
              alignment: Alignment.center,
              child: TextField(
                maxLines: 1,
                maxLength: 20,
                enabled: false,
                focusNode: _nameFieldFocus,
                controller: _nameWatcher,
                style: Theme.of(context).textTheme.bodyText1,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(),
                  isDense: true,
                  border: InputBorder.none,
                  counter: Offstage(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


  Widget actionBBon() {
    return GestureDetector(
      onTap: () {
        _launchDressView(DressSearchType.Crown);
      },
      child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Theme.of(context).dialogBackgroundColor),
          ),
        width: MediaQuery.of(context).size.width/3,
        height: MediaQuery.of(context).size.width/3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            BlurText(
              text: "뽄",
              textSize: 30.0,
            ),
            BlurText(
              text: "${_userProvider?.receivedActData?.awardCount??0}",
              textSize: 35.0,
              strokeWidth: 5.0,
              textLineColor: const Color.fromARGB(255, 140, 80, 40),
              blurColor: const Color.fromARGB(255, 250, 250, 250),
            ),
          ],
        ),
      ),
    );
  }

  Widget actionSummary() {
    return Wrap(
      direction: Axis.horizontal,
      alignment: WrapAlignment.start,
      children: [
        actionBBon(),
        actWidget("좋아요", _userProvider?.receivedActData?.receiveLikeCount??0, DressSearchType.MyDressSortLike, 'assets/like.svg', 'assets/in_arrow.svg'),
        actWidget("댓글", _userProvider?.receivedActData?.receiveReplyCount??0, DressSearchType.MyDressSortReply,  'assets/comment.svg', 'assets/in_arrow.svg'),

        actWidget("드레스", _userProvider?.userInfo?.dressCount??0, DressSearchType.MyDress, 'assets/dress.svg', null),
        actWidget("좋아요", _userProvider?.userInfo?.likeDressCount??0, DressSearchType.ActionLike, 'assets/like.svg', 'assets/out_arrow.svg'),
        actWidget("댓글", _userProvider?.userInfo?.replyCount??0, DressSearchType.ActionReply,  'assets/comment.svg', 'assets/out_arrow.svg')
      ],
    );
  }

  Widget actWidget(String title, int count, DressSearchType type, String icons, String icons2) {
    return GestureDetector(
      onTap: () {
        _launchDressView(type);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).dialogBackgroundColor),
        ),
        width: MediaQuery.of(context).size.width/3,
        height: MediaQuery.of(context).size.width/3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              width: 60.0,
              child: Stack(
                children: <Widget>[
                  SvgPicture.asset(
                    icons,
                    color: Colors.white70,
                    width: 44.0,
                    height: 44.0,
                  ),
                  Visibility(
                    visible: icons2 != null,
                    child: Positioned(
                      right: 0.0,
                      bottom: 0.0,
                      child: SvgPicture.asset(
                          icons2??"assets/in_arrow.svg",
                          color: const Color.fromARGB(255, 140, 80, 40),
                          width: 43.0,
                          height: 33.0,
                        ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 4.0,),
            Text(
              count.toString(), 
              style: Theme.of(context).textTheme.headline5.copyWith(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

   String getSearchDressUrl(DressSearchType type) {
     var userId = _userProvider?.userInfo?.id;
    switch(type) {
      case DressSearchType.Crown: 
        return '/dress/user/crown/$userId';
      break;
      case DressSearchType.MyDress: 
        return'/dress/user/$userId/LIKE';
      break;
      case DressSearchType.ActionLike: 
        return'/dress/user/act/like/$userId';
      break;
      case DressSearchType.ActionReply: 
        return'/dress/user/act/reply/$userId';
      break;
      case DressSearchType.MyDressSortLike:
        return'/dress/user/receive/reply/$userId';
      break;
      default: 
        return'/dress/user/receive/reply/$userId';
    }
  }
  _launchDressView(DressSearchType type){  
    String url = getSearchDressUrl(type);
    setState(() {
      _currentSearchType = type;
    });
    setSearchTypeStr();

    Navigator.pushNamed(
      context, 
      '/sigleContents',
      arguments: <String, dynamic> {
        'title': setSearchTypeStr(),
        'url': url,
        'enableReadMore': true
        }
      );
  }

  void searchDressBackPress() {
    setState(() {
      _currentSearchType = DressSearchType.NONE;
      _searchTypeStr = "";
    });
  }

@override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    if(_refreshUserData && _userProvider.userInfo != null) {
      _refreshUserData = false;
      _userProvider.reqReceivedInfo();
    }

    _nameWatcher.text = _userProvider?.userInfo?.name;
    _descriptionWatcher.text = _userProvider?.userInfo?.aboutMe;

    return SafeArea(
      
      child: Container(
        height: double.infinity,
        width: double.infinity,
        color: Theme.of(context).backgroundColor,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      headerView(),
                      Positioned(
                        top: 5.0,
                        right: 0.0,
                        child: IconButton(
                          onPressed: () {
                            Navigator.pushNamed(
                              context, 
                              '/setting',
                            );
                          },
                          icon: Icon(Icons.settings),
                          color: Colors.grey,
                          iconSize: 24.0,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 16.0,),
                  actionSummary(),
                  // iReceivedWidget(),
                ],
              ),
            )
          ],
        )
      ),
    );
  }
}