import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/MonthlyInfoProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class SummaryCalendarScreen extends StatefulWidget {
  @override
  _SummaryCalendarScreenState createState() => _SummaryCalendarScreenState();
}

class _SummaryCalendarScreenState extends State<SummaryCalendarScreen> {
  
  UserProvider userProvider;
  
  CalendarController _calendarController;

  DateTime _currentDate = new DateTime.now();
  int _year;
  int _month;
  int _day;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
    _year = _currentDate.year;
    _month = _currentDate.month;
    _day = _currentDate.day;
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  Map<DateTime, List> _convertEvent(List<ModelDress> data) {
    Map<DateTime, List> result = {};
    data.forEach((dress) => {
      result[dress.createdAt]=['dress']
    });
    return result;
  }

  void _showDailyEvent(year, month, day) {

  }

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of<UserProvider>(context);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => MonthlyInfoProvider(
            userId: userProvider.userInfo.id,
            year: _year,
            month: _month,
            day: _day
          ),
        ),
      ], 
      child: SafeArea(
        child: Container(
          child: Column(
            children: [
              Consumer<MonthlyInfoProvider>(
                builder: (context, provider, widget) {

                  final _event = _convertEvent(provider.dressList);
                  return TableCalendar(
                    locale: 'ko_KR',
                    calendarController: _calendarController,
                    events: _event,
                    calendarStyle: CalendarStyle(
                      markersColor: Colors.red,
                    ),
                    headerStyle: HeaderStyle(
                      headerMargin: EdgeInsets.only(left: 40, top: 10, right: 40, bottom: 10),
                      centerHeaderTitle: true,
                      formatButtonVisible: false,
                      leftChevronIcon: Icon(Icons.arrow_left, color: Colors.white,),
                      rightChevronIcon: Icon(Icons.arrow_right, color: Colors.white,),
                      titleTextStyle: const TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                    onDaySelected:(date, events, holidays) {
                      _showDailyEvent(date.year, date.month, date.day);
                    },
                    onVisibleDaysChanged: (firstDate, lastDate, format) {
                      provider.getMonthlyDress(
                        userProvider.userInfo.id,
                        _calendarController.focusedDay.year,
                        _calendarController.focusedDay.month
                      );
                    },
                    onCalendarCreated: (DateTime first, DateTime last, CalendarFormat format) {
                      Print.e(' onCalendarCreated $first');
                    },
                  );
                },
              ),
              Container(
                child: Text('ahahahah'),
              )
            ],
          ),
        ),
      ),
    );
  }
}