import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:provider/provider.dart';


class ChangeUserInfo extends StatefulWidget {
  
  //type: name, aboutMe
  final String type;
  ChangeUserInfo({
    Key key,
    this.type,
  }):super(key: key);
  

  @override
  _ChangeUserInfoState createState() => _ChangeUserInfoState();
}

class _ChangeUserInfoState extends State<ChangeUserInfo> {

  final TextEditingController _inputWatcher = TextEditingController();
  UserProvider _userProvider;

  String _result = null;
  String _nameRule = "";

  @override
  void initState() {
    super.initState();
    _nameRule =
      widget.type == "name"
      ? "※ 이름은 3-20자리 까지 가능합니다.\n※ 특수 문자를 사용할 수 없습니다."
      : "※ 최대 170자를 적을 수 있습니다.";
  }

  Container _changeName() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 16.0,),
            Container(
              width: double.infinity,
              height: 70.0,
              child: TextField(
                maxLines: 1,
                maxLength: 20,
                maxLengthEnforced: true,
                controller: _inputWatcher,
                onChanged: (text) {
                  if(_result != null) {
                    setState(() {
                      _result = null;
                    });
                  }
                }, 
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9ㄱ-ㅎ가-힣]')),
                ],
                style: Theme.of(context).textTheme.bodyText1,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(12.0),
                  isDense: true,
                  border: OutlineInputBorder (
                    borderSide: BorderSide(color: Colors.black )
                  ),
                  enabledBorder: new OutlineInputBorder(
                    borderSide:  BorderSide(color: Colors.black ),
                  ),
                  focusedBorder: new OutlineInputBorder(
                    borderSide:  BorderSide(color: Colors.black ),
                  ),
                ),
              ),
            ),           
          ],
        ),
      ),
    ) ;
  }

  void _updateUserInfo(String type) async {
    var inputWord = _inputWatcher.text.trim();

    if(type == "name") {
      if(inputWord.length == 0) {
        setState(() {
          _result = "이름을 입력해주세요!";
        });
        return;
      }

      if(inputWord.length < 3) {
        setState(() {
          _result = "최소 3자리 문자를 입력해주세요!";
        });
        return;
      }

      if(inputWord.length > 20) {
        setState(() {
          _result = "최대 20자리 까지 가능합니다.";
        });
        return;
      }

      var pattern = RegExp('[a-zA-Z0-9가-힣]');
      if(!pattern.hasMatch(inputWord)) {
        setState(() {
          _result = "사용할 수 없는 문자가 있습니다.";
        });       
        return;
      }

    }

    


    var result = await _userProvider?.modifyUserInfo(
      json.encode({
        type: _inputWatcher.text.trim()
      })
    );

    if(result is ModelUser) {
      // Navigator.pop(context);
      setState(() {
        _result = "변경되었습니다.";
      });
    } else {
      _result = "변경을 실패했습니다. ($result)";
    }

  }

  Container _changeDesc() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: TextField(
          keyboardType: TextInputType.multiline,
          maxLines: 8,
          maxLength: 170,
          maxLengthEnforced: true,
          controller: _inputWatcher,
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(12.0),
            isDense: true,
            border: OutlineInputBorder (
              borderSide: BorderSide(color: Colors.black )
            ),
            enabledBorder: new OutlineInputBorder(
              borderSide:  BorderSide(color: Colors.black ),
            ),
            focusedBorder: new OutlineInputBorder(
              borderSide:  BorderSide(color: Colors.black ),
            ),
            
          ),
        ),
      ),
    ) ;
  }

  Widget _infoArea() {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: _result == null,
              child: Text (
                _nameRule,
                style: Theme.of(context).textTheme.bodyText2.apply(color: Color.fromARGB(255, 159, 54,32))
              ),
            ),
            Visibility(
              visible: _result != null,
              child: Text(
                _result == null? _nameRule: _result,
                style: Theme.of(context).textTheme.button,
              ),
            ),
          ],
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);

    if(widget.type == "name") {
      _inputWatcher.text = _userProvider.userInfo.name;
    } else {
      _inputWatcher.text = _userProvider.userInfo.aboutMe;
    }
    

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Theme.of(context).backgroundColor,
          child: Column(
            children: [
              HeaderWithBack(title: widget.type=="name"? "이름 변경": "하고픈 말 변경", callback: () {
                Navigator.pop(context);
              },),
              SizedBox(height: 26.0,),
              widget.type=="name"
              ? _changeName()
              : _changeDesc(),
              _infoArea(),
              SizedBox(height: 26.0,),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: GestureDetector(
                  onTap: () {
                    _updateUserInfo(widget.type);
                  },
                  child: Container(
                    width: double.infinity,
                    height: 60.0,
                    color: Colors.black45,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text (
                        '변경',
                        style: Theme.of(context).textTheme.button,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}