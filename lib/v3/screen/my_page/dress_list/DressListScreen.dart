import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/provider/DressProvider.dart';
import 'package:itswd/v3/screen/dress_view/DressGrid.dart';
import 'package:itswd/v3/screen/dress_view/DressList.dart';
import 'package:provider/provider.dart';

class DressListScreen extends StatefulWidget {

  final String url;
  final String title;
  final bool enableReadMore;

  DressListScreen({
    Key key, 
    this.title,
    this.url,
    this.enableReadMore
  }): super(key: key);
  

  @override
  _DressListScreenState createState() => _DressListScreenState();

}

class _DressListScreenState extends State<DressListScreen> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  int _selectedPos = 0;
  bool _isGridView = true;
  DressProvider _provider;
  bool isFinish = false;

  bool _historyBack() {

    if(_isGridView) {
      setState((){
        isFinish = true;
      });

      return true;
    } else {
      setState(() {
        _isGridView = true;
      });
      return false;
    }
  }
  void _readMore() {
    _provider.readMore();
  }

  void _gridItemSelected(int index, double offset) {
    
    setState(() {
      _isGridView = false;
      _selectedPos = index;    
    });
  }


  Widget _content() {
    return 
    isFinish
    ? Container()
    : Column(
      children: [
        HeaderWithBack(
          title: widget.title, 
          callback: () {
            if(_historyBack()) {
              Navigator.pop(scaffoldKey.currentContext);
            }
          },
        ),
        Expanded(
          child: Consumer<DressProvider>(
            builder: (context, provider, _) {
              _provider = provider;
              return IndexedStack(
                  index: _isGridView?0:1,
                  children: [
                    DressGrid(
                      dressList: provider.getDressList(), 
                      canReadMore: provider.canReadMore(), 
                      readMore: _readMore, 
                      callback: _gridItemSelected,
                    ),
                    DressList(
                      dressList: provider.getDressList(), 
                      canReadMore: provider.canReadMore(), 
                      readMore: _readMore, 
                      selectedPos: _selectedPos,
                    ),
                  ],
                );
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => DressProvider(widget.url, widget.enableReadMore),),
      ],
      child: WillPopScope(
        onWillPop: () async {
          bool result = _historyBack();
          return  result;
        },
        child: Scaffold(
            key: scaffoldKey,
            body: SafeArea(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Theme.of(context).backgroundColor,
                child: _content()
              )
            )
          ),
      ),
    );
  }
}