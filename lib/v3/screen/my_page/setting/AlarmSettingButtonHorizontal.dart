import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AlarmSettingButtonHorizontal extends StatefulWidget {

  final String title;
  final bool state;
  final double width;
  final double height;
  final Function callback;
  AlarmSettingButtonHorizontal({
    Key key,
    this.title,
    this.width = double.infinity,
    this.height = 55.0,
    this.state,
    this.callback
  }) : super(key: key);

  @override
  _AlarmSettingButtonHorizontalState createState() => _AlarmSettingButtonHorizontalState();
}

class _AlarmSettingButtonHorizontalState extends State<AlarmSettingButtonHorizontal> {

  bool _state;
  @override
  void initState() {
    setState(() {
       _state = widget.state;
    });
    super.initState();
  }

  void _changeState() {
    setState(() {
      _state = !_state;
    });
    widget.callback(_state);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _changeState,
      child: Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
          border: Border.all(
            color:_state? Colors.red[200]:Colors.white
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.title, 
                style: Theme.of(context).textTheme.subtitle1.copyWith(color:_state? Colors.red[200]:Colors.white),
              ),
              Switch(
                value: _state,
                onChanged: (value) {
                  _changeState();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}