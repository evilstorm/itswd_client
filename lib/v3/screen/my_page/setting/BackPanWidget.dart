import 'package:flutter/material.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';

class BackPanWidget extends StatelessWidget {

  Widget child;
  String title;

  BackPanWidget({
    Key key,
    this.child,
    this.title
  }): super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Theme.of(context).backgroundColor,
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: <Widget>[
              HeaderWithBack(
                title: title,  
                callback: () => Navigator.pop(context),
              ),
              Expanded(child: child)
            ],
          ),
        ),
      ),
    );
  }
}