import 'package:flutter/material.dart';

class MovePageTextBtn extends StatelessWidget {
  final String title;
  final Function callback;
  const MovePageTextBtn({
    Key key,
    this.title,
    this.callback
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Row(
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(width: 8.0,),
          Icon(
            Icons.keyboard_arrow_right,
            size: 24.0,
            color: Colors.white,
          )
        ],
      ),
    );
  }
}
