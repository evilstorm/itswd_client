import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/SharedPref.dart';
import 'package:itswd/v3/components/common/SharedPrefKeys.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/components/views/WidgetNotify.dart';
import 'package:itswd/v3/components/views/WidgetTerm.dart';
import 'package:itswd/v3/model/response/ModelNotify.dart';
import 'package:itswd/v3/model/response/ModelTerm.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/my_page/setting/BackPanWidget.dart';
import 'package:itswd/v3/screen/my_page/setting/AlarmSettingButton.dart';
import 'package:itswd/v3/screen/my_page/setting/AlarmSettingButtonHorizontal.dart';
import 'package:itswd/v3/screen/my_page/setting/MovePageTextBtn.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  
  UserProvider userProvider;

  @override
  void initState() {
    super.initState();    
  }

  void _backPress() => Navigator.pop(context);

  var _state = false;

  void _likeAlarmStateChange(state) async {
    var response = await ApiHelper().patch('/setting/', json.encode({
      'likeAlarm': state
    }));

    if(response['code'] == 200) {
      userProvider.userInfo.setting.likeAlarm = state;
    } else {
      Print.e(response['message']);
    }


  }
  void _replyAlarmStateChange(state) async {
    var response = await ApiHelper().patch('/setting/', json.encode({
      'replyAlarm': state
    }));

    if(response['code'] == 200) {
      userProvider.userInfo.setting.replyAlarm = state;
    } else {
      Print.e(response['message']);
    }
  }
  void _systemAlarmStateChange(state) async {
    var response = await ApiHelper().patch('/setting/', json.encode({
      'systemAlarm': state
    }));

    if(response['code'] == 200) {
      userProvider.userInfo.setting.systemAlarm = state;
    } else {
      Print.e(response['message']);
    }
  }

  void _moveNotify() async {
    var response = await ApiHelper().get('/notify/count/50');
    if(response['code'] == 200) {
      List<ModelNotify> notifyList = List();
      if (response['data'] != null) {
        notifyList = List<ModelNotify>();
        response['data'].forEach((v) {
          notifyList.add(ModelNotify.fromJsonDynamic(v));
        });
      }

      Navigator.push(context, 
        MaterialPageRoute(
          builder: (context) => BackPanWidget( title: '공지사항', 
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: WidgetNotify(
              data: notifyList, 
              callback: (seq){
                Navigator.pop(context);
              } ,),
          )
          )
        )
      );
    }
  } 


  void _moveUsedTerm() async {
    var response = await ApiHelper().get('/term/last');       
    if(response['code'] == 200) {
      ModelTerm term = ModelTerm.fromJsonDynamic(response['data']);
      Navigator.push(context, 
        MaterialPageRoute(
          builder: (context) => BackPanWidget( title: '이용약관', 
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: WidgetTerm(
              data: term, 
              justShow: true, 
              callback: (seq) { 
                Navigator.pop(context); 
              }
            ),
          ),),
        ),
      );
    }
  } 

  void _logout() async{
    await setInt(SharedPrefKeys.AGREE_TERM_VER_I, -1);
    await setInt(SharedPrefKeys.READ_NOTIFY_INDEX_I, -1);
    await setString(SharedPrefKeys.JWT_TOKEN, null);
    await setInt(SharedPrefKeys.LOGIN_TYPE_I, -1);
    
    exit(0);
  }


  @override
  Widget build(BuildContext context) {
    
    userProvider = Provider.of<UserProvider>(context);
    var _isSelections = [
      userProvider?.userInfo?.setting?.likeAlarm??true, 
      userProvider?.userInfo?.setting?.replyAlarm??true,
      userProvider?.userInfo?.setting?.systemAlarm??true
      ];
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
        body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Theme.of(context).backgroundColor,
          child: Column(
            children: <Widget>[
              HeaderWithBack(title: '설정', callback: _backPress,),
              SizedBox(height: 16.0,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    AlarmSettingButton(title: '좋아요 알림', state: _isSelections[0], width: width/2.5 ,callback: _likeAlarmStateChange,),
                    AlarmSettingButton(title: '댓글 알림', state: _isSelections[1], width: width/2.5, callback: _replyAlarmStateChange,),
                  ],
                ),
              ),
              SizedBox(height: 16.0,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: AlarmSettingButtonHorizontal(title: '시스텀 알림', state: _isSelections[2], callback: _systemAlarmStateChange,),
              ),
              SizedBox(height: 24.0,),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: MovePageTextBtn(title: '공지사항', callback: _moveNotify),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: MovePageTextBtn(title: '이용약관', callback: _moveUsedTerm),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: MovePageTextBtn(title: '로그아웃', callback: _logout),
              )
            ],
          ),
        ),
      ),
    );
  }
}