import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:provider/provider.dart';

import 'package:itswd/v3/components/views/UserIcon.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/model/response/ModelReply.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/provider/ReplyProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';

class ReplyScreen extends StatefulWidget {
  
  ModelDress dress;
  ReplyScreen({
    Key key,
    this.dress,
  }):super(key: key);

  
  @override
  _ReplyScreenState createState() => _ReplyScreenState();


}

class _ReplyScreenState extends State<ReplyScreen> {

  DateTime now = DateTime.now();

  // List<ModelReply> _replyList = List();
  final TextEditingController _sayWatcher = TextEditingController();
  ScrollController _scrollController = ScrollController();

  bool _scrollEnd = false;
  UserProvider _userProvider;
  // DressProvider _dressProvider;
  ReplyProvider _replyProvider;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if(_scrollEnd) {      
        return;
      }
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
      } 
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void postReply() async {
    if(_sayWatcher.text.length == 0) {
      return;
    }

    _replyProvider.postReply(
      json.encode({
        'owner': _userProvider?.userInfo?.id,
        'parent': widget.dress.id,
        'say': _sayWatcher.text
      })
    );

    setState(() {
      widget.dress.replyCount++;
      _userProvider.notifyListeners();

    });
    
    _sayWatcher.text = "";
  }

  Widget emptyView() {
    return Center(
      child: Text('댓글이 없어요.', style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.grey),),
    );
  }

  String getName(ModelUser user) {
    if(user == null) {
      return "unknown";
    }
    if(user.name == null) {
      return user.email.split("@")[0];
    }
    return user.name;
  }

  Widget addContent(context, index) {
    ModelReply item = _replyProvider.resp[index];

    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0,),
      child: Container(
        width: double.infinity,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            UserIcon(user: item.owner, size: 30, showName: false,),
            SizedBox(width: 8.0,),
            Expanded(
              child: RichText(
                softWrap: true,
                text: TextSpan(children: <TextSpan>  [
                  TextSpan(
                    text: getName(item.owner),
                    style: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w900),
                  ),
                  TextSpan(
                    style: Theme.of(context).textTheme.bodyText2,
                    text: "  ${item.say}"
                  ),
                  TextSpan(
                    text: "\n${timeago.format(item.createdAt, locale:'ko')}", 
                    // text: "\n${DateCalcUtil.timeAgo(item.createdAt)}", 
                    style: Theme.of(context).textTheme.caption,
                  ),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }



  Widget replyList() {
    return ListView.builder(
          controller: _scrollController,
          itemCount: _replyProvider.resp.length,
          itemBuilder: (context, index) => addContent(context, index),
    );
  }

  Widget replyPost() {
    return _userProvider.userInfo?.id != null
      ? Container(
      width: double.infinity,
      height: 80.0,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SizedBox(width: 16.0,),
          Expanded(
            child: TextField(
              controller: _sayWatcher,
              maxLength: 90,
              style: Theme.of(context).textTheme.bodyText1,
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)
                ),
                hintText: '입력하세요',
              ),
            ),
          ),
          SizedBox(width: 8.0,),
          Transform.rotate(angle: -39.0 * pi/180,
          child: IconButton(
              icon: Icon(Icons.send),
              iconSize: 40.0,
              color: Theme.of(context).accentColor,
              onPressed: () {
                postReply();
              },
            ),
          ),
          SizedBox(width: 8.0,),
        ],
      ),
    )
    :
    Container(
      child: Text('로그인 후 작성 가능합니다. '),
    );
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = context.watch<UserProvider>();
    // _dressProvider = context.watch<DressProvider>();

    return MultiProvider(
      providers: [        
        ChangeNotifierProvider(create: (_) => ReplyProvider(dressId: widget.dress.id),)
      ],
      child: Scaffold(
        appBar: AppBar(
            title: Text("댓글",),
          ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Theme.of(context).backgroundColor,
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Hero(
                    tag: widget.dress.id,
                    child: CachedNetworkImage(
                    imageUrl: widget.dress.images[0],
                    imageBuilder: (context, imageProvider) => Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover
                        )
                      ),
                    ),
                    placeholder: (context, url) => Icon(Icons.panorama),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  SizedBox(width: 8.0,),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        widget.dress.say,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.bodyText1,
                        maxLines: 2,
                      ),
                    ),
                  )
                ],
              ),
              Expanded(
                child: Consumer<ReplyProvider> (
                  builder: (context, reply, widget){
                    _replyProvider = reply;
                    return reply.resp.length > 0? replyList(): emptyView();
                  },
                )
              ),
              replyPost(),
              SizedBox(height: 32.0),
            ],
          )
        ),
      ),
    );
  }
}