import 'package:flutter/material.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/BaseDressProvider.dart';
import 'package:itswd/v3/screen/dress_view/DressGrid.dart';
import 'package:itswd/v3/screen/dress_view/DressList.dart';
import 'package:itswd/v3/components/common/Print.dart';
enum ViewType {
  Grid, List
}

class DressView extends StatefulWidget {

  final ViewType viewType;
  final BaseDressProvider dressProvider;
  final Function showTypeChange;

  DressView({
    Key key,
    @required this.dressProvider,
    @required this.viewType,
    @required this.showTypeChange,
  }): super(key: key);

  @override
  _DressViewState createState() => _DressViewState();
}

class _DressViewState extends State<DressView> {

  int _selectedPos = 0;

  @override
  void initState() {
    super.initState();

    setState(() {
          _selectedPos = 0;
    });
  }


  void _readMore() {
    widget.dressProvider.readMore();
  }


  void _gridItemSelected(int index, double offset) {
    
    widget.showTypeChange(ViewType.List);

    setState(() {
      _selectedPos = index;    
    });
  }

  int _listType() {
    if(widget.viewType == ViewType.Grid) {
      return 0;
    }
    return 1;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: double.infinity,
      child: IndexedStack(
        index: widget.viewType == ViewType.Grid?0:1,
        children: [          
            DressGrid(
              dressList: widget.dressProvider.getDressList(), 
              canReadMore: widget.dressProvider.canReadMore(), 
              readMore: _readMore, 
              callback: _gridItemSelected,
            ),
            DressList(
              dressList: widget.dressProvider.getDressList(), 
              canReadMore: widget.dressProvider.canReadMore(), 
              readMore: _readMore, 
              selectedPos: _selectedPos,
          ),
        ],
      ),
    );
  }
}