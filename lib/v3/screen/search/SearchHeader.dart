import 'dart:async';

import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/SearchTagProvider.dart';
import 'package:provider/provider.dart';

class SearchHeader extends StatefulWidget {
  
  final Function callback;
  final double height;
  SearchHeader({
    Key key, 
    @required this.height,
    this.callback,
  }): super(key: key);
  
  
  @override
  _SearchHeaderState createState() => _SearchHeaderState();


}

class _SearchHeaderState extends State<SearchHeader> {

  final TextEditingController _searchFieldController = TextEditingController();
  SearchTagProvider provider;
  Timer searchTimer;

  void _reqTagList(String tag) {
    if(searchTimer != null) searchTimer.cancel();
    
    provider.inputProgress();

    searchTimer = Timer(Duration(milliseconds: 500), () {
      if(tag.startsWith("#")) {
        tag = tag.replaceFirst("#", "");
      }
    
      provider.searchTag(tag);
    });
  }

  Widget _tagListItem(BuildContext context, int index) {
    Print.d(" Search Tag provider : ${provider.tagList[0]}");
    return GestureDetector(
      
      onTap: () {
        setState(() {
          _searchFieldController.text = "";          
        });
        widget.callback(provider.tagList[index].tag);        
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          color: Theme.of(context).dialogBackgroundColor,
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "#${provider.tagList[index].tag}",
                style: Theme.of(context).textTheme.bodyText1
              ),
              Text(
                "개시물: ${provider.tagList[index].count}개",
                style: Theme.of(context).textTheme.bodyText2,

              )
            ],
          ),
        ),
      ),
    );
  }


  Widget _searchResultWidget() {
    Print.d(" Search Tag provider.state: ${provider.state}");
    
    if(_searchFieldController.text == "") {
      return SizedBox(width: 0, height: 0,);
    }

    if(provider.state == ProviderState.DONE 
      || provider.state == ProviderState.DATA_CHANGE) {
        return (provider.tagList.length == 0)
        ? _searchBox(Text('${provider.currentTag} 검색 결과 없음요!'))
        : Container(
          color: Theme.of(context).dialogBackgroundColor,
          width: double.infinity,
          height: provider.tagList.length>4? 200:150,
          child: ListView.builder(
            itemCount: provider.tagList.length,
            itemBuilder: (context, index) => _tagListItem(context, index),
          )
        );
      }

    return _searchBox(Text('${_searchFieldController.text} 검색 중'));
  }

  Widget _searchBox(Widget widget) {
    return Container(
      width: double.infinity,
      color: Theme.of(context).dialogBackgroundColor,
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: widget,
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    
    provider = Provider.of<SearchTagProvider>(context);
    Print.e("_searchFieldController.text: ${_searchFieldController.text}");

    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: widget.height,
              decoration: BoxDecoration(
                border: Border.all()
              ),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          maxLines: 1,
                          textAlign: TextAlign.left,
                          onSubmitted: (_) => widget.callback(_searchFieldController.text),
                          controller: _searchFieldController,
                          onChanged: (_) => _reqTagList(_),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.zero,
                            hintText: '#테그 검색',
                            border: InputBorder.none,
                          ),
                          style: Theme.of(context).textTheme.button,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.search),
                        color: Colors.white,
                        iconSize: 28.0,
                        onPressed: () => widget.callback(_searchFieldController.text),
                      )
                    ],
                  ),
                ),
              ),
            ),
            _searchResultWidget()
          ],
        ),
      ),
  );
  }
}