import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/provider/BaseDressProvider.dart';
import 'package:itswd/v3/provider/HotDressProvider.dart';
import 'package:itswd/v3/provider/SearchDressProvider.dart';
import 'package:itswd/v3/screen/search/DressView.dart';
import 'package:itswd/v3/screen/search/SearchHeader.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  SearchScreen({
    Key key,
  }): super(key: key);
  
  final _SearchScreenState state = _SearchScreenState();
  
  @override
  _SearchScreenState createState() => state;

  void screenSelected() {
    state._screenSelected();
  }

  bool backPress() {
    return state._historyBack();
  }

}

class _SearchScreenState extends State<SearchScreen> {
  
  bool _showSearchHeader = true;
  String _searchTag = "";
  bool _showHotDress = true;
  bool _hotGrid = true;
  bool _searchGrid = true;

  SearchDressProvider _searchDressProvider;


  void _screenSelected() {

  }

  void _search(String tag) {
    Print.e("Search Tag: $tag");
    _searchDressProvider?.searchDressWithTag(tag);
    setState(() {
      _searchTag = tag;
      _showSearchHeader = false;
      _showHotDress = false;
      _searchGrid = true;
    });
  }

  Widget _hotDressWidget(BaseDressProvider provider) {

    return DressView(
          dressProvider: provider,
          viewType: _hotGrid?ViewType.Grid: ViewType.List,
          showTypeChange: _hotDressShowTypeChange,
        );
  }

  void _hotDressShowTypeChange(ViewType type) {
    if(type == ViewType.Grid) {
      setState(() {
        _hotGrid = true;
        _showSearchHeader = true;
      });
    } else {
      setState(() {
        _searchTag = '인기순';
        _hotGrid = false;
        _showSearchHeader = false;
      });
    }
  }


  void _searchDressShowTypeChange(ViewType type) {
    bool isGridView = true;
    if(type == ViewType.Grid) {
      isGridView = true;
    } else {
      isGridView = false;
    }

    setState(() {
      _searchGrid = isGridView;    
    });
  }

  Widget _searchDressWidget(BaseDressProvider provider) {
    return DressView(
      dressProvider: provider,
      viewType: _searchGrid?ViewType.Grid: ViewType.List,
      showTypeChange: _searchDressShowTypeChange,
    );
  }


  bool _historyBack() {
    if(_showSearchHeader && _hotGrid) {
      //NOT Action 
      return true;
    }
    
    if(_showHotDress && !_hotGrid) {
      setState(() {
        _showSearchHeader = true;
        _hotGrid = true;
      });
      return false;
    }

    //인기 드레스의 리스트 화면에서
    //검색 불가이기 때문에 서치 화면에서 인기 드레스로 이동 할 때
    //해더는 검색이다. 
    if(_searchGrid) {
      setState(() {
        _showSearchHeader = true;
        _showHotDress = true;
      });

      return false;
    }
    
    setState(() {
      _searchGrid = true;
    });
    return false;
  }

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => HotDressProvider()),
      ChangeNotifierProvider(create: (_) => SearchDressProvider()),
    ],
      child: SafeArea(
        child: Stack(
          children: [
            Positioned.fill(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 0.0),
                child: _showHotDress
                ? Consumer<HotDressProvider>(
                    builder: (context, provider, widget) {
                      return _hotDressWidget(provider);
                    }
                  )
                : Consumer<SearchDressProvider>(
                    builder: (context, provider, widget) {
                      if(_searchDressProvider == null) {
                        provider.searchDressWithTag(_searchTag);
                      }
                      _searchDressProvider = provider;
                      return _searchDressWidget(provider);
                    }
                  ),
                ),
              top: _showSearchHeader? 72.0 : 42.0,
            ),
            _showSearchHeader
            ? SearchHeader(height: 60.0, callback: (tag) => _search(tag))
            : Positioned(
              child: HeaderWithBack(
                title: _searchTag, 
                callback: () => _historyBack()
                ),
              top: 0,
            ),

          ],
        ),
      ),
    );
  }
}