import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/provider/SearchDressProvider.dart';
import 'package:itswd/v3/screen/dress_view/DressGrid.dart';
import 'package:itswd/v3/screen/dress_view/DressList.dart';
import 'package:provider/provider.dart';

class SearchTagScreen extends StatefulWidget {

  final String tagWord;
  SearchTagScreen({
    Key key,
    this.tagWord
  }): super(key: key);

  @override
  _SearchTagScreenState createState() => _SearchTagScreenState();
}

class _SearchTagScreenState extends State<SearchTagScreen> {

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  SearchDressProvider _provider;
  int _selectedPos = 0;
  bool _isGridView = true;


  bool _historyBack() {
    if(_isGridView) {
      Navigator.pop(scaffoldKey.currentContext);
      return true;
    } else {
      setState(() {
        _isGridView = true;
      });
      return false;
    }
  }

  void _readMore() {
    _provider.readMore();
  }


  void _gridItemSelected(int index, double offset) {
    
    setState(() {
      _isGridView = false;
      _selectedPos = index;    
    });
  }


  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SearchDressProvider(tag: widget.tagWord)),
      ],
      child: WillPopScope(
        onWillPop: () async {
          Print.i("SearchTagScreen onWillPop");

          return _historyBack();
        },
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          key: scaffoldKey,
          body: SafeArea(
            child: Container(
              height: double.infinity,
              width: double.infinity,
              child: Column(
                children: [
                  HeaderWithBack(
                      title: widget.tagWord, 
                      callback: () => _historyBack()
                  ),
                  Consumer<SearchDressProvider>(
                    builder: (context, provider, widget) {
                      _provider = provider;
                      return Expanded(
                        child: IndexedStack(
                          index: _isGridView?0:1,
                          children: [
                            DressGrid(
                              dressList: provider.getDressList(), 
                              canReadMore: provider.canReadMore(), 
                              readMore: _readMore, 
                              callback: _gridItemSelected,
                            ),
                            DressList(
                              dressList: provider.getDressList(), 
                              canReadMore: provider.canReadMore(), 
                              readMore: _readMore, 
                              selectedPos: _selectedPos,
                            ),

                          ],
                        ),
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}