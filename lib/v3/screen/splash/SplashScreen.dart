import 'package:flutter/material.dart';
import 'package:itswd/v3/components/Contants.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/common/SharedPref.dart';
import 'package:itswd/v3/components/common/SharedPrefKeys.dart';
import 'package:itswd/v3/components/common/signin/SignInBBon.dart';
import 'package:itswd/v3/components/views/CircularProgress.dart';
import 'package:itswd/v3/components/views/WidgetMustAppUpdate.dart';
import 'package:itswd/v3/components/views/WidgetNotify.dart';
import 'package:itswd/v3/components/views/WidgetTerm.dart';
import 'package:itswd/v3/model/response/ModelSplash.dart';
import 'package:itswd/v3/network/ApiHelper.dart';


class SplashScreen extends StatefulWidget {

  SplashScreen({
    Key key,
  }): super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var _showLoading = true;
  ApiHelper _apiHelper = ApiHelper();

  var _showNotify = true;
  var _showTerm = true;
  var _moveScreen = false;
  var _showSignBtns = false;

  @override
  void initState(){
    super.initState();
    Print.e("initState  !!!!!!!!!");

  }
  Future<ModelSplash> _reqSplashData() async {
    Print.e("_reqSplashData");
    final int appSeq = APP_VER;
    final int notiSeq = await getInt(SharedPrefKeys.READ_NOTIFY_INDEX_I);
    final int termSeq = await getInt(SharedPrefKeys.AGREE_TERM_VER_I);

    final response = await _apiHelper.get('/init/splash/$appSeq/$notiSeq/$termSeq');
    return ModelSplash.fromJson(response);
  }

  Widget _exceptionPage() {
    Print.e("_exceptionPage");
    return Container(
      child: Center(
        child: Text(
          '죄송합니다.\n문제가 생겼습니다.\n\n잠시 후 다시 시도 해주세요.',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle1
        ),
      ),
    );
  }

  void _notifyTap(int seq) {
    Print.e("_notifyTap: $seq");
    setInt(SharedPrefKeys.READ_NOTIFY_INDEX_I, seq);
    setState(() {
      _showNotify = false;
    });
    moveNextPage();
  }

  void _termTap(int seq) {
    Print.e("_termTap: $seq");
    setInt(SharedPrefKeys.AGREE_TERM_VER_I, seq);
    setState(() {
      _showTerm = false;
    });
    moveNextPage();
  }

  void moveNextPage( {bool checkPass = false}) async {

    Print.e("moveNextPage");

    if(!checkPass && (_showNotify || _showTerm)) {
      return;
    }
    Print.e("moveNextPage 11111111");

    SignInBBon(signinResult: _signinResult).autoSignIn();
  }

  void _signinResult(result) {
    Print.e("moveNextPage 555555555");
    Print.e("_signinResult $result");

    if(result['result'] == false) {
      Navigator.pushReplacementNamed(context, '/sign');
    } else {
      Navigator.pushReplacementNamed(
        context, 
        '/main',
        arguments: <String, dynamic> {
        'userInfo': result['data'],
        }
      );
    }
  }

  Widget _contentView(ModelSplash data) {
    Print.e("_contentView");
    if(data.term == null && data.appUpdate == null && data.notify.isEmpty) { 
    Print.e("_contentView 11111111");
      moveNextPage(checkPass: true);

      return CircularProgress();
    }
    Print.e("_contentView 2222222222");
    return Container(
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Visibility(
              child: WidgetNotify(data: data.notify, callback: _notifyTap,),
              visible: data.notify != null && data.notify.length > 0 && _showNotify,
              ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Visibility(
              child: WidgetTerm(data: data.term, callback: _termTap,),
              visible: data.term != null && _showTerm,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Visibility(
              child: WidgetMustAppUpdate(data: data.appUpdate),
              visible: data.appUpdate != null 
              ),
          ),
        ],
      ),
    );


  }

  @override
  Widget build(BuildContext context) {

    // setString(SharedPrefKeys.JWT_TOKEN, null);
    // setInt(SharedPrefKeys.LOGIN_TYPE_I, -1);

    
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Theme.of(context).backgroundColor,
          child: 
            FutureBuilder<ModelSplash>(
                future: _reqSplashData(),
                builder: (context, snapshot) {
                  
                  if(snapshot.hasData) {
                    return _contentView(snapshot.data);
                  } else if(snapshot.hasError) {
                    Print.e(" SnapShot Error ${snapshot.error}");
                    return _exceptionPage();               
                  } else {
                    return  CircularProgress();
                  }
                }
              )
        ),
      ),
    );
  }
}