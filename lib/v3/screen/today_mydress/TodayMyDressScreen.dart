import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:itswd/v3/components/views/ReadMoreText.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:itswd/v3/components/common/FireStorage.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/CircularProgress.dart';
import 'package:itswd/v3/components/views/TextFieldTag.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/provider/DailyDressProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/components/common/DateUtils.dart' as DateUtil;


class TodayMyDressScreen extends StatefulWidget {

  TodayMyDressScreen({
    Key key,
  }): super(key: key);
  
  final _TodayMyDressScreenState state = _TodayMyDressScreenState();
  
  @override
  _TodayMyDressScreenState createState() => state;

  void screenSelected() {
    state._screenSelected();
  }

  bool backPress() {
    return state._historyBack();
  }

}

class _TodayMyDressScreenState extends State<TodayMyDressScreen> {

  final scaffoldKey = GlobalKey<ScaffoldState>();
  final textSay = TextEditingController();

  UserProvider userProvider;
  DailyDressProvider dailyDressProvider;


  TextFieldTag inputWordWidget;

  File _image;

  void _screenSelected() {

  }
  bool _historyBack() {
    return true;
  }

  Future getImage() async {
  var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(image == null) {
      return;
    }
    setState(() {
      _image = image;
    });
    
  }

  void showSnackMsg(String msg) {
    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        duration: Duration(milliseconds: 2000),
        content: Text(msg, style: Theme.of(scaffoldKey.currentContext).textTheme.bodyText2,),
        backgroundColor: Theme.of(scaffoldKey.currentContext).dialogBackgroundColor
      ) ,
    );
  }
  
  Future<String> uploadImages(file) async {
    FireStorage fs = new FireStorage();
    Print.i(" FireStorage fs : $fs");
    return await fs.uploadFile('gevilstorm@gmail.com', file);
  }


  
  Widget addImage() {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: GestureDetector(
        onTap: () {
          getImage();
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[
              Positioned.fill(child: Icon(Icons.add)),
              Positioned.fill(
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(19.0),
                    side: BorderSide(color: Colors.red)
                  ),
                  color: Theme.of(context).backgroundColor,
                  onPressed: () {
                    getImage();
                  },
                  child: Icon(
                    Icons.add,
                    size: 100.0,
                  )
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget postDesc () {
    if(inputWordWidget == null) {
      inputWordWidget = TextFieldTag(key: Key('tag'), witdh: double.infinity,);
    }
    
    return inputWordWidget;
  }

  void _showLoadingDialog() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context){
        return CircularProgress();
    });
  }

  void _postingBBon() async {
    if(_image == null) {
      showSnackMsg('뽐의 기본은 사진!');
      return;
    }
    
    _showLoadingDialog();

    List<String> downloadUrls = new List();
    String uploadUrl = await uploadImages(_image);

    downloadUrls.add(uploadUrl);    
    Print.i(downloadUrls);

    Navigator.pop(context);

    ModelDress dress = await userProvider.postTodayDress(
      downloadUrls, 
      inputWordWidget.getText(), 
      inputWordWidget.getTags()
    );

    dailyDressProvider.setTodayDress(dress);
    userProvider.userInfo.dressCount += 1;

  }

  Widget _todayDressPost() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 16.0,),
            Text(
              '오늘 뽄 좀 지아볼까?',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(height: 16.0,),
              _image == null
              ? addImage()
              : GestureDetector(
                onTap: () => getImage(),
                child: Stack(
                  children: [
                    Image.file(
                      _image, 
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                    ),
                  ]
                )
              ),
            postDesc(),
            SizedBox(height: 16.0,),
            Align(
              alignment: Alignment.topRight,
              child: FlatButton(
                child: Text(
                  '등록', 
                  style: Theme.of(context).textTheme.button.copyWith(fontWeight: FontWeight.w700),),
                  onPressed: () => _postingBBon(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _todayDressReview() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 32.0,),
            Text(
              ' 오늘의 나는 ', 
              style: Theme.of(context).textTheme.subtitle1
              ),
            SizedBox(height: 32.0,),
            Row(
              children: <Widget>[
                SizedBox(width: 32.0,),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: 42.0,
                    ),
                    SizedBox(height: 8.0,),
                    Text(
                      '${userProvider.todayMyDress.likeCount}',
                      style: Theme.of(context).textTheme.subtitle2
                    ),
                  ],
                ),
                SizedBox(width: 32.0,),
                Column(
                  children: <Widget>[
                    Icon(
                      Icons.comment,
                      color: Colors.white,
                      size: 42.0,
                    ),
                    SizedBox(height: 8.0,),
                    Text(
                      '${userProvider.todayMyDress.replyCount}',
                      style: Theme.of(context).textTheme.subtitle2
                    ),
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Column(
                      children: <Widget>[
                        Text('${timeago.format(userProvider.todayMyDress.createdAt, locale:'ko')}', style: Theme.of(context).textTheme.bodyText2,),
                        Text('${DateUtil.dateFormatHHmm.format(userProvider.todayMyDress.createdAt.toLocal())}', style: Theme.of(context).textTheme.bodyText2),

                      ],
                    ),
                  ),
                ),
                SizedBox(width: 16.0,),
              ],
            ),
            SizedBox(height: 16.0,),
            CachedNetworkImage(
              imageUrl: userProvider.todayMyDress.images[0],
              imageBuilder: (context, imageProvider) => Container(
                width: double.infinity,
                height: 400,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover
                  )
                ),
              ),
              placeholder: (context, url) => Icon(Icons.panorama),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(height: 16.0,),
            Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ReadMoreText(
                    userProvider.todayMyDress.say,
                    style: Theme.of(context).textTheme.bodyText1,
                    trimLines: 100,
                    expanded: true,
                    colorClickableText: Theme.of(context).hintColor,
                    trimMode: TrimMode.Line,
                  ),
                )
              )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    userProvider = Provider.of<UserProvider>(context);
    dailyDressProvider = Provider.of<DailyDressProvider>(context);


    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Theme.of(context).backgroundColor,
          child: (userProvider.todayMyDress == null) 
            ? _todayDressPost()
            : _todayDressReview()
        ),
      ),
    );
  }
}