import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:itswd/v3/components/common/PiecesUnits.dart' as util;
import 'package:itswd/v3/components/views/UserIcon.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/network/ApiHelper.dart';


class BriefUserHeader extends StatefulWidget {

  ModelUser user;
  BriefUserHeader({
    Key key,
    this.user
  }): super(key: key);

  @override
  _BriefUserHeaderState createState() => _BriefUserHeaderState();
}

enum UserActions {POST, REPLY, LIKE}

class _BriefUserHeaderState extends State<BriefUserHeader> {
  

  int _countPost;
  int _countLike;
  int _countReply;

  @override
  void initState() {
    setState(() {
      _countPost = 0;
      _countLike = 0;
      _countReply = 0;
    });
    super.initState();

    reqUserAct();
  }

  void reqUserAct() async {
    var response = await ApiHelper().get('/user/received/${widget.user.id}');
    if(response['code'] == 200) {
      setState(() {
        _countPost = response['data'][0]['totalCount'];
        _countLike = response['data'][0]['receiveLikeCount'];
        _countReply = response['data'][0]['receiveReplyCount'];

      });
    }
  }

  Widget userInfo() {
    return widget.user != null ? 
      Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          SizedBox(height: 8.0,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 8.0,),
              UserIcon(user: widget.user, launchDetail: false, size: 90.0, showName: false,),
              SizedBox(width: 24.0),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    userAct(UserActions.POST, _countPost),
                    SizedBox(width: 32.0,),
                    userAct(UserActions.LIKE, _countLike),
                    SizedBox(width: 32.0,),
                    userAct(UserActions.REPLY, _countReply),
                  ],
                ),
              ),
              SizedBox(width: 24.0),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  widget.user.aboutMe ?? util.getName(widget.user), 
                  style: Theme.of(context).textTheme.bodyText1,
                  )
              ),
            ),
          )
        ],
      ),
    )
    :Container(

    );
  }

  Widget userAct(UserActions type, int count) {
    String target;
    switch (type) {
      case UserActions.POST:
        target = '출근룩';
      break;
      case UserActions.REPLY:
        target = "댓글";
      break;
      case UserActions.LIKE:
        target = "좋아요";
      break;
      default:       
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(count.toString(), style: Theme.of(context).textTheme.title,),
        SizedBox(
            height: 2 ,
            width: 50,
            child: DecoratedBox(
              decoration: BoxDecoration(color: Colors.white),
            ),
          ),
        SizedBox(height: 2.0,),
        SizedBox(
            height: 2,
            width: 50,
            child: DecoratedBox(
              decoration: BoxDecoration(color: Colors.white),
            ),
          ),
        SizedBox(height: 8.0,),
        Text(target, style: Theme.of(context).textTheme.body1,)
      ],
    );
  }



  @override
  Widget build(BuildContext context) {
    
    return userInfo();
  }
}