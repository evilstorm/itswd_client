import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CountWithUnderCount extends StatelessWidget {

  final String title;
  final int count;
  CountWithUnderCount({
    Key key,
    @required this.title,
    @required this.count,
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(count.toString(), style: Theme.of(context).textTheme.subtitle1,),
        SizedBox(
            height: 1 ,
            width: 60,
            child: DecoratedBox(
              decoration: BoxDecoration(color: Colors.white),
            ),
          ),
        SizedBox(height: 4.0,),
        SizedBox(
            height: 1,
            width: 60,
            child: DecoratedBox(
              decoration: BoxDecoration(color: Colors.white),
            ),
          ),
        SizedBox(height: 8.0,),
        Text(title, style: Theme.of(context).textTheme.subtitle2)
      ],
    );
  }

}