import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabWidget extends StatefulWidget {
  List<String> tabWord;
  double height;
  Function callback;

  TabWidget({
    Key key,
    @required this.tabWord,
    this.height = 50.0,
    this.callback
  }): super(key: key);

  @override
  TabWidgetState createState() => TabWidgetState();
}

class TabWidgetState extends State<TabWidget> {

  int _selectedTab = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: widget.height,
      decoration: BoxDecoration(
        border: Border.all(),
        color: Colors.black
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: 
          widget.tabWord.asMap().entries.map((item) => 
            Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _selectedTab = item.key;
                  });

                  if(widget.callback != null) {
                    widget.callback(item.key);
                  }
                },
                child: Container(
                  height: 50.0,
                  color: _selectedTab==item.key ? Colors.redAccent : Theme.of(context).backgroundColor,
                  child: Center(
                    child: Text(item.value, style: Theme.of(context).textTheme.bodyText1.copyWith(fontWeight: FontWeight.w600),)
                  ),
                ),
              ),
            )
          ).toList()
      ),
    );
  }
}