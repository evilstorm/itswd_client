import 'package:flutter/material.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/provider/UserDressProvider.dart';
import 'package:itswd/v3/provider/UserEventProvider.dart';
import 'package:itswd/v3/provider/UserProvider.dart';
import 'package:itswd/v3/screen/user_content/UserContents.dart';
import 'package:itswd/v3/screen/user_content/UserDressDetail.dart';
import 'package:provider/provider.dart';

class UserContentScreen extends StatefulWidget {

  final ModelUser user;
  UserContentScreen({
    Key key, 
    @required this.user
  }): super(key: key);


  @override
  _UserContentScreenState createState() => _UserContentScreenState();
}

class _UserContentScreenState extends State<UserContentScreen> {
  UserEventProvider eventProvider;
  UserProvider userProvider;

  bool _backPress() {
    if(eventProvider.isGridView == 0) {
      Navigator.of(context).pop();
      return true;
    }
    eventProvider.showGridView();
    return false;
  }

  bool isBlockedUser() {
    for (var blockUserId in userProvider.userInfo.blockUser) {
      if(blockUserId == widget.user.id) {
        return true;
      }
    }
    return false;
  }
  Widget _content() {
    if(isBlockedUser()) {
      
      return (
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '차단된 유저 입니다.',
              style: Theme.of(context).textTheme.subtitle2
            ),
            SizedBox(height: 16.0,),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                '닫기',
                style: Theme.of(context).textTheme.subtitle1.apply(color: Colors.pinkAccent[700]),
              ),
            )
          ],
        )
      );
    }

    return Column(
      children: [
        HeaderWithBack(title: widget.user.name??"Unknown", callback: () {
          _backPress();
        }),
        Expanded(
          child: Consumer<UserEventProvider>(
            builder: (context, provider, _) {
              eventProvider = provider;
              return IndexedStack(
                index: provider.isGridView,
                children: [
                  UserContents(user: widget.user),
                  UserDressDetail(user: widget.user,)
                ],
              );
            },
          ),
        ),
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    Print.d("_UserContentScreenState build");
    
    userProvider = Provider.of<UserProvider>(context);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserDressProvider(),),
        ChangeNotifierProvider(create: (_) => UserEventProvider(),),
        
      ],
      child: Scaffold(
        body: SafeArea(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Theme.of(context).backgroundColor,
            child: _content()
          ),
        )
      ),
    );
  }
}