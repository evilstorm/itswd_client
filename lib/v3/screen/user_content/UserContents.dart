import 'package:flutter/material.dart';
import 'package:itswd/v3/components/Contants.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/SliverHeader.dart';
import 'package:itswd/v3/model/response/ModelDress.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/provider/BaseProvider.dart';
import 'package:itswd/v3/provider/UserDressProvider.dart';
import 'package:itswd/v3/provider/UserEventProvider.dart';
import 'package:itswd/v3/screen/dress_view/ContentGridSliver.dart';
import 'package:itswd/v3/screen/user_content/TabWidget.dart';
import 'package:itswd/v3/screen/user_content/UserInfoWithCount.dart';
import 'package:provider/provider.dart';

class UserContents extends StatefulWidget {

  final ModelUser user;
  UserContents({
    Key key, 
    @required this.user
  }): super(key: key);

  @override
  _UserContentsState createState() => _UserContentsState();
}

class _UserContentsState extends State<UserContents> {

  ScrollController _scrollController = ScrollController();

  UserDressProvider dressProvider;
  UserEventProvider eventProvider;
  
  double scrollOffSet = 0;

  // List<ModelDress> _dressList;
  bool _scrollTopPos = true;
  bool _isDataLoding = true;

  @override
  void initState() {
    super.initState();
    Print.e(" UserContents INIT initState ");

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
        _scrollTopPos = true;
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(!_isDataLoding && (dressProvider?.canReadMore??false)) {
          _isDataLoding = true;
          dressProvider.readMore();
        }
      } else {
        _scrollTopPos = false;
      }
    });
  }
  
  
  void _tabChange(int selectedTabPos) {
    dressProvider.reqUserDress(widget.user.id, selectedTabPos==0?SORT_TYPE.LIKE:SORT_TYPE.DATE);
    _scrollController.jumpTo(0);
    eventProvider.setTabPos = selectedTabPos;
  }
  
  ///그리드 뷰에서 아이템 선택 시 callback 
  _gridItemSelect(ModelDress dress, int index) {
    eventProvider.showListView(index, _scrollController.position.pixels);
  }
  
  Widget _contentWidget() {
    return ((dressProvider?.dressList??null) != null && dressProvider?.dressList?.length != 0)
    ? ContentGridSliver(key: UniqueKey(), 
        dressList: dressProvider.dressList, 
        clickListener: _gridItemSelect, 
        scrollController: _scrollController
      )
    : SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) => Center(child: Text('깨 끗!', style: Theme.of(context).textTheme.subtitle1,)),
          childCount: 1
        ),
      );
  }
  @override
  Widget build(BuildContext context) {
    dressProvider = Provider.of<UserDressProvider>(context);
    eventProvider = Provider.of<UserEventProvider>(context);

    if(dressProvider.dressList == null && dressProvider.state == ProviderState.INIT) {
      _tabChange(0);
    }
    if(dressProvider.state == ProviderState.DATA_CHANGE 
      ||dressProvider.state == ProviderState.DONE
    ) {
      _isDataLoding = false;
    }
    

    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        SliverPersistentHeader(
          delegate: SliverHeader(
            maxHeight: 200.0,
            minHeight: 50.0,
            child: UserInfoWithCount(userId: widget.user.id, height: 200.0),
          ), 
        ),
        SliverPersistentHeader(
          floating: false,
          pinned: true,                      
          delegate: SliverHeader(
            maxHeight: 50.0,
            minHeight: 50.0,
            child: TabWidget(tabWord: ['인기순', '시간순'], callback: _tabChange,),
          ), 
        ),
        _contentWidget()
      ],
    );
    
  }
}