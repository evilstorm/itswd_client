import 'package:flutter/material.dart';
import 'package:itswd/v3/components/Contants.dart';
import 'package:itswd/v3/components/common/Print.dart';
import 'package:itswd/v3/components/views/HeaderWithBack.dart';
import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/provider/UserDressProvider.dart';
import 'package:itswd/v3/provider/UserEventProvider.dart';
import 'package:itswd/v3/screen/dress_view/ContentListSliver.dart';
import 'package:provider/provider.dart';

class UserDressDetail extends StatefulWidget {

  final ModelUser user;
  UserDressDetail({
    Key key, 
    @required this.user
  }): super(key: key);


  @override
  _UserDressDetailState createState() => _UserDressDetailState();
}

class _UserDressDetailState extends State<UserDressDetail> {

  UserDressProvider dressProvider;
  UserEventProvider eventProvider;

  ScrollController _scrollController = ScrollController();

  bool _isDataLoding = true;
  bool _scrollTopPos = true;


  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == 0) {
        _scrollTopPos = true;
      } else if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(!_isDataLoding && (dressProvider?.canReadMore??false)) {
          _isDataLoding = true;
          dressProvider.readMore();
        }
      } else {
        _scrollTopPos = false;
      }
    });

  }

  Widget _contentWieget() {

    if(dressProvider.dressList == null || dressProvider.dressList?.length == 0) {
      return SliverList(
        delegate: SliverChildBuilderDelegate(
          (context, index) => Center(child: Text('뽄지는 친구가 없네?', style: Theme.of(context).textTheme.subtitle1,)),
          childCount: 1
        ),
      );
    }

    _scrollController.jumpTo((CONTENT_HEIGHT * eventProvider.selectedPos) + HEADER_HEIGHT);
    return ContentListSliver(
        key: UniqueKey(),
        dressList: dressProvider.dressList, 
        selectedItemIndex: eventProvider.selectedPos, 
        launchUserDetail: false, 
      );
  }

  String getName() {
    if(widget.user.name == null) {
      return "Unkwon";
    }
    return widget.user.name;
  }
  @override
  Widget build(BuildContext context) {

    dressProvider = Provider.of<UserDressProvider>(context);
    eventProvider = Provider.of<UserEventProvider>(context);


    return SafeArea(
      child: CustomScrollView(
        controller: _scrollController,
        slivers: [
          _contentWieget()
        ],
      ),
    );
  }
}