import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:itswd/v3/model/response/ModelUser.dart';
import 'package:itswd/v3/network/ApiHelper.dart';
import 'package:itswd/v3/screen/user_content/BriefUserHeader.dart';
import 'package:itswd/v3/screen/user_content/CountWithUnderCount.dart';
import 'package:itswd/v3/components/views/UserIcon.dart';



class UserInfoWithCount extends StatelessWidget {

  final double height;
  final double width;
  final String userId;
  
  UserInfoWithCount({
    Key key,
    @required this.height,
    @required this.userId,
    this.width = -1, 
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = (this.width == -1)
      ? MediaQuery.of(context).size.width
      : this.width;

    return FutureProvider(
      initialData: new ModelUser(),
      create: (_) => reqUserInfo(userId),
      child: Container(
        width: width,
        height: height,
        color: Theme.of(context).backgroundColor,
        child: Consumer<ModelUser>(
          builder: (context, model, child) {
            return Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    UserIcon(user: model, launchDetail: false, size: 90.0, showName: false,),
                    SizedBox(width: 16.0,),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CountWithUnderCount(count: model.dressCount, title: convertString(UserActions.POST),),
                          CountWithUnderCount(count: model.likeDressCount, title: convertString(UserActions.LIKE),),
                          CountWithUnderCount(count: model.replyCount, title: convertString(UserActions.REPLY),),
                      ],),
                    )
                  ],
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Linkify(
                            onOpen: (link) async {
                              if(await canLaunch(link.url)) {
                                await launch(link.url);
                              } else {
                                throw 'Could not lanch $link';
                              }
                            },
                            textAlign: TextAlign.start,
                            text: model.aboutMe,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        )
      ),
    );
  }


  String convertString(UserActions type) {
    switch (type) {
      case UserActions.POST:
        return '출근룩';
      break;
      case UserActions.REPLY:
        return "댓글";
      break;
      case UserActions.LIKE:
        return "좋아요";
      break;
      default:
        return "좋아요";
    }
  }

  Future<ModelUser> reqUserInfo(userId) async {
    var response = await ApiHelper().get('/user/id/$userId');
    if(response['code'] == 200 ) {
     ModelUser _modelUser = ModelUser.fromJsonDynamic(response['data'][0]);
      return _modelUser;
    } else {
      return null;
    }
  }

}